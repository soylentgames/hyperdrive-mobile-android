﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Hyperdrive.PL;

namespace Hyperdrive.SL
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class HyperdriveService : IHyperdriveService
    {

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public Guid Login(string username, string password)
        {
            using (HyperdriveDataContext oDC = new HyperdriveDataContext())
            {
                spLoginResult r = oDC.spLogin(username, password).FirstOrDefault();
                if (r != null)
                {
                    return r.Id;
                }
                else
                    return Guid.Empty;
            }
        }

        public bool AddUser(string username, string password, string emailAddress, out string response)
        {
            using (HyperdriveDataContext oDC = new HyperdriveDataContext())
            {
                response = "";
                return Convert.ToBoolean(oDC.spAddUser(username, password, emailAddress, ref response));
            }
        }
    }
}
