﻿CREATE PROCEDURE [dbo].[spLogin]
	@Username nvarchar(24),
	@Password nvarchar(24)
AS
BEGIN
     SET NOCOUNT ON
     SELECT Id FROM [dbo].[tblUser] WHERE Username=@Username AND PasswordHash=HASHBYTES('SHA2_512', @Password+CAST(Salt AS NVARCHAR(36)))
END