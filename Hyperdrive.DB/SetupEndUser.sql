
--alter schema dbo transfer EndUserSchema.spAddUser;
--alter schema dbo transfer EndUserSchema.spLogin;

--drop schema EndUserSchema;

--create schema EndUserSchema;
--go
--alter schema EndUserSchema transfer [dbo].[spAddUser];
--go
--alter schema EndUserSchema transfer [dbo].[spLogin];
--go

-- -- these next two are run from master
--drop login EndUser
--go
--create login EndUser with password = 'End_User1'
--go

--create user EndUser for login EndUser with default_schema = EndUserSchema
--go

--grant execute on EndUserSchema.spLogin to EndUser
--grant execute on EndUserSchema.spAddUser to EndUser
--go