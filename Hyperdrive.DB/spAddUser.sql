﻿CREATE PROCEDURE [dbo].[spAddUser]
	@Username nvarchar(24),
	@Password nvarchar(24),
	@EmailAddress nvarchar(100),
	@Response nvarchar(100) output
AS
BEGIN
	SET NOCOUNT ON

	IF EXISTS(SELECT * FROM [dbo].[tblUser] WHERE Username = @Username)
	BEGIN
		SET @Response='Username is taken.'
	    RETURN 0
	END
    DECLARE @salt UNIQUEIDENTIFIER=NEWID()
    BEGIN TRY

        INSERT INTO [dbo].[tblUser] (Id, Username, PasswordHash, Salt, EmailAddress)
        VALUES(NewId(), @Username, HASHBYTES('SHA2_512', @Password+CAST(@salt AS NVARCHAR(36))), @salt, @EmailAddress)

        SET @Response='Successfully added user!'
		RETURN 1
    END TRY
    BEGIN CATCH
        SET @Response=ERROR_MESSAGE() 
		RETURN 0
    END CATCH
END
