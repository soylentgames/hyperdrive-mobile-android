using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Hyperdrive.Base;
using Hyperdrive.Base.Controls;
using Microsoft.Xna.Framework;
using System;
using static Android.Views.View;

namespace Hyperdrive.UI.Android
{
    [Activity(Label = "Hyperdrive Mobile"
        , MainLauncher = true
        , Icon = "@drawable/icon"
        , Theme = "@style/Theme.Splash"
        , AlwaysRetainTaskState = true
        , LaunchMode = LaunchMode.SingleInstance
        , ScreenOrientation = ScreenOrientation.SensorLandscape
        , ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]

    public class HyperdriveActivity : AndroidGameActivity
    {
        HyperdriveGame g;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            g = new HyperdriveGame(Application);
            SetContentView(g.Services.GetService<View>());
            g.Run();
        }

        private void OnKeyPress(object sender, KeyEventArgs e)
        {
            TKeyPress keyPress = new TKeyPress();
            //convert to TKeycode format to remain cross-platform
            if (e.KeyCode >= Keycode.A && e.KeyCode <= Keycode.Z)
            {
                keyPress.KeyCode = (TKeycode)e.KeyCode;
                if (e.Event.IsCapsLockOn || e.Event.IsShiftPressed)
                    keyPress.IsUppercase = true;
                else
                    keyPress.IsUppercase = false;
            }

            GameDataManager.OnKeyPress(keyPress);
        }
    }
}

