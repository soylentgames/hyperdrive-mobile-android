using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Hyperdrive.Base;
using Microsoft.Xna.Framework.Input.Touch;
using System.Collections.Generic;
using Hyperdrive.Base.Enemy;
using Hyperdrive.Base.Screens;
using System;
using Android.Views;
using Android.App;
using Android.Content;
using Android.Views.InputMethods;

namespace Hyperdrive.UI.Android
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class HyperdriveGame : Game
    {
        //environment objects
        public bool IsInitialized { get; private set; }

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
       
        InputMethodManager inputManager;

        public HyperdriveGame(Application app)
        {
            inputManager = app.GetSystemService(Context.InputMethodService) as InputMethodManager;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;
            graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;
            IsInitialized = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Add your initialization logic here
            TargetElapsedTime = TimeSpan.FromTicks(166667);
            spriteBatch = new SpriteBatch(GraphicsDevice);

            GameDataManager.Init(graphics, spriteBatch, Content, inputManager, Services.GetService<View>(), new LoadingScreen());
            
            Deactivated += CatchDeactivate;

            base.Initialize();

            IsInitialized = true;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected void CatchDeactivate(object o, EventArgs e)
        {
            GameDataManager.CurrentScreen.Deactivated();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();


            // Add your update logic here
            double dSeconds = gameTime.ElapsedGameTime.TotalSeconds;
            float seconds = (float)dSeconds;

            GameDataManager.Update(seconds, dSeconds);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.Deferred,BlendState.AlphaBlend);

            GameDataManager.Draw();

            spriteBatch.End();

            base.Draw(gameTime);
        }
        
    }
}
