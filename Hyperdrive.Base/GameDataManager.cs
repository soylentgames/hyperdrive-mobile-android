﻿using Android.App;
using Android.Views;
using Android.Views.InputMethods;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Controls;
using Hyperdrive.Base.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace Hyperdrive.Base
{
    public static class GameDataManager
    {
        public static int MaxSounds = 32;
        public static GraphicsDeviceManager Gfx;
        public static SpriteBatch Batch;
        public static ContentManager Content;
        public static Dictionary<string, SoundEffect> Sounds { get; set; }
        public static Dictionary<string, Song> Songs { get; set; }
        public static Dictionary<string, Texture2D> Textures { get; set; }
        public static Dictionary<string, SpriteFont> Fonts { get; set; }
        public static List<SoundEffectInstance> CurrentSounds { get; set; }
        public static GameScreen CurrentScreen { get; set; }
        public static GameGrid Grid { get; set; }
        public static CollisionManager CollisionManager { get; set; }

        public static bool isInitialized = false;

        private static InputMethodManager inputManager { get; set; }
        private static View myView { get; set; }

        public static void Init(GraphicsDeviceManager g, SpriteBatch b, ContentManager content, InputMethodManager inputManager, View myView, GameScreen currentScreen)
        {
            GameDataManager.inputManager = inputManager;
            GameDataManager.myView = myView;
            Gfx = g;
            Batch = b;
            Content = content;
            Sounds = new Dictionary<string, SoundEffect>();
            Songs = new Dictionary<string, Song>();
            Textures = new Dictionary<string, Texture2D>();
            Fonts = new Dictionary<string, SpriteFont>();
            CurrentSounds = new List<SoundEffectInstance>();
            Grid = new GameGrid(1920);
            CollisionManager = new CollisionManager();
            isInitialized = true;
            CurrentScreen = currentScreen;
            CurrentScreen.Init();
        }

        public static void AddSound(SoundEffect i, float pitch, float volume)
        {
            if (CurrentSounds.Count < MaxSounds)
            {
                SoundEffectInstance inst = i.CreateInstance();
                inst.Pitch = pitch;
                inst.Volume = volume;

                CurrentSounds.Add(inst);
                inst.Play();
            }
        }

        public static void Update(float seconds, double dSeconds)
        {
            List<SoundEffectInstance> trash = new List<SoundEffectInstance>();
            foreach (SoundEffectInstance sound in CurrentSounds) if (sound.State == SoundState.Stopped) trash.Add(sound);
            foreach (SoundEffectInstance sound in trash) CurrentSounds.Remove(sound);

            CurrentScreen.Update(seconds, dSeconds);
            if (CurrentScreen.ChangingScreen)
            {
                CurrentScreen = CurrentScreen.NextScreen;
                CurrentScreen.Init();
                CurrentScreen.Update(seconds, dSeconds); //run one update since the next thing that happens is that the draw phase will be invoked
            }
        }

        public static void Draw()
        {
            CurrentScreen.Draw();
        }

        public static void ShowKeyboard()
        {
            inputManager.ShowSoftInput(myView, ShowFlags.Forced);
            inputManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
        }

        public static void HideKeyboard()
        {
            inputManager.HideSoftInputFromWindow(myView.WindowToken, HideSoftInputFlags.None);
        }

        public static void OnKeyPress(TKeyPress keyPress)
        {
            CurrentScreen.OnKeyPress(keyPress);
        }
    }
}
