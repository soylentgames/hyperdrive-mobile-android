﻿using Microsoft.Xna.Framework;
using System;

namespace Hyperdrive.Base
{
    public class MyRandom : Random
    {
        public float NextFloat()
        {
            return (float)base.NextDouble();
        }

        public bool NextBoolean()
        {
            return base.Next(2) == 0 ? true : false;
        }
    }

    public static class GameMath
    {
        public static MyRandom RNGer = new MyRandom();

        public static float RotationFromVector(Vector2 vector)
        {
            return (float)(Math.Atan2(vector.Y, vector.X) + Math.PI / 2);
        }

        public static Vector2 GetPointAtAngle(Vector2 origin, float radius, double radians) //remember, PI radians is 180 degrees
        {
            return new Vector2(origin.X + (float)Math.Cos(radians - Math.PI/2) * radius, origin.Y + (float)Math.Sin(radians - Math.PI/2) * radius); //57.2957795 is conversion to radians
        }

        public static Vector2 RotatePointAroundPoint(Vector2 point, Vector2 origin, double radians)
        {
            double x1 = point.X - origin.X;
            double y1 = point.Y - origin.Y;

            double x2 = x1 * Math.Cos(radians) - y1 * Math.Sin(radians);
            double y2 = x1 * Math.Sin(radians) + y1 * Math.Cos(radians);

            return new Vector2((float)(x2 + origin.X), (float)(y2 + origin.X));
        }

        public static Vector2 DotProduct(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static float Distance(Vector2 v1, Vector2 v2)
        {
            return (float)Math.Pow(Math.Pow(v2.X - v1.X, 2) + Math.Pow(v2.Y - v1.Y, 2), 0.5);
        }

        public static float DistanceSquared(Vector2 v1, Vector2 v2)
        {
            return (float)(Math.Pow(v2.X - v1.X, 2) + Math.Pow(v2.Y - v1.Y, 2));
        }
    }
}
