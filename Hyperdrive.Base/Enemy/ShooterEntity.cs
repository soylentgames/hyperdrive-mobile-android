﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Player;
using Hyperdrive.Base.Enemy.Shots;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Enemy.Controllers;
using Hyperdrive.Base.Gfx;
using Hyperdrive.Base.Container;

namespace Hyperdrive.Base.Enemy
{
    public class ShooterEntity : EnemyEntity
    {
        private double shieldEffectCount;
        private float effect1;
        private float effect2;
        private float effect3;
        private float effect4;
        private Color effectColor;

        //shot vars
        private float shotCount; //counts time
        private int shotsFired; //keeps track of shots fired over time
        private float shotTime; //how long to wait before firing the burst
        private int shotsToFire; //how many shots to fire

        private Rectangle shieldBubble;

        private Animation simpleBulletAnim; //replace this later

        public ShooterEntity(Vector2 pos, Vector2 vel, float scale, float shotTime, int shotsToFire) : base(pos, vel)
        {
            SpriteController = new ShooterSpriteController();
            ShotsPerSecond = 1f; //will shoot at most this fast, at least half this fast
            ShotForce = 400f;
            ShotScale = 1f;
            MaxHull = 8f + GameMath.RNGer.NextFloat() * 8f;
            Hull = MaxHull;
            MaxShields = 4f + GameMath.RNGer.NextFloat() * 3f;
            Shields = MaxShields;
            Points = (int)(MaxHull + MaxShields);
            Acceleration = 20f;
            MaxSpeed = 200f;
            MinDamage = 2f;
            MaxDamage = 3f;
            Scale = scale;
            shieldBubble = new Rectangle(704, 0, 64, 64);
            shotCount = 0;
            shotsFired = 0;
            this.shotTime = shotTime;
            this.shotsToFire = shotsToFire;
            Collider = new CircleCollider(32f * scale, new Vector2(), this, ColliderType.Enemy);
            simpleBulletAnim = new Animation();
            simpleBulletAnim.AddFrame(new Rectangle(704,128,64,64), TimeSpan.FromSeconds(1f));
        }

        public override void Aggro(PlayerEntity p)
        {
            if (currentTarget == null) currentTarget = p;
            else if (currentTarget != null && currentTarget != p) if (GameMath.RNGer.Next(4) == 0) currentTarget = p;
        }

        public override void TargetUpdate(PlayerEntity p, float seconds, double dSeconds)
        {
            currentTarget = p;
        }

        public override void Draw(SpriteBatch b)
        {
            SpriteController.Draw(b, Position, (float)lastHit, Rotation, ShieldPercentage, 1f);

            if (Shields > 0)
            {
                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.Additive);
                
                b.Draw(particles, Position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(effect1, effect1), SpriteEffects.None, 0f);
                b.Draw(particles, Position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(effect2, 0.3f + effect2), SpriteEffects.None, 0f);
                b.Draw(particles, Position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(0.3f + effect3, effect3), SpriteEffects.None, 0f);
                b.Draw(particles, Position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(0.3f + effect4, 0.3f + effect4), SpriteEffects.None, 0f);

                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            }
        }

        public override void Update(float seconds, double dSeconds, EnemyShotContainer shotPool)
        {
            base.Update(seconds, dSeconds);  //updates pos and resets ReturnList
            Velocity = new Vector2(Velocity.X, Velocity.Y + Acceleration);
            
            //velocity cap
            if (Velocity.Length() > MaxSpeed)
            {
                Vector2 vel = Velocity;
                vel.Normalize();
                Velocity = vel * MaxSpeed;
            }

            shieldEffectCount += dSeconds;
            shieldEffectCount = shieldEffectCount % 1;

            effect1 = (float)((Math.Sin(shieldEffectCount * Math.PI * 2) + 1) / 2);
            effect2 = (float)((Math.Cos(shieldEffectCount * Math.PI * 2) + 1) / 2);
            effect3 = (float)((Math.Sin(shieldEffectCount * Math.PI * 2 + Math.PI) + 1) / 2);
            effect4 = (float)((Math.Cos(shieldEffectCount * Math.PI * 2 + Math.PI) + 1) / 2);
            effectColor = new Color(ShieldPercentage, ShieldPercentage / 4f, ShieldPercentage / 4f);

            //fire some shots
            shotCount += seconds;
            if (currentTarget != null && shotCount >= shotTime) // fires 3 shots
            {
                if (shotCount >= shotTime + (0.05f * shotsFired))
                {
                    Vector2 targetDir = (currentTarget.Position + new Vector2(-64 + GameMath.RNGer.Next(128), -64 + GameMath.RNGer.Next(128))) - Position;
                    targetDir.Normalize();
                    targetDir = targetDir * ShotForce;
                    shotPool.AddEntity(new EnemyShotEntity(this, Position, targetDir, simpleBulletAnim, MinDamage, MaxDamage, new Vector2(0.25f, 0.5f), 50f, new Color(255, 64, 64),EnemyShotType.Normal));
                    shotsFired++;
                    GameDataManager.AddSound(fireSound, -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 0.6f);
                    if (shotsFired >= shotsToFire) { shotCount = 0; shotsFired = 0; }
                }
            }
        }
    }
}
