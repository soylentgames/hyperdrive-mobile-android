﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Player;
using Hyperdrive.Base.Enemy.Shots;
using System;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Enemy.Controllers;
using Hyperdrive.Base.Gfx;
using Hyperdrive.Base.Container;

namespace Hyperdrive.Base.Enemy
{
    public class LaseringSpinnerEntity : EnemyEntity
    {
        private float rotationSpeed;
        private int shotCount;

        private float fireCount;
        private double timeDeployed;
        private float deployPoint;
        private bool leaving;
        private bool deployed = false;

        private Animation simpleBulletAnim; //replace this later

        public LaseringSpinnerEntity(Vector2 pos, Vector2 vel, float scale, float rotationSpeed, int shotCount) : base(pos, vel)
        {
            SpriteController = new ShootingSpinnerSpriteController();
            ShotsPerSecond = 7f;
            ShotForce = 1500f;
            ShotScale = 1f;
            MaxHull = 5f + GameMath.RNGer.NextFloat() * 5f;
            Hull = MaxHull;
            Points = (int)MaxHull;  
            Shields = 0f;
            MaxShields = 0f;
            Acceleration = 500f;
            MaxSpeed = 250f;
            MinDamage = 1f;
            MaxDamage = 2f;
            Scale = scale;
            this.shotCount = shotCount;
            this.rotationSpeed = rotationSpeed;

            fireCount = 0;
            timeDeployed = 0;
            leaving = false;
            deployPoint = GameMath.RNGer.Next((GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 4) * 3);
            Collider = new CircleCollider(32f * Scale, new Vector2(), this, ColliderType.Enemy);
            simpleBulletAnim = new Animation();
            simpleBulletAnim.AddFrame(new Rectangle(704, 128, 64, 64), TimeSpan.FromSeconds(1f));
        }

        public override void Aggro(PlayerEntity p)
        {
            deployed = true;
            timeDeployed = 0;
        }

        public override void Update(float seconds, double dSeconds, EnemyShotContainer shotPool)
        {
            base.Update(seconds, dSeconds);  //updates pos and resets ReturnList
            Rotation += rotationSpeed;
            if (!deployed && !leaving)
            {
                Velocity = new Vector2(Velocity.X, Velocity.Y + Acceleration * seconds > MaxSpeed ? MaxSpeed : Velocity.Y + Acceleration * seconds);
                if (Position.Y >= deployPoint)
                {
                    deployed = true;
                    timeDeployed = 0;
                }
            }
            else if (leaving) Velocity = new Vector2(Velocity.X, Velocity.Y + Acceleration * seconds > MaxSpeed ? MaxSpeed : Velocity.Y + Acceleration * seconds);
            else
            {
                Velocity *= 0.9f;
                if (Velocity.Y <= 5f)
                {
                    timeDeployed += seconds;
                    fireCount += seconds;
                    if (fireCount >= 1f / ShotsPerSecond)
                    {
                        fireCount = 0;
                        if (shotCount >= 1)
                        {
                            Vector2 shotVel = new Vector2((float)Math.Cos(Rotation), (float)Math.Sin(Rotation)) * ShotForce;
                            shotPool.AddEntity(new EnemyShotEntity(this, Position, shotVel, simpleBulletAnim, MinDamage, MaxDamage, new Vector2(0.25f, 8f), 50f, new Color(64, 64, 255), EnemyShotType.Laser));
                        }
                        if (shotCount >= 2)
                        {
                            Vector2 shotVel = new Vector2((float)Math.Cos(Rotation + Math.PI / 2), (float)Math.Sin(Rotation + Math.PI / 2)) * ShotForce;
                            shotPool.AddEntity(new EnemyShotEntity(this, Position, shotVel, simpleBulletAnim, MinDamage, MaxDamage, new Vector2(0.25f, 8f), 50f, new Color(64, 64, 255), EnemyShotType.Laser));
                        }
                        if (shotCount >= 3) //always fires 1, 2, or 4
                        {
                            Vector2 shotVel = new Vector2((float)Math.Cos(Rotation + Math.PI), (float)Math.Sin(Rotation + Math.PI)) * ShotForce;
                            shotPool.AddEntity(new EnemyShotEntity(this, Position, shotVel, simpleBulletAnim, MinDamage, MaxDamage, new Vector2(0.25f, 8f), 50f, new Color(64, 64, 255), EnemyShotType.Laser));
                            shotVel = new Vector2((float)Math.Cos(Rotation - Math.PI / 2), (float)Math.Sin(Rotation - Math.PI / 2)) * ShotForce;
                            shotPool.AddEntity(new EnemyShotEntity(this, Position, shotVel, simpleBulletAnim, MinDamage, MaxDamage, new Vector2(0.25f, 8f), 50f, new Color(64, 64, 255), EnemyShotType.Laser));
                        }
                        GameDataManager.AddSound(laserSound, -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 0.6f);
                    }
                    if (timeDeployed >= 15f)
                    {
                        leaving = true;
                    }
                }
            }

            //velocity cap
            if (Velocity.Length() > MaxSpeed)
            {
                Vector2 vel = Velocity;
                vel.Normalize();
                Velocity = vel * MaxSpeed;
            }
        }

        public override void Draw(SpriteBatch b)
        {
            SpriteController.Draw(b,Position,(float)lastHit,Rotation,ShieldPercentage,Scale);
        }
        
    }
}
