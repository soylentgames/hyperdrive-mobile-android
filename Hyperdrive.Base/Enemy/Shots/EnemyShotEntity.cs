﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Particle.AI;
using System.Collections.Generic;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Gfx;

namespace Hyperdrive.Base.Enemy.Shots
{
    public enum EnemyShotType
    {
        Normal,
        Laser
    }

    public class EnemyShotEntity : CollidableEntity
    {
        private static Rectangle glowRect = new Rectangle(574, 0, 64, 64);
        private double glowCount;
        private Color glowColor;

        public static Texture2D particles;
        public EnemyEntity Owner { get; set; }
        public float Knockback { get; protected set; }
        public EnemyShotType Type { get; protected set; }
        public bool Dead { get; set; }
        private Animation Animation { get; set; }
        private float MinDamage { get; set; }
        private float MaxDamage { get; set; }
        private Color ShotColor { get; set; }
        private Vector2 Scale { get; set; }
        private bool HasMoved { get; set; }
        private Rectangle LastCollider { get; set; }
        private float DamageRange { get { return MaxDamage - MinDamage; } }

        public EnemyShotEntity(EnemyEntity owner, Vector2 position, Vector2 velocity, Animation animation, float minDamage, float maxDamage, Vector2 scale, float knockback, Color color, EnemyShotType type) : base(position, velocity)
        {
            Owner = owner;
            ShotColor = color;
            Animation = animation;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            Scale = scale;
            Knockback = knockback;
            HasMoved = false;
            Dead = false;
            Type = type;
            Rotation = GameMath.RotationFromVector(velocity);
            glowCount = 0;
            glowColor = new Color();
            Collider = new RectCollider(Animation.CurrentRectangle.Size.ToVector2() * Scale, new Vector2(), this, ColliderType.EnemyShot);
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds);

            HasMoved = true;
            glowCount += dSeconds;
            if (glowCount >= 1) glowCount -= 1;
            float amt = (float)((Math.Sin(glowCount * Math.PI * 8) + 1) / 2);
            Vector3 colorVals = ShotColor.ToVector3();
            glowColor = new Color(colorVals.X * 0.5f + colorVals.X * amt * 0.7f, colorVals.Y * 0.5f + colorVals.Y * amt * 0.7f, colorVals.Z * 0.5f + colorVals.Z * amt * 0.7f);
            Animation.Update(dSeconds);
            if (Type == EnemyShotType.Normal && GameMath.RNGer.Next(8) == 0)
            {
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new FadingAI(0.25f));
                Rectangle currRect = Animation.CurrentRectangle;
                ReturnList.Add(new ParticleEntity(currRect, types, Position + new Vector2( -currRect.Width * Scale.X / 2f + currRect.Width * Scale.X * GameMath.RNGer.NextFloat(),-currRect.Height * Scale.Y / 2f + currRect.Height * Scale.Y * GameMath.RNGer.NextFloat()), Velocity / 2f, ShotColor, Rotation, Scale/2f));
            }
            else if (Type == EnemyShotType.Laser)
            {
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new FadingAI(0.2f));
                Rectangle currRect = Animation.CurrentRectangle;
                ReturnList.Add(new ParticleEntity(currRect, types, Position, new Vector2(), ShotColor, Rotation, Scale / 2f));
            }
        }

        public override void Draw(SpriteBatch b)
        {
            Rectangle currentRect = Animation.CurrentRectangle;
            b.Draw(particles, Position, currentRect, ShotColor, Rotation, new Vector2(currentRect.Width / 2f, currentRect.Height / 2f), Scale, SpriteEffects.None, 0f);
            b.Draw(particles, Position, glowRect, glowColor, Rotation, new Vector2(glowRect.Width / 2f, glowRect.Height / 2f), Scale + new Vector2(1f,1f), SpriteEffects.None, 0f);
        }
        
        public float GetDamage()
        {
            return MinDamage + DamageRange * (float)GameMath.RNGer.NextDouble();
        }
    }
}
