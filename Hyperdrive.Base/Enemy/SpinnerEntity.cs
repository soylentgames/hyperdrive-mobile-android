﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Player;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Enemy.Controllers;
using Hyperdrive.Base.Container;

namespace Hyperdrive.Base.Enemy
{
    public class SpinnerEntity : EnemyEntity
    {
        private float rotationSpeed = 0.12f;
        private double timeSinceTargeted;
        private bool leaving;

        public SpinnerEntity(Vector2 pos, Vector2 vel, float scale) : base(pos, vel)
        {
            SpriteController = new SpinnerSpriteController();
            ShotsPerSecond = 1f;
            ShotForce = 250f;
            ShotScale = 1f;
            MaxHull = 3f + GameMath.RNGer.NextFloat() * 4f;
            Hull = MaxHull;
            Points = (int)MaxHull;  
            Shields = 0f;
            MaxShields = 0f;
            Acceleration = 500f;
            MaxSpeed = 250f;
            MinDamage = 2f;
            MaxDamage = 3f;
            Scale = scale;
            
            timeSinceTargeted = 0;
            leaving = false;
            Collider = new CircleCollider(32f * Scale, new Vector2(), this, ColliderType.Enemy);
        }

        public override void TargetUpdate(PlayerEntity p, float seconds, double dSeconds)
        {
            if (currentTarget == null && GameMath.RNGer.Next(120) == 0) currentTarget = p;
            else if (currentTarget != null)
            {
                if (timeSinceTargeted < 10) timeSinceTargeted += dSeconds;
                else leaving = true;
            }
        }

        public override void Aggro(PlayerEntity p)
        {
            if (currentTarget == null) currentTarget = p;
            else if (currentTarget != null && currentTarget != p) if (GameMath.RNGer.Next(4) == 0) currentTarget = p; //1 in 4 chance to automatically change targets if hit by one
        }

        public override void Update(float seconds, double dSeconds, EnemyShotContainer shotPool)
        {
            base.Update(seconds, dSeconds);  //updates pos and resets ReturnList
            Rotation += rotationSpeed;
            if (currentTarget == null || leaving) Velocity = new Vector2(Velocity.X, Velocity.Y + Acceleration * seconds > MaxSpeed ? MaxSpeed : Velocity.Y + Acceleration * seconds);
            else
            {
                Vector2 norm = currentTarget.Position - Position;
                norm.Normalize();
                Velocity += norm * Acceleration * seconds;
            }

            //velocity cap
            if (Velocity.Length() > MaxSpeed)
            {
                Vector2 vel = Velocity;
                vel.Normalize();
                Velocity = vel * MaxSpeed;
            }
        }

        public override void Draw(SpriteBatch b)
        {
            SpriteController.Draw(b,Position,(float)lastHit,Rotation,ShieldPercentage,Scale);
        }
        
    }
}
