﻿using System.Collections.Generic;
using Hyperdrive.Base.Particle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Particle.AI;
using Hyperdrive.Base.Player;
using Microsoft.Xna.Framework.Audio;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Enemy.Controllers;
using Hyperdrive.Base.UI;
using Hyperdrive.Base.Container;

namespace Hyperdrive.Base.Enemy
{
    public abstract class EnemyEntity : CollidableEntity
    {
        public static Texture2D particles;
        public static SoundEffect fireSound;
        public static SoundEffect laserSound;

        protected EnemySpriteController SpriteController { get; set; }
        protected PlayerEntity currentTarget;
        protected float lastHit;
        public DamageEntity DamageEntity { get; set; }
        public float Scale { get; set; }
        public int Points { get; protected set; }

        //stats 
        public float Hull { get; set; }
        public float MaxHull { get; set; }
        public float Shields { get; set; }
        public float MaxShields { get; set; }
        public float HullPercentage { get { return Hull / MaxHull; } }
        public float ShieldPercentage { get { return MaxShields > 0 ? Shields / MaxShields : 0f; } }
        public float ShotsPerSecond { get; set; }
        public float ShotDelay { get { return 1f / ShotsPerSecond; } }
        public float ShotForce { get; set; }
        public float Acceleration { get; set; }
        public float MaxSpeed { get; set; }
        public float MinDamage { get; set; }
        public float MaxDamage { get; set; }
        public float ShieldRegen { get; set; }
        public float ShotScale { get; set; }
        public bool Destroyed { get; set; }

        public EnemyEntity(Vector2 pos, Vector2 vel) : base(pos, vel)
        {
            lastHit = 2;
            Destroyed = false;
        }

        public virtual void TargetUpdate(PlayerEntity p, float seconds, double dSeconds)
        {
            //requires gameTime so that when it's overriden that can be sent along
            if(currentTarget == null) currentTarget = p;
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds);
            SpriteController.Update(seconds);
            lastHit += seconds;
        }

        public virtual void Update(float seconds, double dSeconds, EnemyShotContainer shotPool)
        {
            Update(seconds, dSeconds);
        }

        public void Hit(float damage, Vector2 shotVelocity, float knockback)
        {
            lastHit = 0;
            if (Shields < damage)
            {
                damage -= Shields;
                Shields = 0;
                if (Hull < damage)
                {
                    Destroyed = true;
                    Hull = 0;
                }
                else
                {
                    Hull -= damage;
                }
            }
            else
            {
                Shields -= damage;
            }

            //knock back enemy
            Vector2 norm = new Vector2(shotVelocity.X, shotVelocity.Y);
            if (shotVelocity.X != 0 || shotVelocity.Y != 0) norm.Normalize();
            Velocity += norm * knockback;

            if (DamageEntity == null || (DamageEntity != null && DamageEntity.Dead))
            {
                DamageEntity = new DamageEntity(Position, damage);
            }
            else
            {
                DamageEntity.AddDamage(damage,Position);
            }
        }

        public void AddBonusPoints(int amt)
        {
            Points += amt;
        }

        public abstract void Aggro(PlayerEntity p);
        
        public List<ParticleEntity> GetDeathParticles()
        {
            List<ParticleEntity> spawnedParticles = new List<ParticleEntity>();
            float explodemod = GameMath.RNGer.NextFloat();

            List<ParticleAI> types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.12f));
            types.Add(new RotatingAI(0.25f));
            types.Add(new FadingAI(0.5f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(320, 0, 64, 64), types, Position, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(0.15f + explodemod * 0.2f, 0.15f + explodemod * 0.2f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.06f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(0.9f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(384, 0, 64, 64), types, Position, new Vector2(), new Color(255, 128, 128), 0f, new Vector2(0.09f + explodemod * 0.18f, 0.09f + explodemod * 0.18f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.04f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(1.2f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(576, 0, 64, 64), types, Position, new Vector2(), new Color(255, 128, 0), 0f, new Vector2(0.3f + explodemod * 0.2f, 0.3f + explodemod * 0.2f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.09f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(1.0f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(576, 0, 64, 64), types, Position, new Vector2(), new Color(255, 192, 64), 0f, new Vector2(0.09f + explodemod * 0.2f, 0.09f + explodemod * 0.2f)));
            
            //shockwave ring
            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.12f));
            types.Add(new RotatingAI(0.25f));
            types.Add(new FadingAI(0.75f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(0, 512, 128, 128), types, Position, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(0.10f + explodemod * 0.2f, 0.10f + explodemod * 0.2f)));

            Rectangle flameRect = new Rectangle(256, 128, 64, 64);
            
            for (int count = 0; count < 5; count++)
            {
                Color color = new Color(255, 192 + GameMath.RNGer.Next(64), 64 + GameMath.RNGer.Next(128));
                types = new List<ParticleAI>();
                types.Add(new RotatingAI(0.05f));
                types.Add(new FadingFlameAI(1.25f,color.ToVector3()));
                types.Add(new ShrinkingAI(2f));
                types.Add(new FlamingAI());
                types.Add(new GradualForceAI(-200f + GameMath.RNGer.NextFloat() * 400f,-200f + GameMath.RNGer.NextFloat() * 400f));
                float sizeMod = GameMath.RNGer.NextFloat();
                spawnedParticles.Add(new ParticleEntity(flameRect, types, Position, new Vector2(-200f + GameMath.RNGer.NextFloat() * 400f, -200f + GameMath.RNGer.NextFloat() * 400f), color, 0f, new Vector2(0.5f + sizeMod, 0.5f + sizeMod)));
            }
            return spawnedParticles;
        }
    }
}
