﻿using Hyperdrive.Base.Gfx;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Hyperdrive.Base.Enemy.Controllers
{
    public class ShootingSpinnerSpriteController : EnemySpriteController
    {
        public ShootingSpinnerSpriteController()
        {
            Animation anim = new Animation();
            anim.AddFrame(new Rectangle(0, 192, 64, 64), TimeSpan.FromSeconds(0.15));
            anim.AddFrame(new Rectangle(64, 192, 64, 64), TimeSpan.FromSeconds(0.15));
            anim.AddFrame(new Rectangle(128, 192, 64, 64), TimeSpan.FromSeconds(0.15));
            anim.AddFrame(new Rectangle(64, 192, 64, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Default, anim);

            flashRect = new Rectangle(192, 192, 64, 64);

            animations.TryGetValue(AnimationType.Default, out currentAnimation);
        }

        public override void Draw(SpriteBatch b, Vector2 position, float lastHit, float rotation, float shieldPct, float size)
        {
            Draw(b, texture, position, lastHit, rotation, shieldPct, size);
        }

    }
}
