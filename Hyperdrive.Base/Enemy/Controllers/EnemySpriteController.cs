﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Hyperdrive.Base.Gfx;

namespace Hyperdrive.Base.Enemy.Controllers
{
    public abstract class EnemySpriteController : SpriteController
    {
        protected Rectangle flashRect;
        public static Texture2D texture;
        
        protected void Draw(SpriteBatch b, Texture2D texture, Vector2 position, float lastHit, float rotation, float shieldPct, float size) //rotation is in radians
        {
            Color c;
            if (lastHit < 0.5f)
            {
                float redAmt = 1.0f - lastHit / 0.5f;
                c = new Color(1f, 1f - redAmt, 1f - redAmt);
            }
            else c = Color;

            b.Draw(texture, position, currentAnimation.CurrentRectangle, c, rotation, new Vector2(currentAnimation.CurrentRectangle.Width / 2, currentAnimation.CurrentRectangle.Height / 2), size, SpriteEffects.None, 0f);
            if (lastHit == 0) b.Draw(texture, position, flashRect, Color.White, rotation, new Vector2(currentAnimation.CurrentRectangle.Width / 2, currentAnimation.CurrentRectangle.Height / 2), size, SpriteEffects.None, 0f);
        }

        public abstract void Draw(SpriteBatch b, Vector2 position, float lastHit, float rotation, float shieldPct, float size);
    }
}
