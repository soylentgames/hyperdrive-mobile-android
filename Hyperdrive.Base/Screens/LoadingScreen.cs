﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Hyperdrive.Base.Enemy;
using Hyperdrive.Base.Inventory.Frames;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Player;
using Hyperdrive.Base.Player.Shots;
using Hyperdrive.Base.Enemy.Shots;
using Hyperdrive.Base.Controls;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.UI;
using Hyperdrive.Base.Enemy.Controllers;

namespace Hyperdrive.Base.Screens
{
    public class LoadingScreen : GameScreen
    {
        private bool isFirstFrame = true;
        private bool fontsLoaded = false;
        private bool soundsLoaded = false;
        private bool texturesLoaded = false;
        private bool songsLoaded = false;
        private bool contentInitialized = false;
        private bool ready = false;
        private double readyCount = 0;
        private double waitCount = 0; //gotta wait for android to actually allow the app to have the screen..it doesn't sometimes for some reason.  I'll give it a second or two
        private Rectangle loadingRect = new Rectangle(0,0,512,512);
        private Vector2 loadingOrigin = new Vector2(256, 256);
        private Vector2 loadingScale = new Vector2(1f, 1f);
        private int frameCount = 0; //for making sure the app actually draws the frame after each section is finished...damn frame skip
        private Texture2D loadingTexture;
        private SpriteFont loadingFont;
        
        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds); //updates sound and screen effects
            if (ready) readyCount += dSeconds;
            if (isFirstFrame)
            {
                waitCount += dSeconds;
            }
            else //load everything here
            {
                frameCount++;
                if (frameCount < 5) return;
                if (!fontsLoaded)
                {
                    //load content - fonts
                    GameDataManager.Fonts.Add("ak12", GameDataManager.Content.Load<SpriteFont>("AkashiSmall"));
                    GameDataManager.Fonts.Add("ak12bold", GameDataManager.Content.Load<SpriteFont>("AkashiSmallBold"));
                    GameDataManager.Fonts.Add("ak14", GameDataManager.Content.Load<SpriteFont>("Akashi"));
                    GameDataManager.Fonts.Add("ak30", GameDataManager.Content.Load<SpriteFont>("AkashiBig"));
                    GameDataManager.Fonts.Add("hs40", GameDataManager.Content.Load<SpriteFont>("Hyperspeed"));
                    loadingFont = GameDataManager.Fonts["ak14"];
                    fontsLoaded = true;
                    frameCount = 0;
                    return;
                }
                if (!soundsLoaded)
                {
                    //load content - sound
                    GameDataManager.Sounds.Add("loaded", GameDataManager.Content.Load<SoundEffect>("loaded"));
                    GameDataManager.Sounds.Add("lock", GameDataManager.Content.Load<SoundEffect>("lock"));
                    GameDataManager.Sounds.Add("beam4", GameDataManager.Content.Load<SoundEffect>("beam4"));
                    GameDataManager.Sounds.Add("logo1", GameDataManager.Content.Load<SoundEffect>("logo1"));
                    GameDataManager.Sounds.Add("logo2", GameDataManager.Content.Load<SoundEffect>("logo2"));
                    GameDataManager.Sounds.Add("logo3", GameDataManager.Content.Load<SoundEffect>("logo3"));
                    GameDataManager.Sounds.Add("logo4", GameDataManager.Content.Load<SoundEffect>("logo4"));
                    GameDataManager.Sounds.Add("plop1", GameDataManager.Content.Load<SoundEffect>("plop1"));
                    GameDataManager.Sounds.Add("fire1", GameDataManager.Content.Load<SoundEffect>("fire1"));
                    GameDataManager.Sounds.Add("blast9", GameDataManager.Content.Load<SoundEffect>("blast9"));
                    GameDataManager.Sounds.Add("boost1", GameDataManager.Content.Load<SoundEffect>("boost1"));
                    GameDataManager.Sounds.Add("boost2", GameDataManager.Content.Load<SoundEffect>("boost2"));
                    GameDataManager.Sounds.Add("enemydeflect", GameDataManager.Content.Load<SoundEffect>("enemydeflect"));
                    GameDataManager.Sounds.Add("playerhit", GameDataManager.Content.Load<SoundEffect>("playerhit"));
                    GameDataManager.Sounds.Add("enemyhit", GameDataManager.Content.Load<SoundEffect>("enemyhit"));
                    GameDataManager.Sounds.Add("enemydestroyed", GameDataManager.Content.Load<SoundEffect>("enemydestroyed"));
                    GameDataManager.Sounds.Add("startgame", GameDataManager.Content.Load<SoundEffect>("startgame"));
                    soundsLoaded = true;
                    frameCount = 0;
                    return;
                }
                if (!texturesLoaded)
                {
                    //load content - textures
                    GameDataManager.Textures.Add("logo", GameDataManager.Content.Load<Texture2D>("logo"));
                    GameDataManager.Textures.Add("particles", GameDataManager.Content.Load<Texture2D>("particles"));
                    GameDataManager.Textures.Add("enemies", GameDataManager.Content.Load<Texture2D>("enemies"));
                    GameDataManager.Textures.Add("meters", GameDataManager.Content.Load<Texture2D>("meters"));
                    GameDataManager.Textures.Add("broken", GameDataManager.Content.Load<Texture2D>("broken"));
                    GameDataManager.Textures.Add("distort", GameDataManager.Content.Load<Texture2D>("distort"));
                    GameDataManager.Textures.Add("xp", GameDataManager.Content.Load<Texture2D>("xp"));
                    GameDataManager.Textures.Add("analog", GameDataManager.Content.Load<Texture2D>("analog"));
                    GameDataManager.Textures.Add("astral1", GameDataManager.Content.Load<Texture2D>("astral1"));
                    GameDataManager.Textures.Add("astral2", GameDataManager.Content.Load<Texture2D>("astral2"));
                    GameDataManager.Textures.Add("astral3", GameDataManager.Content.Load<Texture2D>("astral3"));
                    GameDataManager.Textures.Add("astral4", GameDataManager.Content.Load<Texture2D>("astral4"));
                    GameDataManager.Textures.Add("astral5", GameDataManager.Content.Load<Texture2D>("astral5"));
                    GameDataManager.Textures.Add("astral6", GameDataManager.Content.Load<Texture2D>("astral6"));
                    GameDataManager.Textures.Add("sparrow", GameDataManager.Content.Load<Texture2D>("playersheet"));
                    GameDataManager.Textures.Add("shots", GameDataManager.Content.Load<Texture2D>("shots"));
                    GameDataManager.Textures.Add("title", GameDataManager.Content.Load<Texture2D>("title"));
                    GameDataManager.Textures.Add("buttons", GameDataManager.Content.Load<Texture2D>("buttons"));
                    GameDataManager.Textures.Add("textboxes", GameDataManager.Content.Load<Texture2D>("textboxes"));
                    texturesLoaded = true;
                    frameCount = 0;
                    return;
                }
                if (!songsLoaded)
                {
                    //load content - music
                    GameDataManager.Songs.Add("hyperdrive9", GameDataManager.Content.Load<Song>("hyperdrive9"));
                    GameDataManager.Songs.Add("hyperdrive2", GameDataManager.Content.Load<Song>("hyperdrive2"));
                    GameDataManager.Songs.Add("death", GameDataManager.Content.Load<Song>("death"));
                    GameDataManager.Songs.Add("Activate", GameDataManager.Content.Load<Song>("Activate"));
                    GameDataManager.Songs.Add("Dark History", GameDataManager.Content.Load<Song>("Dark History"));
                    songsLoaded = true;
                    frameCount = 0;
                    return;
                }
                if (!contentInitialized)
                {
                    //load content-dependent static variables first thing to prevent slowdown ingame
                    AnalogControlCircle.sprite = GameDataManager.Textures["analog"];
                    CollisionManager.playerHitSound = GameDataManager.Sounds["playerhit"];
                    CollisionManager.enemyHitSound = GameDataManager.Sounds["enemyhit"];
                    CollisionManager.enemyDestroyedSound = GameDataManager.Sounds["enemydestroyed"];
                    DamageEntity.font = GameDataManager.Fonts["ak12"];
                    DamageEntity.boldFont = GameDataManager.Fonts["ak12bold"];
                    EnemyEntity.particles = GameDataManager.Textures["particles"];
                    EnemyEntity.fireSound = GameDataManager.Sounds["fire1"];
                    EnemyEntity.laserSound = GameDataManager.Sounds["beam4"];
                    EnemyShotEntity.particles = GameDataManager.Textures["particles"];
                    EnemySpriteController.texture = GameDataManager.Textures["enemies"];
                    ExperienceBarEntity.texture = GameDataManager.Textures["xp"];
                    ExperienceBarEntity.ak12 = GameDataManager.Fonts["ak12"];
                    ExperienceBarEntity.ak14 = GameDataManager.Fonts["ak14"];
                    ExperienceEntity.font = GameDataManager.Fonts["ak12bold"];
                    FireButtonControlCircle.sprite = GameDataManager.Textures["analog"];
                    FrameItem.particles = GameDataManager.Textures["particles"];
                    FrameItem.standardFireSound = GameDataManager.Sounds["fire1"];
                    FrameItem.boostSound = GameDataManager.Sounds["boost1"];
                    FrameItem.boostSustainSound = GameDataManager.Sounds["boost2"];
                    LifeBarEntity.texture = GameDataManager.Textures["meters"];
                    LifeBarEntity.font = GameDataManager.Fonts["ak14"];
                    ParticleEntity.texture = GameDataManager.Textures["particles"];
                    PlayerShotEntity.particles = GameDataManager.Textures["particles"];
                    SparrowSpriteController.texture = GameDataManager.Textures["sparrow"];
                    TButton.texture = GameDataManager.Textures["buttons"];
                    TTextbox.texture = GameDataManager.Textures["textboxes"];
                    GameScreen.particles = GameDataManager.Textures["particles"];
                    contentInitialized = true;
                }
            }

            if (!ready && fontsLoaded && soundsLoaded && texturesLoaded && songsLoaded && contentInitialized)
            {
                ready = true;
                GameDataManager.AddSound(GameDataManager.Sounds["loaded"], 0f, 1f);
            }
            if (ready && readyCount >= 1f)
            {
                NextScreen = new LogoScreen();
            }
        }

        public override void Draw()
        {
            if (isFirstFrame)
            {
                GameDataManager.Batch.Draw(loadingTexture, new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2), loadingRect, Color.White, 0, loadingOrigin, loadingScale, SpriteEffects.None, 0);
                if (waitCount >= 2) isFirstFrame = false;
                return;
            }
            if (!ready)
            {
                GameDataManager.Batch.Draw(loadingTexture, new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2), loadingRect, Color.White, 0, loadingOrigin, loadingScale, SpriteEffects.None,0);
                if (fontsLoaded) GameDataManager.Batch.DrawString(loadingFont, "Fonts Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Fonts Loaded!").X, 10), new Color(1.0f, 0f, 0f));
                if (soundsLoaded) GameDataManager.Batch.DrawString(loadingFont, "Sounds Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Sounds Loaded!").X, 30), new Color(1.0f, 1.0f, 0f));
                if (texturesLoaded) GameDataManager.Batch.DrawString(loadingFont, "Textures Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Textures Loaded!").X, 50), new Color(0f, 1.0f, 0f));
                if (songsLoaded) GameDataManager.Batch.DrawString(loadingFont, "Songs Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Songs Loaded!").X, 70), new Color(0f, 1.0f, 1.0f));
                if (contentInitialized) GameDataManager.Batch.DrawString(loadingFont, "Content Initialized!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Content Initialized!").X, 90), new Color(0f, 0f, 1.0f));
            }
            else
            {
                float amt = (float)readyCount;
                GameDataManager.Batch.Draw(loadingTexture, new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2), loadingRect, new Color(1.0f - amt, 1.0f - amt, 1.0f - amt), 0, loadingOrigin, loadingScale, SpriteEffects.None, 0);
                GameDataManager.Batch.DrawString(loadingFont, "Fonts Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Fonts Loaded!").X, 10), new Color(1.0f - amt, 0f, 0f));
                GameDataManager.Batch.DrawString(loadingFont, "Sounds Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Sounds Loaded!").X, 30), new Color(1.0f - amt, 1.0f - amt, 0f));
                GameDataManager.Batch.DrawString(loadingFont, "Textures Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Textures Loaded!").X, 50), new Color(0f, 1.0f - amt, 0f));
                GameDataManager.Batch.DrawString(loadingFont, "Songs Loaded!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Songs Loaded!").X, 70), new Color(0f, 1.0f - amt, 1.0f - amt));
                GameDataManager.Batch.DrawString(loadingFont, "Content Initialized!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - loadingFont.MeasureString("Content Initialized!").X, 90), new Color(0f, 0f, 1.0f - amt));
            }
        }

        public override void Init()
        {
            GameDataManager.Textures.Add("loading", GameDataManager.Content.Load<Texture2D>("loading"));
            loadingTexture = GameDataManager.Textures["loading"];
        }

        public override void Deactivated()
        { }

        public override void OnKeyPress(TKeyPress keyPress) 
        { }
    }
}
