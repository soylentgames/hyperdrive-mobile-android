﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Particle.AI;
using Microsoft.Xna.Framework.Input.Touch;
using Hyperdrive.Base.Controls;

namespace Hyperdrive.Base.Screens
{
    public class LogoScreen : GameScreen
    {
        double timeCount;
        SpriteFont ak14;
        SpriteFont ak30;
        Texture2D logoTexture;
        Rectangle logoRectFull;
        Rectangle logoRectBitten;
        Rectangle logoRectDroplet;
        Rectangle logoRectCircle;
        Rectangle logoRectBlack;
        List<ParticleEntity> particleList;
        Vector2 centerOfScreen;
        Vector2 mainTextOffset;
        Vector2 subTextOffset;
        Vector2 subTextSize;
        Vector2 mainTextSize;

        Color c;
        int count; //keeps track of logo progress

        public LogoScreen()
        {
            count = -1; //count controls the progress of the logo animation
        }

        public override void Init()
        {
            logoRectFull = new Rectangle(0, 0, 512, 512);
            logoRectBitten = new Rectangle(512, 0, 512, 512);
            logoRectDroplet = new Rectangle(0, 512, 64, 64);
            logoRectCircle = new Rectangle(192, 0, 64, 64);
            logoRectBlack = new Rectangle(128, 512, 64, 64);
            
            ak14 = GameDataManager.Fonts["ak14"];
            ak30 = GameDataManager.Fonts["ak30"];
            logoTexture = GameDataManager.Textures["logo"];

            //determine drawing locs
            subTextSize = ak14.MeasureString("SOYLENT GAMES");
            mainTextSize = ak30.MeasureString("IT'S PEOPLE!");
            centerOfScreen = new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2);
            mainTextOffset = new Vector2(-52.1f,123.7f); //took extensive testing to get this right since SpriteFont.MeasureText() doesn't work right
            subTextOffset = new Vector2(14.9f, 162.5f);
            c = new Color(0.5f, 1f, 0.5f);

            particleList = new List<ParticleEntity>();
            for (int i = 0;i < 70; i++)
            {
                float sizef = 0.1f + GameMath.RNGer.NextFloat() * 0.15f;
                Vector2 size = new Vector2(sizef, sizef);
                Vector2 loc = new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2 + 192f * GameMath.RNGer.NextFloat(), GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2 - 192f * GameMath.RNGer.NextFloat());
                Vector2 vel = new Vector2((GameMath.RNGer.NextFloat() - 0.3f) * 250f, (GameMath.RNGer.NextFloat() - 0.8f) * 350f);
                Color color = new Color(0.8f + 0.2f * GameMath.RNGer.NextFloat(), GameMath.RNGer.NextFloat() * 0.1f, GameMath.RNGer.NextFloat() * 0.1f, 0.9f);
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new RotatingAI(1f));
                types.Add(new StandardGravitationAI());
                types.Add(new LoadScreenFlyingAI());
                ParticleEntity p = new ParticleEntity(logoRectCircle, types, loc, vel, color, 0, size);
                particleList.Add(p);
            }
            count = -1;
            InitComplete = true;
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds); //updates sound and screen effects
            if (!InitComplete) return; //don't do anything until it's definitely all loaded

            timeCount += dSeconds;

            List<ParticleEntity> trash = new List<ParticleEntity>();
            List<ParticleEntity> spawned = new List<ParticleEntity>();
            if (count >= 4) foreach (ParticleEntity p in particleList)
                {
                    p.Update(seconds, dSeconds);
                    List<ParticleEntity> newParticles = p.ReturnList;
                    if (p.Dead) trash.Add(p);
                    if (newParticles != null) foreach (ParticleEntity sp in newParticles) spawned.Add(sp);
                }
            foreach (ParticleEntity p in trash) particleList.Remove(p);
            foreach (ParticleEntity p in spawned) particleList.Add(p);

            if (timeCount >= 3 && count == -1)
            {
                timeCount = 0;
                count = 0;
            }
            else if (timeCount > 0 && count == 0)
            {
                timeCount = 0;
                count = 1;
                GameDataManager.AddSound(GameDataManager.Sounds["logo1"], 0f, 1f);
            }
            else if (timeCount >= 0.5 && count == 1)
            {
                timeCount = 0;
                count = 2;
                GameDataManager.AddSound(GameDataManager.Sounds["logo2"], 0f, 1f);
            }
            else if (timeCount >= 0.5 && count == 2)
            {
                timeCount = 0;
                count = 3;
                GameDataManager.AddSound(GameDataManager.Sounds["logo3"], 0f, 1f);
            }
            else if (timeCount >= 1 && count == 3)
            {
                timeCount = 0;
                count = 4;
                GameDataManager.AddSound(GameDataManager.Sounds["logo4"], 0f, 1f);
            }
            else if (timeCount >= 0.5 && count == 4)
            {
                timeCount = 0;
                count = 5;
            }
            else if (timeCount >= 2.5 && count == 5)
            {
                foreach (ParticleEntity p in particleList) p.Update(seconds, dSeconds);
                count = 6;
                timeCount = 0;
            }
            else if (timeCount >= 0.5 && count == 6)
            {
                NextScreen = new TitleScreen();
            }
            TouchCollection tc = TouchPanel.GetState();
            foreach (TouchLocation t in tc)
            {
                if (count >= 5)
                {
                    count = 6;
                    timeCount = 0.4999999999;
                    break;
                }
            }
        }

        public override void Draw()
        {
            if (!InitComplete) return;
            
            switch (count)
            {
                case 1:
                    GameDataManager.Batch.Draw(logoTexture, centerOfScreen, logoRectFull, c, 0, new Vector2(logoRectFull.Width / 2, logoRectFull.Height / 2), 1.0f, SpriteEffects.None, 0f);
                    break;
                case 2:
                    GameDataManager.Batch.Draw(logoTexture, centerOfScreen, logoRectFull, c, 0, new Vector2(logoRectFull.Width / 2, logoRectFull.Height / 2), 1.0f, SpriteEffects.None, 0f);
                    GameDataManager.Batch.DrawString(ak30, "SOYLENT", centerOfScreen + mainTextOffset, c, 0, mainTextSize / 2, 1.0f, SpriteEffects.None, 0);
                    break;
                case 3:
                    GameDataManager.Batch.Draw(logoTexture, centerOfScreen, logoRectFull, c, 0, new Vector2(logoRectFull.Width / 2, logoRectFull.Height / 2), 1.0f, SpriteEffects.None, 0f);
                    GameDataManager.Batch.DrawString(ak30, "SOYLENT GAMES", centerOfScreen + mainTextOffset, c, 0, mainTextSize / 2, 1.0f, SpriteEffects.None, 0);
                    break;
                case 4: case 5: case 6:
                    Vector2 offset = count == 4 ? new Vector2(4f + GameMath.RNGer.NextFloat() * -8f, 4f + GameMath.RNGer.NextFloat() * -8f) : new Vector2();
                    float prog = count == 4 ? (float)timeCount / 0.5f : 1f;

                    Color color = count == 4 ? new Color(1.0f - prog * 0.5f,0.5f + prog * 0.5f,0.5f) : c;

                    GameDataManager.Batch.Draw(logoTexture, centerOfScreen + offset, logoRectBitten, color, 0, new Vector2(logoRectFull.Width / 2, logoRectFull.Height / 2), 1.0f, SpriteEffects.None, 0f);
                    GameDataManager.Batch.Draw(logoTexture, centerOfScreen + offset + new Vector2(15f + prog * 15f,-(70f + 50f * prog)), logoRectDroplet, color, -0.174533f + 0.261799f * prog, new Vector2(logoRectDroplet.Width / 2, logoRectDroplet.Height / 2), 1.0f, SpriteEffects.None, 0f);
                    GameDataManager.Batch.Draw(logoTexture, centerOfScreen + offset + new Vector2(65f + prog * 15f, -(45f + 50f * prog)), logoRectDroplet, color, 0.523599f * prog, new Vector2(logoRectDroplet.Width / 2, logoRectDroplet.Height / 2), 1.0f, SpriteEffects.None, 0f);
                    GameDataManager.Batch.Draw(logoTexture, centerOfScreen + offset + new Vector2(115f + prog * 15f, -(20f + 50f * prog)), logoRectDroplet, color, 1.0472f * prog, new Vector2(logoRectDroplet.Width / 2, logoRectDroplet.Height / 2), 1.0f, SpriteEffects.None, 0f);

                    foreach (ParticleEntity p in particleList) p.Draw(GameDataManager.Batch);

                    GameDataManager.Batch.DrawString(ak30, "SOYLENT GAMES", centerOfScreen + mainTextOffset + offset, color, 0, mainTextSize / 2, 1.0f, SpriteEffects.None, 0);
                    GameDataManager.Batch.DrawString(ak14, "IT'S PEOPLE!", centerOfScreen + subTextOffset + offset, color, 0, subTextSize / 2, 1.0f, SpriteEffects.None, 0);
                    //draw bit brain and all text and particles

                    if (count == 6) GameDataManager.Batch.Draw(logoTexture, new Rectangle(0, 0, GameDataManager.Gfx.GraphicsDevice.Viewport.Width,GameDataManager.Gfx.GraphicsDevice.Viewport.Height), logoRectBlack, new Color(1.0f, 1.0f, 1.0f, (float)timeCount / 0.5f));
                    break;
            }
        }

        public override void Deactivated()
        {

        }

        public override void OnKeyPress(TKeyPress keyPress)
        { }
    }
}
