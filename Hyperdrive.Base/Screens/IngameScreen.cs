﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Enemy;
using Microsoft.Xna.Framework.Input.Touch;
using Android.OS;
using Android.App;
using Android.Content;
using Microsoft.Xna.Framework.Media;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Particle.AI;
using Hyperdrive.Base.Player;
using Hyperdrive.Base.Inventory.Frames;
using Hyperdrive.Base.Controls;
using Hyperdrive.Base.UI;
using Hyperdrive.Base.Container;

namespace Hyperdrive.Base.Screens
{
    public class IngameScreen : GameScreen
    {
        //only load objects the screen directly needs here, the rest should be contained in objects
        PlayerEntity player;
        Song bgMusic;
        Song deathMusic;
        SpriteFont ak12; //TODO:  Delete these when done with debugging, as all text drawing should be done in objects
        SpriteFont ak14;
        SpriteFont ak30;
        SpriteFont hs40;
        Texture2D distortTexture; //TODO:  replace these two with a cooler death effect later
        Texture2D brokenTexture;

        //controller objects & vars
        AnalogControlCircle mainAnalog;
        FireButtonControlCircle fireButton;
        LifeBarEntity lifeBar;
        ExperienceBarEntity xpBar;
        Vibrator vibe;
        bool canVibrate;

        //regular vars
        private double destroyedTime;
        private bool playerDead;
        private bool introSound;
        private bool justSpawned = true; //indicates the spawn animation is in progress
        private Rectangle fullScreen;
        private double lastFrameTime;
        private float spawnedTime = 0; //time since player spawned
        private float lastSpawned = 0; //time since the last enemy was spawned
        bool isPaused = false;

        //temp vars - for testing

        //containers (layers)
        EntityContainer players; //this one isn't really necessary unless I somehow add networking :3
        PlayerShotContainer playerShots;
        EnemyEntityContainer enemies;
        EnemyShotContainer enemyShots;
        DamageEntityContainer damages;
        EntityContainer experiences;
        ParticleEntityContainer foreParticles; //in front of everything
        ParticleEntityContainer foreParticlesAdditive;
        ParticleEntityContainer midParticles; //in front of enemies, behind players
        ParticleEntityContainer midParticlesAdditive;
        ParticleEntityContainer rearParticles; //behind everything (drawn earliest)
        ParticleEntityContainer rearParticlesAdditive;
        AstralEntityContainer astrals; //spacey stuff in the background

        public IngameScreen()
        {
            //set up containers
            playerShots = new PlayerShotContainer();
            enemies = new EnemyEntityContainer();
            enemyShots = new EnemyShotContainer();
            damages = new DamageEntityContainer();
            experiences = new EntityContainer();
            rearParticles = new ParticleEntityContainer();
            midParticles = new ParticleEntityContainer();
            midParticlesAdditive = new ParticleEntityContainer();
            foreParticlesAdditive = new ParticleEntityContainer();
            astrals = new AstralEntityContainer();
        }

        public override void Init()
        {
            //load system devices
            vibe = (Vibrator)Application.Context.GetSystemService(Context.VibratorService);
            if (!vibe.HasVibrator) canVibrate = false;
            else canVibrate = true;
            
            //content is assured to be loaded now since it is loaded on loading screen (as long as I remember to add it there :D)
            player = new PlayerEntity(new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height));

            //gui stuffs
            lifeBar = new LifeBarEntity(new Vector2(20, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 192));
            xpBar = new ExperienceBarEntity(new Vector2(192, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 84));

            //init controls and vars
            mainAnalog = new AnalogControlCircle();
            fireButton = new FireButtonControlCircle();
            destroyedTime = 0;
            playerDead = false;
            fullScreen = new Rectangle(0, 0, GameDataManager.Gfx.GraphicsDevice.Viewport.Width, GameDataManager.Gfx.GraphicsDevice.Viewport.Height);
            ChooseMusic();
            deathMusic = GameDataManager.Songs["death"];
            ak12 = GameDataManager.Fonts["ak12"];
            ak14 = GameDataManager.Fonts["ak14"]; //this was already loaded on logo screen
            ak30 = GameDataManager.Fonts["ak30"];
            hs40 = GameDataManager.Fonts["hs40"];
            brokenTexture = GameDataManager.Textures["broken"];
            distortTexture = GameDataManager.Textures["distort"];

            //start music and get it goin
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(bgMusic);
            InitComplete = true;
        }

        private void ChooseMusic()
        {
            if (GameMath.RNGer.NextBoolean()) bgMusic = GameDataManager.Songs["hyperdrive9"];
            else bgMusic = GameDataManager.Songs["hyperdrive2"];

        }

        public override void Update(float seconds, double dSeconds)
        {
            if (!InitComplete) return;

            base.Update(seconds, dSeconds); //updates sound/gamedatamanger

            GameDataManager.Grid.Clear(); //clear grid so enemies can add themselves as they update

            lastFrameTime = dSeconds;
            
            TouchCollection tc = TouchPanel.GetState();
            
            //check for intentional pause
            foreach (TouchLocation t in tc) if ((new Vector2(0, 0) - t.Position).Length() < 96 && t.State == TouchLocationState.Pressed)
                {
                    if (isPaused) MediaPlayer.Volume = 1f;
                    else MediaPlayer.Volume = 0.33f;
                    isPaused = !isPaused;
                    
                }
            else if ((new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width, 0) - t.Position).Length() < 96 && t.State == TouchLocationState.Pressed && isPaused)
                {
                    NextScreen = new LogoScreen();
                }
           

            if (justSpawned) 
            {
                player.Velocity = new Vector2(0, ((3f - spawnedTime) / 3f) * -250f);
                spawnedTime += seconds;
                if (spawnedTime >= 0.5f && !introSound)
                {
                    introSound = true;
                    GameDataManager.AddSound(GameDataManager.Sounds["lock"],0f,1f);
                }
                if (spawnedTime >= 3f) justSpawned = false;
            }

            //Player Input 
            mainAnalog.Update();
            fireButton.Update();

            mainAnalog.GetInput(tc);
            fireButton.GetInput(tc);

            if (!isPaused)
            {
                lastSpawned += seconds;

                //update astrals
                astrals.Update(seconds, dSeconds);

                //update particles 
                rearParticles.AddEntities(rearParticles.Update(seconds, dSeconds)); //adds particles generated by particles to their own pool

                midParticles.AddEntities(midParticles.Update(seconds, dSeconds));

                midParticlesAdditive.AddEntities(midParticlesAdditive.Update(seconds, dSeconds));

                foreParticlesAdditive.AddEntities(foreParticlesAdditive.Update(seconds, dSeconds));

                damages.Update(seconds, dSeconds);

                experiences.Update(seconds, dSeconds);

                // Enemy Bullets
                midParticlesAdditive.AddEntities(enemyShots.Update(seconds, dSeconds));

                // Enemies
                enemies.Update(seconds, dSeconds, player, enemyShots);
                
                // Bullets After Enemies
                midParticlesAdditive.AddEntities(playerShots.Update(seconds, dSeconds));

                FrameItem f = player.Owner.EquippedFrame;
                if (!playerDead) //TODO move this stuff into the player object
                {
                    //Player Update
                    player.Update(seconds, dSeconds, fireButton.IsButtonDown, mainAnalog.CurrentDirection, playerShots, mainAnalog.IsBoosting);
                    midParticles.AddEntities(player.ReturnList);
                    
                }
                else destroyedTime += dSeconds;

                bool playerLeveled;
                bool playerHit;
                bool playerDestroyed = false;
                GameDataManager.CollisionManager.DoCollisions(GameDataManager.Grid, playerShots, enemyShots, enemies, midParticlesAdditive, damages, experiences, out playerLeveled, out playerHit); 

                if (playerLeveled) xpBar.LevelUp();
                if (playerHit)
                {
                    if (canVibrate) vibe.Vibrate(20);
                    if (f.Shields <= 0) AddScreenEffect(ScreenEffect.Flash, 1f, Color.Red, 0.5f);
                    if (f.Destroyed) playerDestroyed = true;
                }
                if (playerDestroyed)
                {
                    AddScreenEffect(ScreenEffect.Flash, 1f, Color.Red);
                    MediaPlayer.IsRepeating = false;
                    MediaPlayer.Play(deathMusic);
                    playerDead = true;
                }

                lifeBar.Update(f.HullPercentage, f.ShieldPercentage, f.PowerPercentage, f.BoostPercentage, f.Hull.ToString("0.00") + " / " + f.MaxHull.ToString("0.00"), f.Shields.ToString("0.00") + " / " + f.MaxShields.ToString("0.00"), f.Power.ToString("0.00") + " / " + f.ActualPowerCapacity.ToString("0.00") + " (-" + f.PassivePowerConsumption.ToString("0.00") + ")");
                xpBar.Update(player.Owner.ExperiencePercentage, player.Owner.Score + " / " + player.Owner.ScoreForNextLevel, player.Owner.Level, dSeconds);

                if (destroyedTime >= 5f) //5 seconds dead, then respawn
                {
                    player.Respawn();
                    playerShots.Clear();
                    enemies.Clear();
                    rearParticles.Clear();
                    midParticles.Clear();
                    midParticlesAdditive.Clear();
                    foreParticlesAdditive.Clear();
                    enemyShots.Clear();
                    astrals.Clear();
                    playerDead = false;
                    destroyedTime = 0;
                    spawnedTime = 0;
                    lastSpawned = 0;
                    justSpawned = true;
                    introSound = false;
                    ChooseMusic();
                    MediaPlayer.IsRepeating = true;
                    MediaPlayer.Play(bgMusic);
                }

                //spawn enemies (test mode, will be replaced by a set list for each level)
                if (!justSpawned && lastSpawned > 1f + GameMath.RNGer.NextFloat() * 3f)
                {
                    lastSpawned = 0;
                    int draw = GameMath.RNGer.Next(100);
                    if (draw < 4) enemies.AddEntity(new LaseringSpinnerEntity(new Vector2(GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width, -100), new Vector2(), 1f, 0.005f + GameMath.RNGer.NextFloat() * 0.045f, 1 + GameMath.RNGer.Next(3)));
                    else if (draw < 6)
                    {
                        SpinnerEntity s = new SpinnerEntity(new Vector2(GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width, -100), new Vector2(), 1.5f);
                        s.AddBonusPoints(20);
                        enemies.AddEntity(s);
                    }
                    else if (draw < 15) enemies.AddEntity(new ShooterEntity(new Vector2(GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width, -100), new Vector2(), 1f, 1f + GameMath.RNGer.NextFloat() * 2f, 2 + GameMath.RNGer.Next(5)));
                    else if (draw < 20) enemies.AddEntity(new ShootingSpinnerEntity(new Vector2(GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width, -100), new Vector2(), 1f, 0.01f + GameMath.RNGer.NextFloat() * 0.09f, 0.15f + GameMath.RNGer.NextFloat() * 0.35f, 1 + GameMath.RNGer.Next(3)));
                    else enemies.AddEntity(new SpinnerEntity(new Vector2(GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width, -100), new Vector2(), 1f));
                    
                }
                //spawn stars
                if (GameMath.RNGer.Next(3) == 0)
                {
                    int starDraw = GameMath.RNGer.Next(3);
                    Rectangle particleRect = new Rectangle(0, 64, 64, 64);
                    switch (starDraw)
                    {
                        case 0:
                            particleRect = new Rectangle(0, 64, 64, 64);
                            break;
                        case 1:
                            particleRect = new Rectangle(64, 64, 64, 64);
                            break;
                        case 2:
                            particleRect = new Rectangle(128, 64, 64, 64);
                            break;
                    }

                    List<ParticleAI> types = new List<ParticleAI>();
                    types.Add(new PulsingAI(1.05f, 16));

                    float scaleF = 0.02f + GameMath.RNGer.NextFloat() * 0.08f;
                    Vector2 scale = new Vector2(scaleF, scaleF);
                    rearParticles.AddEntity(new ParticleEntity(particleRect, types, new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width * GameMath.RNGer.NextFloat(), 0), new Vector2(0, 200 + GameMath.RNGer.NextFloat() * 500), Color.White, GameMath.RNGer.NextFloat() * 6.28319f, scale));
                }
            }  //pause bracket ends here
        }

        public override void Draw()
        {
            if (!InitComplete) return;

            // Add your drawing code here
            astrals.Draw(GameDataManager.Batch);
            rearParticles.Draw(GameDataManager.Batch);
            enemies.Draw(GameDataManager.Batch);

            midParticles.Draw(GameDataManager.Batch);

            GameDataManager.Batch.End();
            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.Additive);

            enemyShots.Draw(GameDataManager.Batch);
            playerShots.Draw(GameDataManager.Batch);
            midParticlesAdditive.Draw(GameDataManager.Batch);

            GameDataManager.Batch.End();
            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            if (!playerDead)
            {
                player.Draw(GameDataManager.Batch);
                FrameItem f = player.Owner.EquippedFrame;
                Vector2 offset = (f.SpriteController.LastHit < 0.5f) ? new Vector2(GameMath.RNGer.Next(21) - 10, GameMath.RNGer.Next(21) - 10) : new Vector2(0, 0);
                mainAnalog.Draw(GameDataManager.Batch, offset);
                fireButton.Draw(GameDataManager.Batch, offset);
                lifeBar.Draw(GameDataManager.Batch, offset);
                xpBar.Draw(GameDataManager.Batch, offset);
            }

            GameDataManager.Batch.End();
            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.Additive);

            foreParticlesAdditive.Draw(GameDataManager.Batch);

            damages.Draw(GameDataManager.Batch);

            experiences.Draw(GameDataManager.Batch);

            GameDataManager.Batch.End();
            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            
            GameDataManager.Batch.DrawString(ak12, "Grid Count: " + GameDataManager.Grid.DebugItemCount(), new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - 280, 20), Color.Yellow);

            if (justSpawned)
            {
                if (spawnedTime < 2.5f)
                {
                    float travelTime = 0.5f;
                    Vector2 stringSize = hs40.MeasureString("Enemy Rush Mode");
                    Vector2 stringSizeEntering = ak30.MeasureString("Now Entering:");
                    GameDataManager.Batch.DrawString(ak30, "Now Entering:", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2 - 40), new Color(0.5f, 0.8f, 0f), 0, stringSizeEntering / 2, 1f, SpriteEffects.None, 0); ;
                    GameDataManager.Batch.DrawString(hs40, "Enemy Rush Mode", new Vector2(spawnedTime < travelTime ? GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2 + GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2 * (1f - spawnedTime / travelTime) : GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2), new Color(0.8f, 1f, 0.45f), 0, stringSize / 2, 1f, SpriteEffects.None, 0);
                }
                else
                {
                    Vector2 stringSize = hs40.MeasureString("START!");
                    GameDataManager.Batch.DrawString(hs40, "START!", new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2), new Color(0.45f, 1f, 0.2f), 0, stringSize / 2, 1f + (spawnedTime - 2.5f) * 3f, SpriteEffects.None, 0);
                }
            }
            if (isPaused)
            {
                Vector2 origin = ak30.MeasureString("PAUSED") / 2f;
                GameDataManager.Batch.DrawString(ak30, "PAUSED", GameDataManager.Gfx.GraphicsDevice.Viewport.Bounds.Size.ToVector2() / 2f + new Vector2(2,2), Color.Black, 0, origin, 1f, SpriteEffects.None, 0); 
                GameDataManager.Batch.DrawString(ak30, "PAUSED", GameDataManager.Gfx.GraphicsDevice.Viewport.Bounds.Size.ToVector2() / 2f, Color.Red, 0, origin, 1f, SpriteEffects.None, 0);
            }

            GameDataManager.Batch.End();
            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.Additive);

            DrawScreenEffects();

            GameDataManager.Batch.End();
            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            if (playerDead)
            {
                if (GameMath.RNGer.Next(15) == 0) GameDataManager.Batch.Draw(distortTexture, fullScreen, new Rectangle(0, 0, 1024, 1024), Color.White, 0, new Vector2(), SpriteEffects.None, 0);
                GameDataManager.Batch.Draw(brokenTexture, fullScreen, new Rectangle(0, 0, 1024, 1024), Color.White, 0, new Vector2(), SpriteEffects.None, 0);
            }
        }

        public override void Deactivated()
        {
            //pause automatically when window isn't active
            if (!isPaused)
            {
                MediaPlayer.Volume = 0.33f;
                isPaused = true;
            }
        }

        public override void OnKeyPress(TKeyPress keyPress)
        { }
    }
}
