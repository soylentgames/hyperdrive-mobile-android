﻿using Hyperdrive.Base.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Hyperdrive.Base.Screens
{
    public enum ScreenEffect
    {
        None,
        FadingIn,
        FadingOut,
        Flash
    }

    public abstract class GameScreen
    {
        public static Texture2D particles;
        
        public GameScreen NextScreen { get; protected set; }
        public bool ChangingScreen { get { return NextScreen != null; } }
        public bool InitComplete { get; set; }

        public ScreenEffect CurrentEffect { get; set; }
        public Color CurrentEffectColor { get; set; }
        public float CurrentEffectLength { get; set; }
        public float CurrentEffectProgress { get; set; }

        public UIElement Focus { get; set; }

        public GameScreen()
        {
            NextScreen = null;
            InitComplete = false;
            CurrentEffect = ScreenEffect.None;
        }

        public abstract void Init();
        public abstract void Deactivated(); //use this to catch when the window is deactivated

        public virtual void Update(float seconds, double dSeconds)
        {
            if (CurrentEffect != ScreenEffect.None)
            {
                CurrentEffectProgress += seconds;
                if (CurrentEffectProgress >= CurrentEffectLength)
                {
                    CurrentEffect = ScreenEffect.None;
                    CurrentEffectProgress = 0;
                }
            }
        }

        public virtual void AddScreenEffect(ScreenEffect type, float length, Color color)
        {
            CurrentEffect = type;
            CurrentEffectLength = length;
            CurrentEffectColor = color;
        }

        public virtual void AddScreenEffect(ScreenEffect type, float length, Color color, float startProgress)
        {
            AddScreenEffect(type, length, color);
            CurrentEffectProgress = startProgress;
        }

        public virtual void DrawScreenEffects()
        {
            if (CurrentEffect != ScreenEffect.None)
            {
                switch (CurrentEffect)
                {
                    case ScreenEffect.Flash:
                        {
                            GameDataManager.Batch.End();
                            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
                            
                            float progress = 1f - CurrentEffectProgress / CurrentEffectLength;
                            Vector3 colorVect = CurrentEffectColor.ToVector3();
                            Color actualColor = new Color(colorVect.X * progress, colorVect.Y * progress, colorVect.Z * progress);
                            GameDataManager.Batch.Draw(particles, GameDataManager.Gfx.GraphicsDevice.Viewport.Bounds, new Rectangle(320, 128, 64, 64), actualColor);

                            GameDataManager.Batch.End();
                            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                        }
                        break;
                    case ScreenEffect.FadingOut:
                        {
                            float progress = CurrentEffectProgress / CurrentEffectLength;
                            Color actualColor = new Color(0, 0, 0, progress);
                            GameDataManager.Batch.Draw(particles, GameDataManager.Gfx.GraphicsDevice.Viewport.Bounds, new Rectangle(320, 128, 64, 64), actualColor);
                        }
                        break;
                }
            }
        }

        public abstract void OnKeyPress(TKeyPress keyPress); //receive on-screen keyboard input

        public abstract void Draw();
       
    }
}
