﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Particle;
using System.Collections.Generic;
using Hyperdrive.Base.Particle.AI;
using Microsoft.Xna.Framework.Media;
using Hyperdrive.Base.Controls;
using Microsoft.Xna.Framework.Input.Touch;
using Hyperdrive.Base.Container;

namespace Hyperdrive.Base.Screens
{
    public class TitleScreen : GameScreen
    {
        private float animationProgress = 0;
        private float topPointY;
        private float bottomPointY;
        private float centerPointX;
        private float centerPointY;
        private float travelDistance;
        private float widestSide;
        private float warpTime;
        private Vector2 titleLocation;
        private bool finishedWarp = false;
        private bool isStarting = false;
        private int colorCount = 0;
        private Texture2D titleTexture;
        private ParticleEntityContainer particleList;
        private Rectangle titleRect = new Rectangle(127, 4, 778, 161);
        private Rectangle titleShadowRect = new Rectangle(127, 171, 778, 161);
        private Rectangle titleBurstRect = new Rectangle(0, 512, 1024, 512);
        private Rectangle titleBurstCoreRect = new Rectangle(510, 766, 4, 4);
        private Song titleTheme;
        private TButton startButton;
        private TTextbox txtUsername;

        public override void Init()
        {
            MediaPlayer.Stop();
            titleTexture = GameDataManager.Textures["title"];
            titleTheme = GameDataManager.Songs["Activate"];
            particleList = new ParticleEntityContainer();
            topPointY = 90f;
            warpTime = 0.2f;
            bottomPointY = GameDataManager.Gfx.GraphicsDevice.Viewport.Height * 0.75f;
            centerPointX = GameDataManager.Gfx.GraphicsDevice.Viewport.Width * 0.5f;
            centerPointY = GameDataManager.Gfx.GraphicsDevice.Viewport.Height * 0.5f;
            travelDistance = bottomPointY - topPointY;
            titleLocation = new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width * 0.5f, topPointY);
            widestSide = GameDataManager.Gfx.GraphicsDevice.Viewport.Width > GameDataManager.Gfx.GraphicsDevice.Viewport.Height ? GameDataManager.Gfx.GraphicsDevice.Viewport.Width : GameDataManager.Gfx.GraphicsDevice.Viewport.Height;
            startButton = new TButton(new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 200), new Vector2(100, 40), 10, 1, "Start", GameDataManager.Fonts["ak14"]);
            txtUsername = new TTextbox(new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 4, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 250), new Vector2(200, 40), 10, 0, "Username", GameDataManager.Fonts["ak14"]);
            txtUsername.OnClick += delegate
            {
                if ( Focus == null || Focus != txtUsername) { Focus = txtUsername; GameDataManager.ShowKeyboard(); }
                else { Focus = null; GameDataManager.HideKeyboard(); }
            };
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds);

            particleList.AddEntities(particleList.Update(seconds, dSeconds));

            if (animationProgress < warpTime)
            {
                animationProgress += seconds;
                float currProg = animationProgress / warpTime;
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new FadingAI(1.0f));
                Color c;
                switch (colorCount)
                {
                    case 0:
                        c = new Color(1.0f, 0f, 0f);
                        break;
                    case 1:
                        c = new Color(0f, 1.0f, 0f);
                        break;
                    case 2:
                        c = new Color(0f, 0f, 1.0f);
                        break;
                    default:
                        c = new Color();
                        break;
                }
                colorCount++;
                if (colorCount > 2) colorCount = 0;
                particleList.AddEntity(new CustomParticleEntity(titleTexture, titleShadowRect, types, new Vector2(centerPointX, bottomPointY - (travelDistance * currProg)), new Vector2(), c, 0, currProg));
            }
            else if (animationProgress >= warpTime && !finishedWarp)
            {
                animationProgress += seconds;
                finishedWarp = true;
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new FadingAI(1.0f));
                types.Add(new ShrinkingAI(2f));
                CustomParticleEntity p = new CustomParticleEntity(titleTexture, titleBurstRect, types, titleLocation, new Vector2(), Color.White, 0, 3f);
                p.LayerDepth = -1f;
                particleList.AddEntity(p);
                GameDataManager.AddSound(GameDataManager.Sounds["blast9"], 0f ,1f);
                MediaPlayer.Play(titleTheme);
                MediaPlayer.IsRepeating = true;
                AddScreenEffect(ScreenEffect.Flash, 0.75f, new Color(1f, 1f, 1f));
            }
            else
            {
                animationProgress += seconds;
                if (GameMath.RNGer.NextBoolean())
                {
                    List<ParticleAI> types = new List<ParticleAI>();
                    types.Add(new PointingAI());
                    types.Add(new TrailsAI(0.5f));
                    Vector2 pos = new Vector2((GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width + GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width) / 2f, 0);
                    float screenCenterX = GameDataManager.Gfx.GraphicsDevice.Viewport.Width / 2;
                    float velX = pos.X <= screenCenterX ? (screenCenterX - pos.X) / screenCenterX * -3000f : (pos.X - screenCenterX) / screenCenterX * 3000f;
                    Vector2 vel = new Vector2(velX, GameMath.RNGer.NextFloat() * 2000f + 2000f);
                    particleList.AddEntity(new ParticleEntity(new Rectangle(832, 64, 64, 64), types, pos, vel, new Color(0.2f + GameMath.RNGer.NextFloat() * 0.8f, 0.2f + GameMath.RNGer.NextFloat() * 0.8f, 0.2f + GameMath.RNGer.NextFloat() * 0.8f), 0, new Vector2(1f, 1f + GameMath.RNGer.NextFloat() * 7f)));
                }
            }

            TouchCollection tc = TouchPanel.GetState();
            txtUsername.GetInput(tc);
            startButton.GetInput(tc);
            if (startButton.IsPressed && !isStarting)
            {
                GameDataManager.AddSound(GameDataManager.Sounds["startgame"], 0f, 1f);
                MediaPlayer.Stop();
                isStarting = true;
                AddScreenEffect(ScreenEffect.FadingOut, 2f, Color.Black);
            }
            startButton.Update(seconds, dSeconds);

            if (isStarting && CurrentEffect == ScreenEffect.None) NextScreen = new IngameScreen();
        }

        public override void Draw()
        {
            GameDataManager.Batch.End();

            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
            particleList.Draw(GameDataManager.Batch);
            GameDataManager.Batch.End();

            GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            if (animationProgress >= warpTime)
            {
                GameDataManager.Batch.Draw(titleTexture, titleLocation, titleRect, Color.White, 0, new Vector2(titleRect.Width / 2, titleRect.Height / 2), 1f, SpriteEffects.None, 0);

                if (animationProgress < warpTime + 0.2f)
                {
                    GameDataManager.Batch.End();
                    GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.Additive);

                    float amt = ((warpTime + 0.2f) - animationProgress) * 5f;
                    Color c = new Color(amt, amt, amt);
                    GameDataManager.Batch.Draw(titleTexture, new Vector2(centerPointX,centerPointY), titleBurstCoreRect, Color.White, 0, new Vector2(titleRect.Width / 2, titleRect.Height / 2), widestSide/4f, SpriteEffects.None, 0);

                    GameDataManager.Batch.End();
                    GameDataManager.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                }
            }

            startButton.Draw(GameDataManager.Batch);

            txtUsername.Draw(GameDataManager.Batch); 

            DrawScreenEffects();
        }

        public override void Deactivated()
        {

        }

        public override void OnKeyPress(TKeyPress keyPress) 
        {
            if (Focus != null && Focus == txtUsername)
            {
                txtUsername.Text += keyPress.KeyCode.ToString();
            }
        }
    }
}
