﻿using Hyperdrive.Base.Particle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Hyperdrive.Base
{
    public abstract class GameEntity
    {
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public float Rotation { get; set; } //in rads, PI = 180 degrees, 0 is up
        public List<ParticleEntity> ReturnList { get; protected set; }

        public GameEntity()
        {
            Position = new Vector2();
            Velocity = new Vector2();
            Rotation = 0;
            ReturnList = new List<ParticleEntity>();
        }

        public GameEntity(Vector2 pos, Vector2 vel)
        {
            Position = pos;
            Velocity = vel;
            Rotation = 0;
            ReturnList = new List<ParticleEntity>();
        }

        public virtual void Update(float seconds, double dSeconds) //MAKE SURE ALL CHILD CLASSES RUN THIS FIRST so ReturnList is reset
        {
            ReturnList.Clear();
            Position += Velocity * seconds;
        }

        public abstract void Draw(SpriteBatch b);

    }
}
