﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Controls
{
    public class TTextbox : UIElement
    {
        public static Texture2D texture;

        public Vector2 Size { get; protected set; }

        public SpriteFont Font { get; protected set; }

        public string Text { get; set; }

        // public delegate void OnClickEventHandler(object sender, EventArgs e);

        public event EventHandler OnClick;

        private double cursorBlinkTime = 0;
        private bool cursorVisible = false;
        private Rectangle textRect;
        private Rectangle flashRect;

        public TTextbox(Vector2 position, Vector2 size, int touchBuffer, int style, string text, SpriteFont font)
        {
            Position = position;
            TouchBuffer = touchBuffer;
            Size = size;
            Font = font;
            switch (style)
            {
                case 0:
                    textRect = new Rectangle(0, 0, 256, 128);
                    flashRect = new Rectangle(0, 128, 256, 128);
                    break;
                case 1:
                    textRect = new Rectangle(256, 0, 256, 128);
                    flashRect = new Rectangle(256, 128, 256, 128);
                    break;
                default:
                    textRect = new Rectangle(0, 0, 256, 128);
                    flashRect = new Rectangle(0, 128, 256, 128);
                    break;
            }
            Text = text;
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds);
            cursorBlinkTime += dSeconds;
            if (cursorBlinkTime > 0.5) cursorVisible = !cursorVisible; 
        }

        public override void Draw(SpriteBatch b)
        {
            b.Draw(texture, new Rectangle((int)(Position.X - Size.X / 2f), (int)(Position.Y - Size.Y / 2f), (int)Size.X, (int)Size.Y), textRect, Color.Aqua, 0, new Vector2(0, 0), SpriteEffects.None, 0f);
            b.DrawString(Font, Text, new Vector2(Position.X - (Size.X / 2f - 3), Position.Y - (Size.Y / 2f - 3)), Color.White);
        }

        public override void GetInput(TouchCollection tc)
        {
            foreach (TouchLocation t in tc)
            {
                if (t.Position.X <= Position.X + (Size.X / 2 + TouchBuffer) && t.Position.X >= Position.X - (Size.X / 2 + TouchBuffer))
                    if (t.Position.Y <= Position.Y + (Size.Y / 2 + TouchBuffer) && t.Position.Y >= Position.Y - (Size.Y / 2 + TouchBuffer))
                        if (t.State == TouchLocationState.Pressed)
                            OnClick(this, EventArgs.Empty);
            }
        }
        
    }
}
