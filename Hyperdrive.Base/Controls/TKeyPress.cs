﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hyperdrive.Base.Controls
{
    public enum TKeycode
    {
        A = 29,
        B = 30,
        C = 31,
        D = 32,
        E = 33,
        F = 34,
        G = 35,
        H = 36,
        I = 37,
        J = 38,
        K = 39,
        L = 40,
        M = 41,
        N = 42,
        O = 43,
        P = 44,
        Q = 45,
        R = 46,
        S = 47,
        T = 48,
        U = 49,
        V = 50,
        W = 51,
        X = 52,
        Y = 53,
        Z = 54,
        Comma = 55,
        Period = 56,
        Space = 62,
        Enter = 66,
        Minus = 69,
        Equals = 70,
        LeftBracket = 71,
        RightBracket = 72,
        Backslash = 73,
        Semicolon = 74,
        Apostrophe = 75,
        Slash = 76,
        At = 77,
        Plus = 81
    }

    public class TKeyPress
    {
        public TKeycode KeyCode { get; set; }
        public bool IsUppercase { get; set; }
    }
}
