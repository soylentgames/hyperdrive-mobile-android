﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace Hyperdrive.Base.Controls
{
    public class AnalogControlCircle : UIElement
    {
        public static Texture2D sprite;
        public float deadzone = 32f;
        public float touchzone = 96f;
        private Rectangle circleRect;
        private Rectangle arrowRect;
        private Rectangle boostHighlight;
        private int currentTouchID;
        public Vector2 CurrentDirection { get; set; }
        public int FramesSinceTouched { get; protected set; }
        public bool IsBoosting { get; set; }

        public AnalogControlCircle()
        {
            CurrentDirection = new Vector2();
            Position = new Vector2(96, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 96);
            circleRect = new Rectangle(0, 0, 128, 128);
            arrowRect = new Rectangle(128, 0, 64, 64);
            boostHighlight = new Rectangle(192, 0, 64, 64);
            IsBoosting = false;
            FramesSinceTouched = 11;
            currentTouchID = -1;
        }

        public override void GetInput(TouchCollection tc)
        {
            bool receivedDir = false;
            if (tc.Count > 0)
            {
                if (currentTouchID == -1) //need to acquire touch
                {
                    foreach (TouchLocation t in tc)
                    {
                        Vector2 diff = t.Position - Position;
                        if (diff.Length() <= touchzone)
                        {
                            if (FramesSinceTouched > 0 && FramesSinceTouched <= 10) IsBoosting = true; //start boosting when there is a short gap

                            receivedDir = true;
                            FramesSinceTouched = 0;
                            if (diff.Length() < deadzone)
                            {
                                CurrentDirection = new Vector2();
                            }
                            else
                            {
                                diff.Normalize();
                                CurrentDirection = diff;
                            }
                            currentTouchID = t.Id;
                            break;
                        }
                    }
                }
                else foreach (TouchLocation t in tc) if (t.Id == currentTouchID)
                        {
                            //check if touch ending
                            if (t.State == TouchLocationState.Released) currentTouchID = -1;

                            receivedDir = true;
                            Vector2 diff = t.Position - Position;
                            if (diff.Length() < deadzone)
                            {
                                CurrentDirection = new Vector2();
                            }
                            else
                            {
                                diff.Normalize();
                                CurrentDirection = diff;
                            }
                            break;
                        }

            }
            if (!receivedDir)
            {
                CurrentDirection = new Vector2();
                currentTouchID = -1;
                FramesSinceTouched++;
                IsBoosting = false;
            }

        }

        public void Update()
        {
            Position = new Vector2(84, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 84);
        }

        public override void Draw(SpriteBatch b)
        {
            Draw(b, new Vector2());
        }

        public void Draw(SpriteBatch b, Vector2 offset)
        {
            Vector2 pos = new Vector2(Position.X - 64, Position.Y - 64);
            Color c = Color.White;

            if (IsBoosting)
            {
                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.Additive);

                //draw the highlight
                Color blueColor = new Color(0.5f, 0.5f, 1f, 1f);
                b.Draw(sprite, Position + offset, boostHighlight, blueColor, 0, new Vector2(boostHighlight.Width / 2, boostHighlight.Height / 2), 4.0f, SpriteEffects.None, 0f);

                b.End();
                b.Begin();
            }
            else if (!IsBoosting && FramesSinceTouched <= 10 && FramesSinceTouched > 0)
            {
                //TODO replace this with some kind of additive particle list in GameDataManager
                //end normal drawing
                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.Additive);
                
                //draw the highlight
                b.Draw(sprite, Position + offset, boostHighlight, c, 0, new Vector2(boostHighlight.Width / 2, boostHighlight.Height / 2), 2.0f + ((float)(10 - FramesSinceTouched) / 10f) * 2.0f, SpriteEffects.None, 0f);

                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            }
            b.Draw(sprite, pos + offset, circleRect, c, 0, new Vector2(), 1.0f, SpriteEffects.None, 0f);
            if (CurrentDirection.X == 0 && CurrentDirection.Y == 0) b.Draw(sprite, Position + offset, circleRect, c, 0, new Vector2(64f, 64f), 0.2f, SpriteEffects.None, 0);
            else b.Draw(sprite, Position + offset + CurrentDirection * 48f, arrowRect, c, GameMath.RotationFromVector(CurrentDirection), new Vector2(32f, 64f), 1.0f, SpriteEffects.None, 0);
        }
    }
}
