﻿using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Hyperdrive.Base.Controls
{
    public class TButton : UIElement
    {
        public static Texture2D texture;

        public bool IsPressed { get; protected set; }
        public int FramesSinceTouched { get; protected set; }
        public string Text { get; set; }
        public Vector2 Size { get; protected set; }
        public SpriteFont Font { get; protected set; }
        private Rectangle buttonRect;
        private Rectangle flashRect;

        public TButton(Vector2 position, Vector2 size, int touchBuffer, int style, string text, SpriteFont font)
        {
            Position = position;
            FramesSinceTouched = 16;
            TouchBuffer = touchBuffer;
            Size = size;
            Font = font;
            IsPressed = false;
            switch (style)
            {
                case 0:
                    buttonRect = new Rectangle(0, 0, 256, 128);
                    flashRect = new Rectangle(0, 128, 256, 128);
                    break;
                case 1:
                    buttonRect = new Rectangle(256, 0, 256, 128);
                    flashRect = new Rectangle(256, 128, 256, 128);
                    break;
                default:
                    buttonRect = new Rectangle(0, 0, 256, 128);
                    flashRect = new Rectangle(0, 128, 256, 128);
                    break;
            }
            Text = text;
        }

        public override void GetInput(TouchCollection tc)
        {
            foreach (TouchLocation t in tc)
            {
                if (t.Position.X <= Position.X + (Size.X / 2 + TouchBuffer) && t.Position.X >= Position.X - (Size.X / 2 + TouchBuffer))
                    if (t.Position.Y <= Position.Y + (Size.Y / 2 + TouchBuffer) && t.Position.Y >= Position.Y - (Size.Y / 2 + TouchBuffer))
                    {
                        IsPressed = true; FramesSinceTouched = 0;
                    }
            }
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds);
            if (!IsPressed && FramesSinceTouched < 16) FramesSinceTouched++;

            IsPressed = false;
        }

        public override void Draw(SpriteBatch b)
        {
            Vector2 stringSize = Font.MeasureString(Text);
            float progress = (float)FramesSinceTouched / 16f;
            float flashProgress = 1f - progress;
            progress += 1f; //to make this a multiplier for the size of the flash
            b.Draw(texture, new Rectangle((int)(Position.X - Size.X / 2f), (int)(Position.Y - Size.Y / 2f), (int)Size.X, (int)Size.Y), buttonRect, new Color(0.25f, 0.5f + flashProgress / 2f, 1f), 0, new Vector2(0, 0), SpriteEffects.None, 0f);
            
            if (FramesSinceTouched < 16)
            {
                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.Additive);
                b.Draw(texture, new Rectangle((int)(Position.X - (Size.X * progress) / 2f), (int)(Position.Y - (Size.Y * progress) / 2f), (int)(Size.X * progress), (int)(Size.Y * progress)), flashRect, new Color(flashProgress * 0.5f,flashProgress,flashProgress * 0.5f), 0, new Vector2(0, 0), SpriteEffects.None, 0f);

                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

                b.DrawString(Font, Text, Position, new Color(0f, 0f, 0f), 0, stringSize / 2, progress, SpriteEffects.None, 0);
            }
            else b.DrawString(Font, Text, Position, new Color(1f, 1f, 1f), 0, stringSize / 2, 1f, SpriteEffects.None, 0);
        }
    }
}
