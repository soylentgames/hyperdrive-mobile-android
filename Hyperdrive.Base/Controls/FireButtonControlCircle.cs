﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace Hyperdrive.Base.Controls
{
    public class FireButtonControlCircle : UIElement
    {
        public static Texture2D sprite;
        private Rectangle circleRect;

        public bool IsButtonDown { get; set; }

        public FireButtonControlCircle()
        {
            Position = new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - 96, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 96);
            circleRect = new Rectangle(0, 0, 128, 128);
            IsButtonDown = false;
        }
        
        public void Draw(SpriteBatch b, Vector2 offset)
        {
            Color c = Color.White;

            b.Draw(sprite, Position + offset, circleRect, c, 0, new Vector2(64,64), 1.0f, SpriteEffects.None, 0f);
        }

        public override void Draw(SpriteBatch b)
        {
            Draw(b, new Vector2());
        }

        public override void GetInput(TouchCollection tc)
        {
            IsButtonDown = false;
            if (tc.Count > 0)
            {
                foreach (TouchLocation t in tc)
                {
                    Vector2 diff = t.Position - Position;
                    float len = diff.Length();
                    if (len <= 96f)
                    {
                        IsButtonDown = true;
                        break;
                    }
                }
            }
        }

        public void Update()
        {
            Position = new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width - 96, GameDataManager.Gfx.GraphicsDevice.Viewport.Height - 96);
        }
    }
}
