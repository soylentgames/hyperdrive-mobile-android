﻿using Microsoft.Xna.Framework.Input.Touch;

namespace Hyperdrive.Base.Controls
{
    public abstract class UIElement : GameEntity
    {
        public int TouchBuffer { get; protected set; }

        public abstract void GetInput(TouchCollection tc);
    }
}
