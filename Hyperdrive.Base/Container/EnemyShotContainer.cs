﻿using Hyperdrive.Base.Enemy.Shots;
using Hyperdrive.Base.Particle;
using System.Collections.Generic;

namespace Hyperdrive.Base.Container
{
    public class EnemyShotContainer : EntityContainer
    {
        private static int outsideScreenBuffer = 300;

        public override List<ParticleEntity> Update(float seconds, double dSeconds)
        {
            List<ParticleEntity> newParticleList = new List<ParticleEntity>();
            List<GameEntity> removalList = new List<GameEntity>();
            foreach (EnemyShotEntity e in Entities)
            {
                e.Update(seconds, dSeconds);
                newParticleList.AddRange(e.ReturnList);
                
                //check for offscreen
                if (e.Position.X < 0 - outsideScreenBuffer || e.Position.X > GameDataManager.Gfx.GraphicsDevice.Viewport.Width + outsideScreenBuffer ||
                    e.Position.Y < 0 - outsideScreenBuffer || e.Position.Y > GameDataManager.Gfx.GraphicsDevice.Viewport.Height + outsideScreenBuffer)
                    removalList.Add(e);
                else //add to grid
                {
                    GameDataManager.Grid.AddToGrid(e);
                }
            }
            foreach (GameEntity e in removalList) Entities.Remove(e);
            return newParticleList;
        }
        
    }
}
