﻿using Hyperdrive.Base.Particle;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Hyperdrive.Base.Container
{
    public class EntityContainer
    {
        protected List<GameEntity> Entities { get; set; }

        public EntityContainer()
        {
            Entities = new List<GameEntity>();
        }

        public virtual List<ParticleEntity> Update(float seconds, double dSeconds)
        {
            List<ParticleEntity> list = new List<ParticleEntity>();
            foreach (GameEntity e in Entities)
            {
                e.Update(seconds, dSeconds);
                list.AddRange(e.ReturnList);
            }
            return list;
        }
        
        public virtual void Draw(SpriteBatch b)
        {
            foreach (GameEntity e in Entities) e.Draw(b);
        }

        public virtual void AddEntity(GameEntity e)
        {
            Entities.Add(e);
        }


        public virtual void AddEntities(List<GameEntity> ents)
        {
            Entities.AddRange(ents);
        }

        public virtual void Remove(GameEntity e)
        {
            if (Entities.Contains(e)) Entities.Remove(e);
        }

        public virtual void Clear()
        {
            Entities.Clear();
        }

        public int Count()
        {
            return Entities.Count;
        }
    }
}
