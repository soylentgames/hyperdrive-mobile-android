﻿using System.Collections.Generic;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.UI;

namespace Hyperdrive.Base.Container
{
    public class DamageEntityContainer : EntityContainer
    {
        public override List<ParticleEntity> Update(float seconds, double dSeconds)
        {
            //remember to base.Update() if generating particles
            List<ParticleEntity> list = new List<ParticleEntity>();
            List<GameEntity> removalList = new List<GameEntity>();
            foreach (DamageEntity d in Entities)
            {
                d.Update(seconds, dSeconds);
                list.AddRange(d.ReturnList);
                if (d.Dead) removalList.Add(d);
            }
            foreach (DamageEntity d in removalList) Entities.Remove(d);
            return list;
        }

        public bool Contains(DamageEntity d)
        {
            return Entities.Contains(d);
        }
    }
}
