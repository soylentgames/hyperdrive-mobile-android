﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Particle.AI;
using Microsoft.Xna.Framework.Graphics;

namespace Hyperdrive.Base.Container
{
    public class AstralEntityContainer : EntityContainer
    {
        private List<Texture2D> astralTextures { get; set; }
        public AstralEntityContainer() : base()
        {
            astralTextures = new List<Texture2D>();
            astralTextures.Add(GameDataManager.Textures["astral1"]);
            astralTextures.Add(GameDataManager.Textures["astral2"]);
            astralTextures.Add(GameDataManager.Textures["astral3"]);
            astralTextures.Add(GameDataManager.Textures["astral4"]);
            astralTextures.Add(GameDataManager.Textures["astral5"]);
            astralTextures.Add(GameDataManager.Textures["astral6"]);
        }
        
        public override List<ParticleEntity> Update(float seconds, double dSeconds)
        {
            bool needsNewAstral = false;
            foreach (ParticleEntity p in Entities)
            {
                p.Update(seconds, dSeconds);
                if (p.Position.Y > GameDataManager.Gfx.GraphicsDevice.Viewport.Height + p.ParticleRect.Height / 2) needsNewAstral = true;
            }

            if (needsNewAstral == true || Entities.Count == 0)
            {
                Entities.Clear();
                GameMath.RNGer.Next(astralTextures.Count);
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new PulsingAI(1.0001f, 60));
                types.Add(new RotatingAI(0.0005f));
                Entities.Add(new CustomParticleEntity(astralTextures[GameMath.RNGer.Next(astralTextures.Count)], new Rectangle(0, 0, 1024, 1024), types, new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width * GameMath.RNGer.NextFloat(), -700), new Vector2(0, 50f), Color.White, 0, new Vector2(1f, 1f)));
            }
            return new List<ParticleEntity>();
        }
    }
}
