﻿using System.Collections.Generic;
using Hyperdrive.Base.Player;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Enemy.Shots;
using Hyperdrive.Base.Enemy;

namespace Hyperdrive.Base.Container
{
    public class EnemyEntityContainer : EntityContainer
    {
        private static int outsideScreenBuffer = 300;
        
        public override List<ParticleEntity> Update(float seconds, double dSeconds)
        {
            return new List<ParticleEntity>();
            //don't use
        }
        
        public void Update(float seconds, double dSeconds, PlayerEntity player, EnemyShotContainer shotPool)
        {
            List<GameEntity>removalList = new List<GameEntity>();
            foreach (EnemyEntity e in Entities)
            {
                //update
                if (player != null) e.TargetUpdate(player, seconds, dSeconds);
                e.Update(seconds, dSeconds, shotPool);
                
                //check for offscreen 
                if (e.Position.X < 0 - outsideScreenBuffer || e.Position.X > GameDataManager.Gfx.GraphicsDevice.Viewport.Width + outsideScreenBuffer ||
                    e.Position.Y < 0 - outsideScreenBuffer || e.Position.Y > GameDataManager.Gfx.GraphicsDevice.Viewport.Height + outsideScreenBuffer)
                    removalList.Add(e);
                else //add to grid
                {
                    GameDataManager.Grid.AddToGrid(e);
                }
            }
            foreach (GameEntity e in removalList) Entities.Remove(e);
        }
    }
}
