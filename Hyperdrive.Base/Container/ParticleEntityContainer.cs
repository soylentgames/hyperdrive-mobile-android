﻿using Hyperdrive.Base.Particle;
using System.Collections.Generic;

namespace Hyperdrive.Base.Container
{
    public class ParticleEntityContainer : EntityContainer
    {
        private static int outsideScreenBuffer = 300;
      
        public  void AddEntities(List<ParticleEntity> ents)
        {
            Entities.AddRange(ents);
        }

        public override List<ParticleEntity> Update(float seconds, double dSeconds)
        {
            List<ParticleEntity> list = new List<ParticleEntity>();
            List <GameEntity> removalList = new List<GameEntity>();
            foreach (ParticleEntity p in Entities)
            {
                p.Update(seconds, dSeconds);
                list.AddRange(p.ReturnList);
                if (p.Dead || p.Position.X > GameDataManager.Gfx.GraphicsDevice.Viewport.Width + outsideScreenBuffer || p.Position.X < -outsideScreenBuffer || p.Position.Y > GameDataManager.Gfx.GraphicsDevice.Viewport.Height + outsideScreenBuffer || p.Position.Y < -outsideScreenBuffer) removalList.Add(p);
            }
            foreach (ParticleEntity p in removalList) Entities.Remove(p);
            return list;
        }

    }
}
