﻿using Hyperdrive.Base.Inventory.Frames;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace Hyperdrive.Base.Collision
{
    public static class Collision
    {
        public static bool CheckCollision(Collider c1, Collider c2)
        {
            switch (c1.Shape)
            {
                case ColliderShape.Frame:
                    return CheckCollision((FrameCollider)c1, c2);
                case ColliderShape.Circle:
                    switch (c2.Shape)
                    {
                        case ColliderShape.Circle:
                            return CheckCollision((CircleCollider) c1, (CircleCollider) c2);
                        case ColliderShape.Rect:
                            return CheckCollision((CircleCollider)c1, (RectCollider)c2);
                        case ColliderShape.Frame:
                            return CheckCollision((FrameCollider)c2, (CircleCollider)c1);
                    }
                    break;
                case ColliderShape.Rect:
                    switch (c2.Shape)
                    {
                        case ColliderShape.Circle:
                            return CheckCollision((CircleCollider)c2, (RectCollider)c1);
                        case ColliderShape.Rect:
                            return CheckCollision((RectCollider)c1, (RectCollider)c2);
                        case ColliderShape.Frame:
                            return CheckCollision((FrameCollider)c2, (RectCollider)c1);
                    }
                    break;
            }
            return false;
        }

        private static bool CheckCollision(CircleCollider c1, CircleCollider c2)
        {
            if (GameMath.DistanceSquared(c1.GetOrigin(), c2.GetOrigin()) <= Math.Pow(c1.Radius + c2.Radius, 2)) return true;
            else return false;
        }

        private static bool CheckCollision(CircleCollider c1, RectCollider c2)
        {
            Vector2 cOrigin = c1.GetOrigin();
            Vector2 rOrigin = c2.GetOrigin();
            if (GameMath.DistanceSquared(cOrigin, rOrigin) > Math.Pow(c1.Radius + c2.Radius, 2)) return false;

            // Rotate circle's center point using the logic of the rectangle being 0 degrees rotated instead of the circle
            double unrotatedCircleX = Math.Cos(c1.Owner.Rotation) * (cOrigin.X - rOrigin.X) -
                    Math.Sin(c1.Owner.Rotation) * (cOrigin.Y - rOrigin.Y) + rOrigin.X;
            double unrotatedCircleY = Math.Sin(c1.Owner.Rotation) * (cOrigin.X - rOrigin.X) +
                    Math.Cos(c1.Owner.Rotation) * (cOrigin.Y - rOrigin.Y) + rOrigin.Y;

            // Closest point in the rectangle to the center of circle rotated backwards(unrotated)
            double closestX, closestY;

            // Find the unrotated closest x point from center of unrotated circle
            Vector2 topLeft = rOrigin - c2.Size / 2;
            if (unrotatedCircleX < topLeft.X)
                closestX = topLeft.X;
            else if (unrotatedCircleX > topLeft.X + c2.Size.X)
                closestX = topLeft.X + c2.Size.X;
            else
                closestX = unrotatedCircleX;

            // Find the unrotated closest y point from center of unrotated circle
            if (unrotatedCircleY < topLeft.Y)
                closestY = topLeft.Y;
            else if (unrotatedCircleY > topLeft.Y + c2.Size.Y) //these two lines
                closestY = topLeft.Y + c2.Size.Y;              //may be wrong - the var may be c1
            else
                closestY = unrotatedCircleY;

            double distance = GameMath.Distance(new Vector2((float)unrotatedCircleX, (float)unrotatedCircleY), new Vector2((float)closestX, (float)closestY));
            if (distance < c1.Radius) return true;
            else return false;
        }

        private static bool CheckCollision(RectCollider c1, RectCollider c2)
        {
            if (GameMath.DistanceSquared(c1.GetOrigin(), c2.GetOrigin()) <= Math.Pow(c1.Radius + c2.Radius, 2)) return true;
            else return false;
        }

        private static bool CheckCollision(TPolygon c1, TPolygon c2)
        {
            return false;
        }

        private static bool CheckCollision(FrameCollider c1, Collider c2)
        {
            bool collision = false;
            c1.HitPart = null;
            //check collision for each part
            foreach (KeyValuePair<CollidableItem, Collider> sc in c1.SubColliders) if (CheckCollision(sc.Value,c2)) { collision = true; c1.HitPart = sc.Key; break; }
            return collision;
        }
    }
}
