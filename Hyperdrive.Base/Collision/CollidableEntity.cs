﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Collision
{
    public abstract class CollidableEntity : GameEntity
    {
        public CollidableEntity(Vector2 pos, Vector2 vel) : base(pos, vel)
        { }

        public Collider Collider { get; set; }
    }
}
