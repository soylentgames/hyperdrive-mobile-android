﻿using Hyperdrive.Base.Collision.Event;
using Hyperdrive.Base.Container;
using Hyperdrive.Base.Enemy;
using Hyperdrive.Base.Enemy.Shots;
using Hyperdrive.Base.Inventory.Frames;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Player;
using Hyperdrive.Base.Player.Shots;
using Hyperdrive.Base.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;

namespace Hyperdrive.Base.Collision
{
    public class CollisionManager
    {

        public static SoundEffect enemyDestroyedSound;
        public static SoundEffect enemyHitSound;
        public static SoundEffect playerHitSound;

        private List<CollisionEvent> collisions = new List<CollisionEvent>();

        public void DoCollisions(GameGrid grid, PlayerShotContainer playerShots, EnemyShotContainer enemyShots, EnemyEntityContainer enemies, ParticleEntityContainer deathParticleDump, DamageEntityContainer damages, EntityContainer experiences, out bool playerLeveled, out bool playerHit)
        {
            //reset stuffs
            collisions.Clear();
            playerLeveled = false;
            playerHit = false;

            //detect collisions
            for (int x = 0; x < grid.NumberOfXCells; x++)
                for (int y = 0; y < grid.NumberOfYCells; y++)
                    collisions.AddRange(CheckCellCollisions(grid.Cells[x, y]));
            //process collisions
            foreach (CollisionEvent e in collisions)
                switch (e.Type)
                {
                    case CollisionType.EnemyShot_to_Player:
                        {
                            EnemyShotToPlayerEvent ev = (EnemyShotToPlayerEvent)e;
                            float damage = ev.Shot.GetDamage();
                            ev.Shot.Dead = true; //later on this will be used for things like lasers which penetrate, won't always be true
                            /*if (ev.Shot.Dead)*/ enemyShots.Remove(ev.Shot);
                            playerHit = true;

                            //play hit sound
                            GameDataManager.AddSound(playerHitSound, -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 0.75f);

                            //first check if this part is not the frame; if so, apply damage to shields first because shields apply to whole ship
                            if (ev.PartHit != ev.Player.Owner.EquippedFrame) damage = ((FramePartItem)ev.PartHit).MainBody.HitShields(damage);
                            //handle hit and destruction
                            if (ev.PartHit.Hit(damage))
                            {
                                if (ev.PartHit != ev.Player.Owner.EquippedFrame)
                                {
                                    ev.Player.Owner.EquippedFrame.Hit(((FramePartItem)ev.PartHit).ExplodeDamage);
                                }
                                deathParticleDump.AddEntities(ev.PartHit.GetDeathParticles());
                            }
                        }
                        break;
                    case CollisionType.PlayerShot_to_Enemy:
                        {
                            PlayerShotToEnemyEvent ev = (PlayerShotToEnemyEvent)e;
                            PlayerCharacter hitOwner;
                            playerShots.Remove(ev.Shot);
                            ev.Enemy.Hit(ev.Shot.GetDamage(), ev.Shot.Velocity, ev.Shot.Knockback);
                            if (ev.Enemy.Destroyed)
                            {
                                enemies.Remove(ev.Enemy);
                                hitOwner = ev.Shot.Owner;
                                GameDataManager.AddSound(enemyDestroyedSound, -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 1f);

                                deathParticleDump.AddEntities(ev.Enemy.GetDeathParticles());
                                experiences.AddEntity(new ExperienceEntity(ev.Enemy.Position + new Vector2(0, -20), ev.Enemy.Points));
                                if (hitOwner.AddScore(ev.Enemy.Points)) //true if leveled up
                                {
                                    //level up effects
                                    playerLeveled = true;
                                }
                            }
                            else
                            {
                                ev.Enemy.Aggro(ev.Shot.Owner.Entity);
                                GameDataManager.AddSound(enemyHitSound, -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 0.5f);
                            }
                            if (ev.Enemy.DamageEntity != null && !ev.Enemy.DamageEntity.Dead && !damages.Contains(ev.Enemy.DamageEntity)) damages.AddEntity(ev.Enemy.DamageEntity);
                        }
                        break;
                    case CollisionType.Enemy_to_Player:
                        {
                            EnemyToPlayerEvent ev = (EnemyToPlayerEvent)e;
                            float damage = ev.Enemy.Hull + 2f;
                            ev.Enemy.Destroyed = true; //later on this will be used for things like lasers which penetrate, won't always be true
                                                 /*if (ev.Shot.Dead)*/
                            enemies.Remove(ev.Enemy);
                            deathParticleDump.AddEntities(ev.Enemy.GetDeathParticles());
                            playerHit = true;

                            //play hit sound
                            GameDataManager.AddSound(playerHitSound, -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 0.75f);

                            //first check if this part is not the frame; if so, apply damage to shields first because shields apply to whole ship
                            if (ev.PartHit != ev.Player.Owner.EquippedFrame) damage = ((FramePartItem)ev.PartHit).MainBody.HitShields(damage);
                            //handle hit and destruction
                            if (ev.PartHit.Hit(damage))
                            {
                                if (ev.PartHit != ev.Player.Owner.EquippedFrame)
                                {
                                    ev.Player.Owner.EquippedFrame.Hit(((FramePartItem)ev.PartHit).ExplodeDamage);
                                }
                                deathParticleDump.AddEntities(ev.PartHit.GetDeathParticles());
                            }
                        }
                        break;
                }
        }

        private List<CollisionEvent> CheckCellCollisions(List<CollidableEntity> cellContents)
        {
            List<CollisionEvent> collisions = new List<CollisionEvent>();
            for (int i = 0; i < cellContents.Count; i++)
                for (int j = i + 1; j < cellContents.Count; j++)
                    if (Collision.CheckCollision(cellContents[i].Collider,cellContents[j].Collider))
                        switch (cellContents[i].Collider.Type)
                        {
                            case ColliderType.EnemyShot:
                                switch (cellContents[j].Collider.Type)
                                {
                                    case ColliderType.Player:
                                        collisions.Add(new EnemyShotToPlayerEvent((EnemyShotEntity)cellContents[i], (PlayerEntity)cellContents[j], ((FrameCollider)cellContents[j].Collider).HitPart));
                                        break;
                                }
                                break;
                            case ColliderType.PlayerShot:
                                switch (cellContents[j].Collider.Type)
                                {
                                    case ColliderType.Enemy:
                                        collisions.Add(new PlayerShotToEnemyEvent((PlayerShotEntity)cellContents[i], (EnemyEntity)cellContents[j]));
                                        break;
                                }
                                break;
                            case ColliderType.Enemy:
                                switch (cellContents[j].Collider.Type)
                                {
                                    case ColliderType.PlayerShot:
                                        collisions.Add(new PlayerShotToEnemyEvent((PlayerShotEntity)cellContents[j], (EnemyEntity)cellContents[i]));
                                        break;
                                    case ColliderType.Player:
                                        collisions.Add(new EnemyToPlayerEvent((EnemyEntity)cellContents[i], (PlayerEntity)cellContents[j], ((FrameCollider)cellContents[j].Collider).HitPart));
                                        break;
                                }
                                break;
                            case ColliderType.Player:
                                switch (cellContents[j].Collider.Type)
                                {
                                    case ColliderType.EnemyShot:
                                        collisions.Add(new EnemyShotToPlayerEvent((EnemyShotEntity)cellContents[j], (PlayerEntity)cellContents[i], ((FrameCollider)cellContents[i].Collider).HitPart));
                                        break;
                                    case ColliderType.Enemy:
                                        collisions.Add(new EnemyToPlayerEvent((EnemyEntity)cellContents[j], (PlayerEntity)cellContents[i], ((FrameCollider)cellContents[i].Collider).HitPart));
                                        break;
                                }
                                break;
                        }
            return collisions;
        }
    }
}
