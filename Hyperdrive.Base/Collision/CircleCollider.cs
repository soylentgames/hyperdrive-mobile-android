﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace Hyperdrive.Base.Collision
{
    public class CircleCollider : Collider
    {
        public float Radius { get; private set; }

        public CircleCollider(float radius, Vector2 offset, CollidableEntity owner, ColliderType type)
        {
            Radius = radius;
            Offset = offset;
            Owner = owner;
            Type = type;
            Shape = ColliderShape.Circle;
        }

        public Vector2 GetOrigin()
        {
            if (Offset.X != 0 || Offset.Y != 0) return Owner.Position + GameMath.RotatePointAroundPoint(Offset, Vector2.Zero, Owner.Rotation);
            else return Owner.Position;
        }

        public override List<Vector2> GetGridPoints()
        {
            List<Vector2> returnList = new List<Vector2>();
            Vector2 origin = GetOrigin();

            for (double d = 0; d < Math.PI * 2; d += Math.PI / 4)
            {
                returnList.Add(GameMath.GetPointAtAngle(origin, Radius, d));
            }

            return returnList;
        }
    }
}
