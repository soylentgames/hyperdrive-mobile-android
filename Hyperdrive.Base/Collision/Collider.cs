﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Collision
{
    public enum ColliderType
    {
        Player,
        Enemy,
        PlayerShot,
        EnemyShot,
        Static
    }

    public enum ColliderShape
    {
        Circle,
        Rect,
        Frame
    }

    public abstract class Collider
    {
        public CollidableEntity Owner { get; protected set; }
        public ColliderType Type { get; protected set; }
        public ColliderShape Shape { get; protected set; }

        public Vector2 Offset { get; protected set; }
        
        public abstract List<Vector2> GetGridPoints();
        
    }
}
