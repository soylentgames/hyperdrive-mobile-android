﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Hyperdrive.Base.Inventory.Frames;

namespace Hyperdrive.Base.Collision
{
    public class FrameCollider : Collider
    {
        public Dictionary<CollidableItem, Collider> SubColliders { get; private set; }
        public CollidableItem HitPart { get; set; }

        public FrameCollider(Vector2 offset, CollidableEntity owner, Dictionary<CollidableItem, Collider> subColliders)
        {
            Owner = owner;
            Type = ColliderType.Player;
            Shape = ColliderShape.Frame;
            SubColliders = subColliders;
            HitPart = null;
        }

        public override List<Vector2> GetGridPoints()
        {
            List<Vector2> gridPoints = new List<Vector2>();
            foreach (KeyValuePair<CollidableItem, Collider> sc in SubColliders) gridPoints.AddRange(sc.Value.GetGridPoints());
            return gridPoints;
        }
    }
}
