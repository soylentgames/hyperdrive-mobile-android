﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Collision
{
    public class TPolygon
    {
        public TPolygon()
        {
            Edges = new List<Vector2>();
            Points = new List<Vector2>();
        }
        public List<Vector2> Edges { get; private set; }
        public List<Vector2> Points { get; private set; }

        public void BuildEdges()
        {
            Vector2 pointA;
            Vector2 pointB;
            Edges.Clear();
            for (int i = 0; i < Points.Count; i++)
            {
                pointA = Points[i];
                if (i + 1 >= Points.Count)
                {
                    pointB = Points[0];
                }
                else
                {
                    pointB = Points[i + 1];
                }
                Edges.Add(pointB - pointA);
            }
        }


        public Vector2 GetCenter()
        {
            float totalX = 0;
            float totalY = 0;
            for (int i = 0; i < Points.Count; i++)
            {
                totalX += Points[i].X;
                totalY += Points[i].Y;
            }
            
            return new Vector2(totalX / (float)Points.Count, totalY / (float)Points.Count);
        }

        public void Offset(Vector2 v)
        {
            Offset(v.X, v.Y);
        }

        public void Offset(float x, float y)
        {
            for (int i = 0; i < Points.Count; i++)
            {
                Vector2 p = Points[i];
                Points[i] = new Vector2(p.X + x, p.Y + y);
            }
        }
    }
}
