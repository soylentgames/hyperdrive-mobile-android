﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Collision
{
    public class RectCollider : Collider
    {
        public Vector2 Size { get; set; }
        public float Radius { get; private set; }

        public Vector2 UL { get; private set; } 
        public Vector2 UR { get; private set; }
        public Vector2 LL { get; private set; }
        public Vector2 LR { get; private set; }

        public RectCollider(Vector2 size, Vector2 offset, CollidableEntity owner, ColliderType type)
        {
            Size = size;
            Vector2 halfSize = Size / 2;
            Radius = halfSize.Length();
            Offset = offset;
            Owner = owner;
            Type = type;
            Shape = ColliderShape.Rect;
            UL = new Vector2(-halfSize.X, -halfSize.Y);
            UR = new Vector2(halfSize.X, -halfSize.Y);
            LL = new Vector2(-halfSize.X, halfSize.Y);
            LR = new Vector2(halfSize.X, halfSize.Y);
        }

        public Vector2 GetOrigin()
        {
            if (Offset.X != 0 || Offset.Y != 0) return Owner.Position + GameMath.RotatePointAroundPoint(Offset, Vector2.Zero, Owner.Rotation);
            else return Owner.Position;
        }

        public override List<Vector2> GetGridPoints()
        {
            List<Vector2> returnList = new List<Vector2>();
            Vector2 origin = GetOrigin();
            returnList.Add(GameMath.RotatePointAroundPoint(origin - Size / 2,origin, Owner.Rotation));
            returnList.Add(GameMath.RotatePointAroundPoint(origin + Size / 2, origin, Owner.Rotation));
            returnList.Add(GameMath.RotatePointAroundPoint(origin + new Vector2(Size.X / 2, -Size.Y / 2), origin, Owner.Rotation));
            returnList.Add(GameMath.RotatePointAroundPoint(origin + new Vector2(-Size.X / 2, Size.Y / 2), origin, Owner.Rotation));
            return returnList;
        }
    }
}
