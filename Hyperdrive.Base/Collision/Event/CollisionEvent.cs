﻿namespace Hyperdrive.Base.Collision.Event
{
    public enum CollisionType
    {
       Enemy_to_Player,
       EnemyShot_to_Player,
       PlayerShot_to_Enemy
    }
    public abstract class CollisionEvent
    {
        public CollisionType Type { get; set; }


    }
}
