﻿using Hyperdrive.Base.Enemy;
using Hyperdrive.Base.Inventory.Frames;
using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Collision.Event
{
    public class EnemyToPlayerEvent : CollisionEvent
    {
        public EnemyEntity Enemy { get; private set; }
        public PlayerEntity Player { get; private set; }
        public CollidableItem PartHit { get; private set; }

        public EnemyToPlayerEvent(EnemyEntity enemy, PlayerEntity player, CollidableItem partHit)
        {
            Enemy = enemy;
            Player = player;
            PartHit = partHit;
            Type = CollisionType.Enemy_to_Player;
        }
    }
}
