﻿using Hyperdrive.Base.Enemy;
using Hyperdrive.Base.Player.Shots;

namespace Hyperdrive.Base.Collision.Event
{
    public class PlayerShotToEnemyEvent : CollisionEvent
    {
        public PlayerShotEntity Shot { get; private set; }
        public EnemyEntity Enemy { get; private set; }

        public PlayerShotToEnemyEvent(PlayerShotEntity playerShot, EnemyEntity enemy)
        {
            Enemy = enemy;
            Shot = playerShot;
            Type = CollisionType.PlayerShot_to_Enemy;
        }
    }
}
