﻿using Hyperdrive.Base.Enemy.Shots;
using Hyperdrive.Base.Inventory.Frames;
using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Collision.Event
{
    public class EnemyShotToPlayerEvent : CollisionEvent
    {
        public EnemyShotEntity Shot { get; private set; }
        public PlayerEntity Player { get; private set; }
        public CollidableItem PartHit { get; private set; }

        public EnemyShotToPlayerEvent(EnemyShotEntity enemyShot, PlayerEntity player, CollidableItem partHit)
        {
            Shot = enemyShot;
            Player = player;
            PartHit = partHit;
            Type = CollisionType.EnemyShot_to_Player;
        }
    }
}
