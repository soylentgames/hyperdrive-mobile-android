﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.UI
{
    public class ExperienceEntity : GameEntity
    {
        public bool Dead { get; set; }
        public static SpriteFont font;
        private String xpString;
        private float currentAlpha;
        private Color currentColor;

        public ExperienceEntity(Vector2 pos, int xp) : base(pos, new Vector2(0,-25f))
        {
            currentAlpha = 1f;
            xpString = "+" + xp.ToString() + "xp";
            Dead = false;
        }
        
        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds);  //updates pos and resets ReturnList
            currentAlpha -= 0.3333f * seconds;
            if (currentAlpha < 0)
            {
                currentAlpha = 0;
                Dead = true;
            }
            currentColor = new Color(currentAlpha, 0.2f * currentAlpha, currentAlpha);
        }

        public override void Draw(SpriteBatch b)
        {
            b.DrawString(font, xpString, Position, currentColor);
        }
    }
}
