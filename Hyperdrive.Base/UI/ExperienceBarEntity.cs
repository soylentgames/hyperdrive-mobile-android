﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.UI
{
    public class ExperienceBarEntity : GameEntity
    {
        public static Texture2D texture;
        private static Rectangle outlineRect = new Rectangle(0,0,512,64);
        private static Rectangle flashRect = new Rectangle(0, 256, 512, 64);
        private static Rectangle frame1Rect = new Rectangle(7,64, 498,64);
        private static Rectangle frame2Rect = new Rectangle(7, 128, 498, 64);
        private static Rectangle frame3Rect = new Rectangle(7, 192, 498, 64);

        private Rectangle currentXpRect;
        private float lastXpPct;
        private double frameTime;
        private double levelTime;
        private int frame;
        private bool xpChanged;
        private string xpText;
        private int level;
        public static SpriteFont ak14;
        public static SpriteFont ak12;

        public ExperienceBarEntity(Vector2 pos) : base(pos, new Vector2())
        {
            lastXpPct = 1f;
            frame = 1;
            frameTime = 0;
            levelTime = 0.5;
            xpText = "";
        }

        public void Update(float xpPct, string xpText, int level, double dSeconds)
        {
            this.xpText = xpText;
            this.level = level;

            //update frame
            levelTime += dSeconds;
            frameTime += dSeconds;
            double postMod = frameTime % 0.2; //5fps for the bar
            if (postMod < frameTime)
            {
                frameTime = postMod;
                frame++;
                if (frame == 4) frame = 1;
            }

            //update pct
            if (lastXpPct != xpPct)
            {
                lastXpPct = (lastXpPct * 9f + xpPct) / 10f;
                xpChanged = true;
                if (Math.Abs(lastXpPct - xpPct) < 0.005) lastXpPct = xpPct;
            }
            else xpChanged = false;
            
            //get rect bitch
            switch (frame)
            {
                case 1:
                    currentXpRect = new Rectangle(frame1Rect.X, frame1Rect.Y, (int)((float)frame1Rect.Width * lastXpPct), frame1Rect.Height);
                    break;
                case 2:
                    currentXpRect = new Rectangle(frame2Rect.X, frame2Rect.Y, (int)((float)frame2Rect.Width * lastXpPct), frame2Rect.Height);
                    break;
                case 3:
                    currentXpRect = new Rectangle(frame3Rect.X, frame3Rect.Y, (int)((float)frame3Rect.Width * lastXpPct), frame3Rect.Height);
                    break;
            }

        }

        public override void Draw(SpriteBatch b)
        {
            Draw(b, new Vector2());
        }

        public void Draw(SpriteBatch b, Vector2 offset)
        {
            b.Draw(texture, Position + offset, outlineRect, Color.White, 0, new Vector2(0, 0), new Vector2(1f, 1f), SpriteEffects.None, 0);
            b.Draw(texture, new Vector2(Position.X + (offset.X + 7), Position.Y + offset.Y), currentXpRect, Color.White, 0, new Vector2(0, 0), new Vector2(1f, 1f), SpriteEffects.None, 0);
            if (xpChanged) b.Draw(texture, new Vector2(Position.X + (offset.X + 7), Position.Y + offset.Y), currentXpRect, Color.Green, 0, new Vector2(0, 0), new Vector2(1f, 1f), SpriteEffects.None, 0);
            b.DrawString(ak14, xpText, new Vector2(Position.X + (offset.X + 20), Position.Y + (offset.Y + 34)), Color.White);
            b.DrawString(ak12, "Level " + level, new Vector2(Position.X + (offset.X + 408), Position.Y + (offset.Y + 22)), Color.Aqua);
            b.End();

            b.Begin(SpriteSortMode.Deferred, BlendState.Additive);
            if (levelTime < 0.5)
            {
                float amt = 1f - (float)levelTime / 0.5f;
                b.Draw(texture, Position + offset, flashRect, new Color(amt, amt, amt), 0, new Vector2(0, 0), new Vector2(1f, 1f), SpriteEffects.None, 0);
            }
            b.End();

            b.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
        }

        public void LevelUp()
        {
            levelTime = 0;
        }
    }
}
