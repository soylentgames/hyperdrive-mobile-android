﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.UI
{
    public class DamageEntity : GameEntity
    {
        public static SpriteFont font;
        public static SpriteFont boldFont;
        public bool Dead { get; set; }
        private float damage;
        private float lastDisplayed;
        private float currentAlpha;
        private string damageString;
        private string comboString;
        private int hitCount;
        private Color currentColor;
        private Color currentComboColor;

        public DamageEntity(Vector2 pos, float initialDamage) : base(pos, new Vector2(0,-25f))
        {
            damage = initialDamage;
            lastDisplayed = 0;
            currentAlpha = 1f;
            hitCount = 1;
            damageString = "-" + lastDisplayed.ToString("0.00");
            comboString = hitCount.ToString() + " hits";
            Dead = false;
        }

        public void AddDamage(float damage, Vector2 position)
        {
            this.damage += damage;
            hitCount++;
            Position = position;
            currentAlpha = 1f;
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds); //updates position and resets ReturnList

            currentAlpha -= 0.5f * seconds;
            if (currentAlpha < 0)
            {
                currentAlpha = 0;
                Dead = true;
            }
            currentColor = new Color(0.2f * currentAlpha, currentAlpha, 0.2f * currentAlpha);
            currentComboColor = new Color(currentAlpha, 0.8f * currentAlpha, 0.1f * currentAlpha);

            if (lastDisplayed < damage)
            {
                lastDisplayed = (lastDisplayed * 7f + damage) / 8f;
                if (damage - lastDisplayed < 0.01f) lastDisplayed = damage;
            }
            damageString = "-" + lastDisplayed.ToString("0.00");
            comboString = hitCount.ToString() + " hits";
        }

        public override void Draw(SpriteBatch b)
        {
            b.DrawString(font, damageString, Position, currentColor);
            if (hitCount > 1) b.DrawString(boldFont, comboString, Position + new Vector2(70, 0), currentComboColor);
        }
    }
}
