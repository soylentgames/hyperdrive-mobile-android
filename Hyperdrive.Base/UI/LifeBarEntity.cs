﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Hyperdrive.Base.UI
{
    public class LifeBarEntity : GameEntity
    {
        public static Texture2D texture;
        public static SpriteFont font;
        private static Rectangle outlineRect = new Rectangle(0, 0, 128, 512);
        private static Rectangle hullRect = new Rectangle(128, 24, 64, 481);
        private static Rectangle powerRect = new Rectangle(192, 42, 64, 463);
        private static Rectangle shieldRect = new Rectangle(256, 7, 128, 498);
        private static Rectangle boostRect = new Rectangle(384, 293, 128, 212);

        private Rectangle currentHullRect;
        private Rectangle currentShieldRect;
        private Rectangle currentPowerRect;
        private Rectangle currentBoostRect;
        private int currentHullPosY;
        private int currentShieldPosY;
        private int currentPowerPosY;
        private int currentBoostPosY;

        private float lastHullPct;
        private float lastShieldPct;
        private float lastPowerPct;
        private float lastBoostPct;

        private bool hullChanging;
        private bool hullChangeDir;
        private bool shieldChanging;
        private bool shieldChangeDir;
        private bool powerChanging;
        private bool powerChangeDir;
        private string hullString;
        private string shieldString;
        private string powerString;
        
        public LifeBarEntity(Vector2 pos) : base(pos, new Vector2())
        {
            lastHullPct = 1f;
            lastShieldPct = 1f;
            lastPowerPct = 1f;
        }

        public void Update(float hullPct, float shieldPct, float powerPct, float boostPct, string hullString, string shieldString, string powerString)
        {
            if (lastHullPct != hullPct)
            {
                hullChanging = true;
                lastHullPct = (lastHullPct * 9f + hullPct) / 10f;
                if (lastHullPct > hullPct) hullChangeDir = false;
                else hullChangeDir = true;
                if (Math.Abs(lastHullPct - hullPct) < 0.001)
                {
                    lastHullPct = hullPct;
                    hullChanging = false;
                }
            }
            else hullChanging = false;
            if (lastShieldPct != shieldPct)
            {
                shieldChanging = true;
                lastShieldPct = (lastShieldPct * 9f + shieldPct) / 10f;
                if (lastShieldPct > shieldPct) shieldChangeDir = false;
                else shieldChangeDir = true;
                if (Math.Abs(lastShieldPct - shieldPct) < 0.001)
                {
                    lastShieldPct = shieldPct;
                    shieldChanging = false;
                }
            }
            else shieldChanging = false;
            if (lastPowerPct != powerPct)
            {
                powerChanging = true;
                lastPowerPct = (lastPowerPct * 9f + powerPct) / 10f;
                if (lastPowerPct > powerPct) powerChangeDir = false;
                else powerChangeDir = true;
                if (Math.Abs(lastPowerPct - powerPct) < 0.001)
                {
                    lastPowerPct = powerPct;
                    powerChanging = false;
                }
            }
            else powerChanging = false;
            if (lastBoostPct != boostPct)
            {
                lastBoostPct = (lastBoostPct * 9f + boostPct) / 10f;
                if (Math.Abs(lastBoostPct - boostPct) < 0.001) lastBoostPct = boostPct;
            }

            currentHullRect = new Rectangle(hullRect.X, (int)((float)hullRect.Y + (float)hullRect.Height * (1f - lastHullPct)), hullRect.Width, (int)((float)hullRect.Height * lastHullPct));
            currentShieldRect = new Rectangle(shieldRect.X, (int)((float)shieldRect.Y + (float)shieldRect.Height * (1f - lastShieldPct)), shieldRect.Width, (int)((float)shieldRect.Height * lastShieldPct));
            currentPowerRect = new Rectangle(powerRect.X, (int)((float)powerRect.Y + (float)powerRect.Height * (1f - lastPowerPct)), powerRect.Width, (int)((float)powerRect.Height * lastPowerPct));
            currentBoostRect = new Rectangle(boostRect.X, (int)((float)boostRect.Y + (float)boostRect.Height * (1f - lastBoostPct)), boostRect.Width, (int)((float)boostRect.Height * lastBoostPct));
            currentHullPosY = (int)(Position.Y - (outlineRect.Height - hullRect.Height) + hullRect.Y);
            currentShieldPosY = (int)(Position.Y - (outlineRect.Height - shieldRect.Height) + shieldRect.Y);
            currentPowerPosY = (int)(Position.Y - (outlineRect.Height - powerRect.Height) + powerRect.Y);
            currentBoostPosY = (int)(Position.Y - (outlineRect.Height - boostRect.Height) + boostRect.Y);
            this.hullString = hullString;
            this.shieldString = shieldString;
            this.powerString = powerString;
        }

        public override void Draw(SpriteBatch b)
        {
            Draw(b, new Vector2());
        }

        public void Draw(SpriteBatch b, Vector2 offset)
        {
            b.Draw(texture, Position + offset, outlineRect, Color.White, 0, new Vector2(0, outlineRect.Height), new Vector2(1f, 1f), SpriteEffects.None, 0);

            b.Draw(texture, new Vector2(Position.X + offset.X, currentHullPosY + offset.Y), currentHullRect, hullChanging ? (hullChangeDir ? new Color(0, 255, 0) : new Color(256, 192, 0)) : Color.Red, 0, new Vector2(0, currentHullRect.Height), new Vector2(1f, 1f), SpriteEffects.None, 0);
            b.Draw(texture, new Vector2(Position.X + offset.X, currentShieldPosY + offset.Y), currentShieldRect, shieldChanging ? (shieldChangeDir ? new Color(255, 0, 255) : new Color(255, 0, 0)) : new Color(255, 0, 255), 0, new Vector2(0, currentShieldRect.Height), new Vector2(1f, 1f), SpriteEffects.None, 0);
            b.Draw(texture, new Vector2(Position.X + offset.X, currentPowerPosY + offset.Y), currentPowerRect, powerChanging ? (powerChangeDir ? new Color(0, 0, 255) : new Color(0, 192, 255)) : Color.Blue, 0, new Vector2(0, currentPowerRect.Height), new Vector2(1f, 1f), SpriteEffects.None, 0);
            b.Draw(texture, new Vector2(Position.X + offset.X, currentBoostPosY + offset.Y), currentBoostRect, Color.White, 0, new Vector2(0, currentBoostRect.Height), new Vector2(1f, 1f), SpriteEffects.None, 0);

            b.DrawString(font, hullString, new Vector2(Position.X + (offset.X + 10), Position.Y + (offset.Y - 10)), Color.White, (float)-Math.PI / 2f, new Vector2(), 1f, SpriteEffects.None, 0);
        }
    }
}
