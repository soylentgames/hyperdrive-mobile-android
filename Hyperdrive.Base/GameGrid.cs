﻿using Hyperdrive.Base.Collision;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base
{
    public class GameGrid
    {
        public int CellSize { get; private set; }
        public int NumberOfXCells { get; private set; }
        public int NumberOfYCells { get; private set; }
        public List<CollidableEntity>[,] Cells { get; private set; }

        public GameGrid(int cellSize)
        {
            ResizeGrid(cellSize);
        }

        public void ResizeGrid(int cellSize)
        {
            CellSize = cellSize;
            NumberOfXCells = GameDataManager.Gfx.GraphicsDevice.Viewport.Width / CellSize;
            NumberOfYCells = GameDataManager.Gfx.GraphicsDevice.Viewport.Height / CellSize;
            if (GameDataManager.Gfx.GraphicsDevice.Viewport.Width % CellSize != 0) NumberOfXCells++;
            if (GameDataManager.Gfx.GraphicsDevice.Viewport.Height % CellSize != 0) NumberOfYCells++;
            Cells = new List<CollidableEntity>[NumberOfXCells, NumberOfYCells];
            for (int x = 0; x < NumberOfXCells; x++)
                for (int y = 0; y < NumberOfYCells; y++)
                    Cells[x, y] = new List<CollidableEntity>();
        }

        public void AddToGrid(CollidableEntity e, int x, int y)
        {
            Cells[x, y].Add(e);
        }
        
        public int DebugItemCount()
        {
            int count = 0;
            for (int x = 0; x < NumberOfXCells; x++)
                for (int y = 0; y < NumberOfYCells; y++)
                    count += Cells[x, y].Count;
            return count;
        }

        public void Clear()
        {
            for (int x = 0; x < NumberOfXCells; x++)
                for (int y = 0; y < NumberOfYCells; y++)
                    Cells[x, y].Clear();
        }

        public void ConvertToGridCoords(int x, int y, out int gridX, out int gridY)
        {
            gridX = x / CellSize;
            gridY = y / CellSize;
        }

        public void AddToGrid(CollidableEntity e)
        {
            foreach (Vector2 point in e.Collider.GetGridPoints())
            {
                int x = (int)point.X;
                int y = (int)point.Y;
                ConvertToGridCoords(x, y, out x, out y);
                if (x >= 0 && x < NumberOfXCells && y >= 0 && y < NumberOfYCells && !Cells[x, y].Contains(e))
                    Cells[x, y].Add(e);
                
            }
        }
    }
}
