﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Hyperdrive.Base.Particle
{
    public class ParticleEntity : GameEntity
    {
        public static Texture2D texture { get; set; }
        private List<ParticleAI> types;
        public Color Color { get; set; }
        public Vector2 Scale { get; set; }
        public Rectangle ParticleRect { get; set; }
        public float LayerDepth { get; set; }
        public Vector2 ScaledSize { get { return new Vector2(ParticleRect.Width * Scale.X,ParticleRect.Height * Scale.Y); } }
        public bool Dead { get; set; }

        public ParticleEntity(Rectangle particleRect, List<ParticleAI> types, Vector2 pos, Vector2 vel, Color color, float rotation, Vector2 scale) : base(pos, vel)
        {
            ParticleRect = particleRect;
            this.types = types;
            Color = color;
            Rotation = rotation;
            Scale = scale;
            Dead = false;
            LayerDepth = 0;
            foreach (ParticleAI type in this.types) type.Init(this);
        }

        public ParticleEntity(Rectangle particleRect, List<ParticleAI> types, Vector2 pos, Vector2 vel, Color color, float rotation, float scale) : base(pos, vel)
        {
            ParticleRect = particleRect;
            this.types = types;
            Color = color;
            Rotation = rotation;
            Scale = new Vector2(scale,scale);
            Dead = false;
            LayerDepth = 0;
            foreach (ParticleAI type in this.types) type.Init(this);
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds); //clears ReturnList and updates position
            List<ParticleAI> trash = new List<ParticleAI>();

            foreach (ParticleAI type in types)
            {
                if (!type.Dead)
                {
                    type.Update(this, seconds); //by passing in this object we can set particles to its returnlist
                    if (type.Dead) trash.Add(type);
                }
                else trash.Add(type);
            }
            foreach (ParticleAI type in trash) types.Remove(type);
        }

        public override void Draw(SpriteBatch b) //rotation is in radians
        {
            b.Draw(texture, Position, ParticleRect, Color, Rotation, new Vector2(ParticleRect.Width / 2, ParticleRect.Height / 2), Scale, SpriteEffects.None, LayerDepth);
        }

        public void AddAI(ParticleAI ai)
        {
            types.Add(ai);
        }

        public bool HasAI(Type test)
        {
            foreach  (ParticleAI type in types)
            {
                if (type.GetType() == test)
                    return true;
            }
            return false;
        }

        public void KillAI(Type toKill)
        {
            foreach (ParticleAI type in types)
            {
                if (type.GetType() == toKill)
                    type.Dead = true;
            }
        }
    }
}
