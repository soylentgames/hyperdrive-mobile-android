﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    class ShrinkingLinearAI : ParticleAI
    {
        private float rate;

        public ShrinkingLinearAI(float rate)
        {
            this.rate = rate;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            p.Scale = new Vector2(p.Scale.X - rate, p.Scale.Y - rate);
            if (p.Scale.X <= 0 || p.Scale.Y <= 0) p.Dead = true;
        }
    }
}
