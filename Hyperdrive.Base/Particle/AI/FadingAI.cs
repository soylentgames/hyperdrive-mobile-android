﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Particle.AI
{
    public class FadingAI : ParticleAI
    {
        private float fadeRate;
        private float currentAlpha;
        private Color fullColor;

        public FadingAI(float fadeTime)
        {
            fadeRate = 1f / fadeTime;
            currentAlpha = 1f;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            if (currentAlpha == 1f) fullColor = new Color(p.Color,1f);
            currentAlpha -= fadeRate * seconds;
            if (currentAlpha < 0) currentAlpha = 0;
            p.Color = new Color(fullColor.ToVector3() * currentAlpha);
            if (currentAlpha == 0) p.Dead = true;
        }

    }
}
