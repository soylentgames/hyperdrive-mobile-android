﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class PulsingAI : ParticleAI
    {
        private float[] aiData;
        private float factor;
        private int pulseFrames;

        public PulsingAI(float factor, int pulseFrames)
        {
            this.factor = factor;
            this.pulseFrames = pulseFrames;
            aiData = new float[2];
        }

        public override void Update(ParticleEntity p, float seconds) //TODO update this ai to use gametime instead of frames
        {
            aiData[0]++;
            if (aiData[0] == pulseFrames)
            {
                aiData[0] = 0;
                if (aiData[1] == 0) aiData[1] = 1;
                else aiData[1] = 0;
            }
            if (aiData[1] == 0)
            {
                Vector2 scale = p.Scale;
                scale.X *= factor;
                scale.Y *= factor;
                p.Scale = scale;
            }
            else
            {
                Vector2 scale = p.Scale;
                scale.X /= factor;
                scale.Y /= factor;
                p.Scale = scale;
            }
        }

    }
}
