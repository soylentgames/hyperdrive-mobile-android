﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class GrowingAI : ParticleAI
    {
        private float rateX;
        private float rateY;

        public GrowingAI(float rate)
        {
            rateX = rate;
            rateY = rate;
        }

        public GrowingAI(float rateX, float rateY)
        {
            this.rateX = rateX;
            this.rateY = rateY;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            p.Scale = new Vector2(p.Scale.X * rateX,p.Scale.Y * rateY);
        }
    }
}
