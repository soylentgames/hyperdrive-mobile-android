﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class GradualForceAI : ParticleAI
    {
        private float forceX;
        private float forceY;

        public GradualForceAI(Vector2 force)
        {
            forceX = force.X;
            forceY = force.Y;
        }

        public GradualForceAI(float forceX, float forceY)
        {
            this.forceX = forceX;
            this.forceY = forceY;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            Vector2 vel = new Vector2(p.Velocity.X + forceX * seconds, p.Velocity.Y + forceY * seconds);
            p.Velocity = vel;
        }
    }
}
