﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Particle.AI
{
    public class TrailsFlameAI : ParticleAI
    {
        //public bool spawn = false;

        public override void Update(ParticleEntity p, float seconds)
        {
            List<ParticleAI> types = new List<ParticleAI>();
            types.Add(new FadingFlameAI(0.25f,p.Color.ToVector3()));
            p.ReturnList.Add(new ParticleEntity(p.ParticleRect, types, p.Position, new Vector2(), p.Color, p.Rotation, p.Scale));
        }
    }
}
