﻿namespace Hyperdrive.Base.Particle.AI
{
    public class RotatingAI : ParticleAI
    {
        private float spinSpeed;
        private float spinDirection;

        public RotatingAI(float spinSpeed)
        {
            this.spinSpeed = spinSpeed;
            spinDirection = GameMath.RNGer.NextBoolean() ? -1f : 1f;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            p.Rotation += spinSpeed * spinDirection;
        }
    }
}
