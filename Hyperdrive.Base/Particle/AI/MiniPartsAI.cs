﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class MiniPartsAI : ParticleAI
    {
        private int frequency;

        public MiniPartsAI(int frequency)
        {
            if (frequency < 1) frequency = 1;
            this.frequency = frequency;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            if (GameMath.RNGer.Next(frequency) == 0)
            {
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new FadingAI(0.25f));
                p.ReturnList.Add(new ParticleEntity(p.ParticleRect, types, p.Position + new Vector2((-p.ParticleRect.Width*p.Scale.X/2f) + (p.ParticleRect.Width * p.Scale.X), -(p.ParticleRect.Height*p.Scale.Y/2f) + (p.ParticleRect.Height * p.Scale.Y)), p.Velocity/2f, p.Color, p.Rotation, p.Scale/2f));
            }
        }
    }
}
