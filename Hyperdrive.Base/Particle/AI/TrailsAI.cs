﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class TrailsAI : ParticleAI
    {
        private float fadeTime;

        public TrailsAI(float fadeTime)
        {
            this.fadeTime = fadeTime;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            List<ParticleAI> types = new List<ParticleAI>();
            types.Add(new FadingAI(fadeTime));
            p.ReturnList.Add(new ParticleEntity(p.ParticleRect, types, p.Position, new Vector2(), p.Color, p.Rotation, p.Scale));
        }
    }
}
