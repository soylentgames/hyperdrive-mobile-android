﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class GrowingLinearAI : ParticleAI
    {
        private float rateX;
        private float rateY;

        public GrowingLinearAI(float rate)
        {
            rateX = rate;
            rateY = rate;
        }

        public GrowingLinearAI(float rateX, float rateY)
        {
            this.rateX = rateX;
            this.rateY = rateY;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            p.Scale = new Vector2(p.Scale.X + rateX * seconds, p.Scale.Y + rateY * seconds);
        }
    }
}
