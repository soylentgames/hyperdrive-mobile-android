﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class FlamingAI : ParticleAI
    {
        private static Rectangle flameRect = new Rectangle(256, 128, 64, 64);
        private static Rectangle flameRect2 = new Rectangle(128, 0, 64, 64);
        private static Rectangle flameRect3 = new Rectangle(384, 128, 64, 64);
        //public bool spawn = false;

        public override void Update(ParticleEntity p, float seconds)
        {
            p.Position = new Vector2(p.Position.X + (GameMath.RNGer.NextBoolean() ? 1f : -1f), p.Position.Y + (GameMath.RNGer.NextBoolean() ? 1f : -1f));
            
            int draw = GameMath.RNGer.Next(3);
            List<ParticleAI> types = new List<ParticleAI>();
            types.Add(new FadingFlameAI(0.2f,p.Color.ToVector3()));
            types.Add(new RotatingAI(0.03f));

            if (draw == 0) p.ReturnList.Add(new ParticleEntity(flameRect, types, p.Position + new Vector2(-8f + GameMath.RNGer.NextFloat() * 16f,-8f + GameMath.RNGer.NextFloat() * 16f), new Vector2(), p.Color, p.Rotation, p.Scale / 1.5f));
            else if (draw == 1) p.ReturnList.Add(new ParticleEntity(flameRect2, types, p.Position + new Vector2(-8f + GameMath.RNGer.NextFloat() * 16f, -8f + GameMath.RNGer.NextFloat() * 16f), new Vector2(), p.Color, p.Rotation, p.Scale / 1.5f));
            else if (draw == 2) p.ReturnList.Add(new ParticleEntity(flameRect3, types, p.Position + new Vector2(-8f + GameMath.RNGer.NextFloat() * 16f, -8f + GameMath.RNGer.NextFloat() * 16f), new Vector2(), p.Color, p.Rotation, p.Scale / 1.5f));
        }
    }
}
