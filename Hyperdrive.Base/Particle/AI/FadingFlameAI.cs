﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Particle.AI
{
    public class FadingFlameAI : ParticleAI
    {
        private float fadeRed;
        private float fadeGreen;
        private float fadeBlue;
        private float currentRed;
        private float currentBlue;
        private float currentGreen;

        public FadingFlameAI(float fadeTime, Vector3 color)
        {
            fadeRed = 1f / fadeTime;
            fadeGreen = fadeRed * 1.125f;
            fadeBlue = fadeRed * 1.25f;
            currentRed = color.X;
            currentGreen = color.Y;
            currentBlue = color.Z;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            currentRed -= fadeRed * seconds;
            currentGreen -= fadeGreen * seconds;
            currentBlue -= fadeBlue * seconds;
            if (currentRed < 0) currentRed = 0;
            if (currentGreen < 0) currentGreen = 0;
            if (currentBlue < 0) currentBlue = 0;
            p.Color = new Color(currentRed, currentGreen, currentBlue);
            if (currentRed == 0) p.Dead = true;
        }

    }
}
