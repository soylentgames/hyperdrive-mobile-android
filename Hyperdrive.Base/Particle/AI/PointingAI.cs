﻿namespace Hyperdrive.Base.Particle.AI
{
    // PointingAI points the particle in the direction it's moving
    public class PointingAI : ParticleAI
    {
        public override void Update(ParticleEntity p, float seconds)
        {
            p.Rotation = GameMath.RotationFromVector(p.Velocity);
        }

        public override void Init(ParticleEntity p)
        {
            p.Rotation = GameMath.RotationFromVector(p.Velocity);
        }
    }
}
