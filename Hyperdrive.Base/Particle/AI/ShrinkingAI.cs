﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class ShrinkingAI : ParticleAI
    {
        private bool firstRun = true;
        private Vector2 initialScale;
        private float rate;

        public ShrinkingAI(float shrinkTime)
        {
            rate = 1f / shrinkTime;
        }

        public override void Update(ParticleEntity p, float seconds)
        {
            if (firstRun) initialScale = p.Scale;
            p.Scale = new Vector2(p.Scale.X - (rate * initialScale.X * seconds),p.Scale.Y - (rate * initialScale.Y * seconds));
            if (p.Scale.X <= 0f || p.Scale.Y <= 0f) p.Dead = true;
        }
    }
}
