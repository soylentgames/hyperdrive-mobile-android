﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Particle.AI
{
    public class LoadScreenFlyingAI : ParticleAI
    {
        public override void Update(ParticleEntity p, float seconds)
        {
            if (p.Position.Y > GameDataManager.Gfx.GraphicsDevice.Viewport.Height / 2 + 178f && p.HasAI(typeof(LoadScreenFlyingAI))) 
		    {
                if (GameMath.RNGer.NextBoolean())
                {
                    GameDataManager.AddSound(GameDataManager.Sounds["plop1"], -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 0.3f + GameMath.RNGer.NextFloat() * 0.7f);
                }

                p.Dead = true;
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new LoadScreenPloppingAI());
                p.ReturnList.Add(new ParticleEntity(p.ParticleRect, types, new Vector2(p.Position.X, p.Position.Y),
                    new Vector2(-40f, 0f), p.Color, 0, new Vector2(p.Scale.X * 1.5f, p.Scale.Y / 1.5f)));
                types = new List<ParticleAI>();
                types.Add(new LoadScreenPloppingAI());
                p.ReturnList.Add(new ParticleEntity(p.ParticleRect, types, new Vector2(p.Position.X, p.Position.Y),
                    new Vector2(40f, 0f), p.Color, 0, new Vector2(p.Scale.X * 1.5f, p.Scale.Y / 1.5f)));
		    }
	    }
    }
}
