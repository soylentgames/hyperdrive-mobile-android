﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class LoadScreenPloppingAI : ParticleAI
    {
        public override void Update(ParticleEntity p, float seconds)
        {
            Vector2 vel = new Vector2(p.Velocity.X * 0.95f, 0);
            p.Velocity = vel;
        }
    }
}
