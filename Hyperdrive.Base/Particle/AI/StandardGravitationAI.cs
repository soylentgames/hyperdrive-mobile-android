﻿using Microsoft.Xna.Framework;

namespace Hyperdrive.Base.Particle.AI
{
    public class StandardGravitationAI : ParticleAI
    {
        public override void Update(ParticleEntity p, float seconds)
        {
            Vector2 vel = new Vector2(p.Velocity.X * 0.98f, p.Velocity.Y + 7.5f);
            p.Velocity = vel;
        }
    }
}
