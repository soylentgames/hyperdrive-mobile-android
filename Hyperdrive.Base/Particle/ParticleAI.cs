﻿namespace Hyperdrive.Base.Particle
{
    public abstract class ParticleAI
    {
        public bool Dead { get; set; }

        protected ParticleAI()
        {
            Dead = false;
        }

        public abstract void Update(ParticleEntity p, float seconds);

        public virtual void Init(ParticleEntity p)
        {

        }
    }
}
