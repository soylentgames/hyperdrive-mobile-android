﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Hyperdrive.Base.Particle
{
    public class CustomParticleEntity : ParticleEntity
    {
        private Texture2D customTexture;

        public CustomParticleEntity(Texture2D texture, Rectangle particleRect, List<ParticleAI> types, Vector2 pos, Vector2 vel, Color color, float rotation, Vector2 scale) : base(particleRect, types, pos, vel, color, rotation, scale)
        {
            customTexture = texture;
        }

        public CustomParticleEntity(Texture2D texture, Rectangle particleRect, List<ParticleAI> types, Vector2 pos, Vector2 vel, Color color, float rotation, float scale) : base(particleRect, types, pos, vel, color, rotation, scale)
        {
            customTexture = texture;
        }

        public override void Draw(SpriteBatch b)
        {
            b.Draw(customTexture, Position, ParticleRect, Color, Rotation, new Vector2(ParticleRect.Width / 2f, ParticleRect.Height / 2f), Scale, SpriteEffects.None, 0f);
        }
    }
}
