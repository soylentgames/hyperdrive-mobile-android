﻿using Microsoft.Xna.Framework;
using System;

namespace Hyperdrive.Base.Gfx
{
    public class AnimationFrame
    {
        public Rectangle SourceRectangle { get; set; }
        public TimeSpan Duration { get; set; }

        public AnimationFrame(Rectangle rectangle, TimeSpan duration)
        {
            SourceRectangle = rectangle;
            Duration = duration;
        }
    }
}
