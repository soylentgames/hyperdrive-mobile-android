﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Hyperdrive.Base.Gfx
{
    public abstract class SpriteController
    {
        protected Dictionary<AnimationType, Animation> animations;
        protected Animation currentAnimation;
        public Rectangle CurrentRect { get { return currentAnimation.CurrentRectangle; } }
        public Color Color { get; set; }

        public SpriteController()
        {
            Color = Color.White;
            animations = new Dictionary<AnimationType, Animation>();
        }
        
        public void Update(float seconds)
        {
            currentAnimation.Update(seconds);
        }
    }
}
