﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyperdrive.Base.Gfx
{
    public enum AnimationType
    {
        Default,
        Player_Normal,
        Player_Damage1,
        Player_Damage2,
        Player_Damage3,
        Player_LeftWing_Normal,
        Player_LeftWing_Damage1,
        Player_LeftWing_Damage2,
        Player_LeftWing_Damage3,
        Player_LeftWing_Destroyed,
        Player_RightWing_Normal,
        Player_RightWing_Damage1,
        Player_RightWing_Damage2,
        Player_RightWing_Damage3,
        Player_RightWing_Destroyed,
        Player_Shot1
    }

    public class Animation
    {
        List<AnimationFrame> frames = new List<AnimationFrame>();
        TimeSpan timeIntoAnimation;
        TimeSpan Duration
        {
            get
            {
                double totalSeconds = 0;
                foreach (var frame in frames)
                    totalSeconds += frame.Duration.TotalSeconds;
                return TimeSpan.FromSeconds(totalSeconds);
            }
        }

        public Animation(List<AnimationFrame> frames)
        {
            foreach (AnimationFrame f in frames) this.frames.Add(f);
        }

        public Animation()
        {
        }

        public List<AnimationFrame> GetFrames()
        {
            return frames;
        }

        public Rectangle CurrentRectangle
        {
            get
            {
                AnimationFrame currentFrame = null;
                TimeSpan accumulatedTime = new TimeSpan();
                foreach (var frame in frames)
                {
                    if (accumulatedTime + frame.Duration >= timeIntoAnimation)
                    {
                        currentFrame = frame;
                        break;
                    }
                    else
                        accumulatedTime += frame.Duration;
                }

                if (currentFrame == null)
                {
                    currentFrame = frames.LastOrDefault();
                }

                if (currentFrame != null)
                    return currentFrame.SourceRectangle;
                else
                    return Rectangle.Empty;
            }
        }

        public void AddFrame(Rectangle rect, TimeSpan dur)
        {
            AnimationFrame frame = new AnimationFrame(rect, dur);
            frames.Add(frame);
        }

        public void Update(double dSeconds)
        {
            double secondsIntoAnim = timeIntoAnimation.TotalSeconds + dSeconds;

            double remaining = secondsIntoAnim % Duration.TotalSeconds;

            timeIntoAnimation = TimeSpan.FromSeconds(remaining);
        }
    }
}
