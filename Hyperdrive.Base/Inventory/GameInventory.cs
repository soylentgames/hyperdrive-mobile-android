﻿using System.Collections.Generic;

namespace Hyperdrive.Base.Inventory
{
    public class GameInventory
    {
        public List<GameItem> Items { get; set; }
        public int MaxItems { get; protected set; }
        public int DisplayWidth { get; protected set; }
        public int DisplayHeight { get; protected set; }

    }
}
