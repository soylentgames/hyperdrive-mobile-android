﻿using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory.Frames
{
    public class ShieldGeneratorItem : EquipmentItem
    {
        public float MaxShields { get; protected set; }
        public float ShieldRegen { get; protected set; }

        public ShieldGeneratorItem(int id, float quality, float damage, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            Reset();
        }
        
        public override string GetName()
        {
            switch (ItemID)
            {
                case 1: return "Screen I";
            }
            return "";
        }

        public override void Reset()
        {
            base.Reset();
            switch (ItemID)
            {
                case 1:
                    MaxShields = 15f;
                    ShieldRegen = 2f;
                    Weight = 10f;
                    break; 
            }
        }
    }
}
