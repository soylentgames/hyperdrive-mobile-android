﻿using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory.Frames
{
    public class PowerCoreItem : EquipmentItem
    {
        public float PowerCapacity {get; protected set;}
        public float PowerRegen { get; protected set; }

        public PowerCoreItem(int id, float quality, float damage, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            Reset();
        }
        
        public override string GetName()
        {
            switch (ItemID)
            {
                case 1: return "SoloGen I";
            }
            return "";
        }

        public override void Reset()
        {
            base.Reset();
            switch (ItemID)
            {
                case 1:
                    ActivePowerConsumption = 0f;
                    PassivePowerConsumption = 0f;
                    PowerCapacity = 150f;
                    PowerRegen = 10f;
                    Weight = 40f;
                    break;
            }
        }
    }
}
