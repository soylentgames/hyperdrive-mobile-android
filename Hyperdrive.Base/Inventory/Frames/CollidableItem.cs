﻿using System.Collections.Generic;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory.Frames
{
    public abstract class CollidableItem : GameItem
    {
        public Collider Collider { get; protected set; }
        public bool Destroyed { get; protected set; }
        
        public CollidableItem(int id, float quality, float damage, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            Reset();
        }
        
        public override float GetMaxAmount()
        {
            return 1f;
        }

        public virtual void Reset()
        {
            Destroyed = false;
        }

        public virtual void Destroy()
        {
            Destroyed = true;
        }

        public abstract bool Hit(float damage);

        public abstract List<ParticleEntity> GetDeathParticles();
    }
}
