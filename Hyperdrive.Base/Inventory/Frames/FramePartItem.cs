﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Particle.AI;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory.Frames
{
    public enum FramePartType
    {
        LEFT_WING,
        RIGHT_WING,
        SUPPORT
    }

    public class FramePartItem : CollidableItem
    {
        //class for subparts of the ship, these will be wings, weapon platforms, etc
        public float MaxHull { get; protected set; } //these values are relative to the part only
        public float Hull { get; protected set; }
        public float ExplodeDamage { get; protected set; }
        public FramePartType Type { get; protected set; }
        public FrameItem MainBody { get; protected set; }
        public float HullPercentage { get { return Hull / MaxHull; } }
        private Vector2 ExplosionOrigin { get; set; }

        public float MaxShields { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in ShieldPoints) if (hp.EquippedItem != null) total += ((ShieldGeneratorItem)hp.EquippedItem).MaxShields;
                return total;
            } }
        public float ShieldRegen { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in ShieldPoints) if (hp.EquippedItem != null) total += ((ShieldGeneratorItem)hp.EquippedItem).ShieldRegen;
                return total;
            } }
        public float PowerCapacity { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in CorePoints) if (hp.EquippedItem != null) total += ((PowerCoreItem)hp.EquippedItem).PowerCapacity;
                return total;
            } }
        public float PowerRegen { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in CorePoints) if (hp.EquippedItem != null) total += ((PowerCoreItem)hp.EquippedItem).PowerRegen;
                return total;
            } }
        public float PassivePowerConsumption { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in WeaponPoints) if (hp.EquippedItem != null) total += hp.EquippedItem.PassivePowerConsumption;
                foreach (FrameHardpoint hp in ShieldPoints) if (hp.EquippedItem != null) total += hp.EquippedItem.PassivePowerConsumption;
                foreach (FrameHardpoint hp in CorePoints) if (hp.EquippedItem != null) total += hp.EquippedItem.PassivePowerConsumption;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null) total += hp.EquippedItem.PassivePowerConsumption;
                foreach (FrameHardpoint hp in EnhancementPoints) if (hp.EquippedItem != null) total += hp.EquippedItem.PassivePowerConsumption;
                return total;
            } }
        public float Acceleration { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total += ((EngineItem)hp.EquippedItem).Acceleration;
                return total;
            } }
        public float MaxSpeed { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total += ((EngineItem)hp.EquippedItem).MaxSpeed;
                return total;
            } }
        public float BoostCost { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total += ((EngineItem)hp.EquippedItem).ActivePowerConsumption;
                return total;
            } }
        public float BoostTime { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total += ((EngineItem)hp.EquippedItem).BoostTime;
                return total;
            } }
        public float BoostCooldown { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total += ((EngineItem)hp.EquippedItem).BoostCooldown;
                return total;
            } }
        public float BoostSpeedMultiplier { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total += ((EngineItem)hp.EquippedItem).BoostSpeedMultiplier;
                return total;
            } }
        public float BoostAccelMultiplier { get
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total += ((EngineItem)hp.EquippedItem).BoostAccelMultiplier;
                return total;
            } }
        public float BoostFactor { get //for doing math with boostcooldown and boosttime
            {
                float total = 0;
                foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) total++;
                return total;
            } }

        public List<FrameHardpoint> WeaponPoints { get; protected set; }
        public List<FrameHardpoint> ShieldPoints { get; protected set; }
        public List<FrameHardpoint> CorePoints { get; protected set; }
        public List<FrameHardpoint> EnginePoints { get; protected set; }
        public List<FrameHardpoint> EnhancementPoints { get; protected set; }
        
        public FramePartItem(int id, float quality, float damage, FrameItem mainBody, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            MainBody = mainBody;
            Reset();
        }

        public override string GetName()
        {
            switch (ItemID)
            {
                case 1: return "Sparrow Standard Left Wing";
                case 2: return "Sparrow Standard Right Wing";
            }
            return "";
        }

        public override bool Hit(float damage)
        {
            //TODO: figure out how I'm going to protect the wings and such with the shield value for the whole frame
            if (Hull < damage)
            {
                Destroyed = true;
                DestroySubparts();
                Hull = 0;
            }
            else
            {
                Hull -= damage;
            }
            if (Type == FramePartType.LEFT_WING) MainBody.SpriteController.LeftWingHit();
            else if (Type == FramePartType.RIGHT_WING) MainBody.SpriteController.RightWingHit();

            return Destroyed;
        }

        public void DestroySubparts()
        {
            foreach (FrameHardpoint hp in WeaponPoints) if (hp.EquippedItem != null) hp.EquippedItem.Destroy();
            foreach (FrameHardpoint hp in ShieldPoints) if (hp.EquippedItem != null) hp.EquippedItem.Destroy();
            foreach (FrameHardpoint hp in CorePoints) if (hp.EquippedItem != null) hp.EquippedItem.Destroy();
            foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null) hp.EquippedItem.Destroy();
            foreach (FrameHardpoint hp in EnhancementPoints) if (hp.EquippedItem != null) hp.EquippedItem.Destroy();
        }

        public virtual Vector2 GetExplosionPosition(Vector2 position)
        {
            return new Vector2(position.X + ExplosionOrigin.X, position.Y + ExplosionOrigin.Y);
        }

        public override void Reset()
        {
            base.Reset();
            WeaponPoints = new List<FrameHardpoint>();
            ShieldPoints = new List<FrameHardpoint>();
            CorePoints = new List<FrameHardpoint>();
            EnginePoints = new List<FrameHardpoint>();
            EnhancementPoints = new List<FrameHardpoint>();
            switch (ItemID)
            {
                case 1:
                    Type = FramePartType.LEFT_WING;
                    MaxHull = 100;
                    Hull = 100;
                    ExplodeDamage = 3;
                    Collider = new RectCollider(new Vector2(32f, 31f), new Vector2(-16f, 12f), Owner.Entity, ColliderType.Player);
                    ExplosionOrigin = new Vector2(-11f, 13f);
                    break;
                case 2:
                    Type = FramePartType.RIGHT_WING;
                    MaxHull = 100;
                    Hull = 100;
                    ExplodeDamage = 3;
                    Collider = new RectCollider(new Vector2(32f, 31f), new Vector2(16f, 12f), Owner.Entity, ColliderType.Player);
                    ExplosionOrigin = new Vector2(11f, 13f);
                    break;
            }
        }

        public override List<ParticleEntity> GetDeathParticles()
        {
            List<ParticleEntity> spawned = new List<ParticleEntity>();
            Vector2 explosionPosition = GetExplosionPosition(Owner.Entity.Position);

            List<ParticleAI> types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.12f));
            types.Add(new RotatingAI(0.25f));
            types.Add(new FadingAI(0.5f));
            spawned.Add(new ParticleEntity(new Rectangle(320, 0, 64, 64), types, explosionPosition, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(0.17f, 0.175f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.06f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(0.9f));
            spawned.Add(new ParticleEntity(new Rectangle(384, 0, 64, 64), types, explosionPosition, new Vector2(), new Color(255, 128, 128), 0f, new Vector2(0.27f, 0.27f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.09f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(1.0f));
            spawned.Add(new ParticleEntity(new Rectangle(576, 0, 64, 64), types, explosionPosition, new Vector2(), new Color(255, 192, 64), 0f, new Vector2(0.11f, 0.11f)));

            //shockwave ring
            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.12f));
            types.Add(new RotatingAI(0.25f));
            types.Add(new FadingAI(0.75f));
            spawned.Add(new ParticleEntity(new Rectangle(0, 512, 128, 128), types, explosionPosition, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(0.12f, 0.12f)));

            Rectangle flameRect = new Rectangle(256, 128, 64, 64);
            Rectangle glowRect = new Rectangle(576, 0, 64, 64);

            for (int count = 0; count < 4; count++)
            {
                Color color = new Color(255, 192 + GameMath.RNGer.Next(64), 64 + GameMath.RNGer.Next(128));
                types = new List<ParticleAI>();
                types.Add(new RotatingAI(0.05f));
                types.Add(new FadingFlameAI(1f, color.ToVector3()));
                types.Add(new ShrinkingAI(2f));
                types.Add(new FlamingAI());
                types.Add(new GradualForceAI(new Vector2(-200f + GameMath.RNGer.NextFloat() * 400f, -200f + GameMath.RNGer.NextFloat() * 400f)));
                float sizeMod = GameMath.RNGer.NextFloat();
                spawned.Add(new ParticleEntity(flameRect, types, explosionPosition, new Vector2(-100f + GameMath.RNGer.NextFloat() * 200f, -100f + GameMath.RNGer.NextFloat() * 200f), color, 0f, new Vector2(0.5f + sizeMod, 0.5f + sizeMod)));
            }

            Color color2 = new Color(255, 255, 128);
            float explodeMod = 0f;
            switch (Type)
            {
                case FramePartType.LEFT_WING:
                    types = new List<ParticleAI>();
                    types.Add(new GrowingLinearAI(8f,4f));
                    types.Add(new GradualForceAI(-400f, 0f));
                    types.Add(new FadingAI(1.0f));
                    spawned.Add(new ParticleEntity(new Rectangle(576, 0, 64, 64), types, explosionPosition, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(1f, 1f)));

                    for (int i = 0; i < 15; i++)
                    {
                        explodeMod = 0.2f + GameMath.RNGer.NextFloat() * 0.4f;
                        types = new List<ParticleAI>();
                        types.Add(new GrowingLinearAI(0.075f,0.6f));
                        types.Add(new PointingAI());
                        types.Add(new FadingAI(1.5f));
                        types.Add(new GradualForceAI(0, 300f));
                        types.Add(new TrailsFlameAI());
                        spawned.Add(new ParticleEntity(glowRect, types, explosionPosition + new Vector2(-7f + GameMath.RNGer.NextFloat() * 14f, -7f + GameMath.RNGer.NextFloat() * 14f), new Vector2(-1000f + GameMath.RNGer.NextFloat() * 500f, -300f + GameMath.RNGer.NextFloat() * 600f),color2,0f,new Vector2(explodeMod/8f, explodeMod)));
                    }
                    break;
                case FramePartType.RIGHT_WING:
                    types = new List<ParticleAI>();
                    types.Add(new GrowingLinearAI(8f, 4f));
                    types.Add(new GradualForceAI(400f, 0f));
                    types.Add(new FadingAI(1.0f));
                    spawned.Add(new ParticleEntity(new Rectangle(576, 0, 64, 64), types, explosionPosition, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(1f, 1f)));
                    for (int i = 0; i < 15; i++)
                    {
                        explodeMod = 0.2f + GameMath.RNGer.NextFloat() * 0.4f;
                        types = new List<ParticleAI>();
                        types.Add(new GrowingLinearAI(0.075f, 0.6f));
                        types.Add(new PointingAI());
                        types.Add(new FadingAI(1.5f));
                        types.Add(new GradualForceAI(0, 300f));
                        types.Add(new TrailsFlameAI());
                        spawned.Add(new ParticleEntity(glowRect, types, explosionPosition + new Vector2(-7f + GameMath.RNGer.NextFloat() * 14f, -7f + GameMath.RNGer.NextFloat() * 14f), new Vector2(500f + GameMath.RNGer.NextFloat() * 500f, -300f + GameMath.RNGer.NextFloat() * 600f), color2, 0f, new Vector2(explodeMod / 8f, explodeMod)));
                    }
                    break;
            }
            return spawned;
        }
    }
}