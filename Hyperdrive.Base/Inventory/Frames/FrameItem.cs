﻿using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Container;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Particle.AI;
using Hyperdrive.Base.Player;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Hyperdrive.Base.Inventory.Frames
{
    public class FrameItem : CollidableItem
    {
        public static Texture2D particles;
        public static SoundEffect standardFireSound;  //gun sounds should all be loaded here statically
        public static SoundEffect boostSound;
        public static SoundEffect boostSustainSound;

        public PlayerSpriteController SpriteController { get; protected set; }

        public List<FrameHardpoint> WeaponPoints { get; protected set; }
        public List<FrameHardpoint> ShieldPoints { get; protected set; }
        public List<FrameHardpoint> CorePoints { get; protected set; }
        public List<FrameHardpoint> EnginePoints { get; protected set; }
        public List<FrameHardpoint> EnhancementPoints { get; protected set; }

        public float MaxWeight { get; protected set; }
        public float Power { get; protected set; }
        public float Boost { get; protected set; }
        public float Hull { get; protected set; }
        public float MaxHull { get; protected set; }
        public float PassivePowerConsumption { get; protected set; }
        public float Shields { get; protected set; }
        public float MaxShields { get; protected set; }
        public float ShieldRegen { get; protected set; }
        public float PowerCapacity { get; protected set; }
        public float ActualPowerCapacity { get; protected set; }
        public float PowerRegen { get; protected set; }
        public float Acceleration { get; protected set; }
        public float MaxSpeed { get; protected set; }
        public float BoostTime { get; protected set; }
        public float BoostCooldown { get; protected set; }
        public float BoostSpeedMultiplier { get; protected set; }
        public float BoostAccelMultiplier { get; protected set; }
        public float BoostPowerCost { get; protected set; }
        public float BoostCost { get; protected set; }
        public float BoostRegen { get; protected set; }
        public List<WeaponItem> EquippedWeapons { get
            {
                List<WeaponItem> list = new List<WeaponItem>();
                foreach (FrameHardpoint hp in WeaponPoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) list.Add((WeaponItem)hp.EquippedItem);
                foreach (FramePartItem p in SubParts) foreach (FrameHardpoint hp in p.WeaponPoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed) list.Add((WeaponItem)hp.EquippedItem);
                return list;
            } }

        public float WingDependency { get; protected set; } //how much the ship's max velocity, acceleration, and deceleration will decrease by when a wing is destroyed
        private List<FramePartItem> SubParts { get; set; }

        public float HullPercentage { get { return Hull / MaxHull; } }
        public float ShieldPercentage { get { return MaxShields > 0 ? Shields / MaxShields : 0f; } } //TODO:  update most of this stuff to pull from equipped items
        public float PowerPercentage { get
            {
                return ActualPowerCapacity > 0 ? Power / ActualPowerCapacity : 0f;
            } }
        public float BoostPercentage { get { return Boost / 100f; } }
        public List<CollidableItem> CollidableParts { get
            {
                List<CollidableItem> list = new List<CollidableItem>();
                list.Add(this);
                list.AddRange(SubParts);
                return list;
            } }
        
        public Dictionary<CollidableItem,Collider> Colliders { get
            {
                Dictionary<CollidableItem, Collider> colliders = new Dictionary<CollidableItem, Collider>();
                foreach (CollidableItem c in CollidableParts) colliders.Add(c, c.Collider);
                return colliders;
            } }

        private double shieldEffectCount;
        private double lastBoostCount; //seconds since last boost attempt (both success or failure)
        private double boostSustained; //seconds boost has been held consistently
        private double lastHit; //seconds since player was last hit, for drawing the player sprite reddish

        private float effect1;
        private float effect2;
        private float effect3;
        private float effect4;
        private Color effectColor;

        private Rectangle shieldBubble;
        private Rectangle shieldGlow;
        private Rectangle shieldSubstance;

        public FrameItem(int id, int amount, float quality, float damage, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            Amount = amount;
            shieldBubble = new Rectangle(704, 0, 64, 64); //TODO: change this to be gotten from the shield equipment
            shieldGlow = new Rectangle(576, 0, 64, 64);
            shieldSubstance = new Rectangle(384, 0, 64, 64);
            Reset();
        }

        public void UpdateFrameStats()
        {
            //update stats which are dependent on equipped items
            float passivePowerConsumption = 0;

            float maxShields = 0;
            float shieldRegen = 0;
            float powerCapacity = 0;
            float powerRegen = 0;
            float acceleration = 0;
            float maxSpeed = 0;
            float boostTime = 0;
            float boostCooldown = 0;
            float boostSpeedMult = 0;
            float boostAccelMult = 0;
            float boostCount = 0;
            foreach (FrameHardpoint hp in ShieldPoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed)
                {
                    ShieldGeneratorItem g = (ShieldGeneratorItem)hp.EquippedItem;
                    maxShields += g.MaxShields;
                    shieldRegen += g.ShieldRegen;
                    passivePowerConsumption += g.PassivePowerConsumption;
                }
            foreach (FrameHardpoint hp in CorePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed)
                {
                    PowerCoreItem c = (PowerCoreItem)hp.EquippedItem;
                    powerCapacity += c.PowerCapacity;
                    powerRegen += c.PowerRegen;
                    passivePowerConsumption += c.PassivePowerConsumption;
                }
            foreach (FrameHardpoint hp in EnginePoints) if (hp.EquippedItem != null && !hp.EquippedItem.Destroyed)
                {
                    EngineItem e = (EngineItem)hp.EquippedItem;
                    acceleration += e.Acceleration;
                    maxSpeed += e.MaxSpeed;
                    boostTime += e.BoostTime;
                    boostCooldown += e.BoostCooldown;
                    boostAccelMult += e.BoostAccelMultiplier;
                    boostSpeedMult += e.BoostSpeedMultiplier;
                    boostCount++;
                    passivePowerConsumption += e.PassivePowerConsumption;
                }
            foreach (FramePartItem p in SubParts)
            {
                maxShields += p.MaxShields;
                shieldRegen += p.ShieldRegen;
                powerCapacity += p.PowerCapacity;
                powerRegen += p.PowerRegen;
                acceleration += p.Acceleration;
                maxSpeed += p.MaxSpeed;
                boostTime += p.BoostTime;
                boostCooldown += p.BoostCooldown;
                boostAccelMult += p.BoostAccelMultiplier;
                boostSpeedMult += p.BoostSpeedMultiplier;
                boostCount += p.BoostFactor;
                passivePowerConsumption += p.PassivePowerConsumption;
            }
            MaxShields = maxShields;
            ShieldRegen = shieldRegen;
            PowerCapacity = powerCapacity;
            PowerRegen = powerRegen;
            Acceleration = acceleration;
            MaxSpeed = maxSpeed;
            BoostTime = boostTime / boostCount;
            BoostCooldown = boostCooldown / boostCount;
            BoostAccelMultiplier = boostAccelMult / boostCount;
            BoostSpeedMultiplier = boostSpeedMult / boostCount;
            PassivePowerConsumption = passivePowerConsumption;
            ActualPowerCapacity = PowerCapacity - passivePowerConsumption;
            BoostCost = 100f / BoostTime;
            BoostRegen = 100f / BoostCooldown;
            
    }

        public List<ParticleEntity> Update(float seconds, double dSeconds, bool isFiring, Vector2 position, Vector2 direction, Vector2 velocity, EntityContainer shotPool, bool isBoosting, out Vector2 newVel)
        {
            List<WeaponItem> equippedWeapons = EquippedWeapons; 
            foreach (WeaponItem w in equippedWeapons) w.Update(dSeconds);
            lastHit += dSeconds;

            UpdateFrameStats();

            //regen shields if last hit more than 2 seconds ago
            if (lastHit >= 2) 
            {
                Shields += ShieldRegen * seconds;
                if (Shields > MaxShields) Shields = MaxShields;
            }

            //regen power
            Power += PowerRegen * seconds;
            if (Power > ActualPowerCapacity) Power = ActualPowerCapacity;

            //update shield effect variables
            shieldEffectCount += dSeconds;
            shieldEffectCount = shieldEffectCount % 1;
            effect1 = (float)((Math.Sin(shieldEffectCount * Math.PI * 2) + 1) / 2);
            effect2 = (float)((Math.Cos(shieldEffectCount * Math.PI * 2) + 1) / 2);
            effect3 = (float)((Math.Sin(shieldEffectCount * Math.PI * 2 + Math.PI) + 1) / 2);
            effect4 = (float)((Math.Cos(shieldEffectCount * Math.PI * 2 + Math.PI) + 1) / 2);
            effectColor = new Color(ShieldPercentage, ShieldPercentage, ShieldPercentage / 2f);

            //update sprite
            bool leftWingDestroyed = false;
            bool rightWingDestroyed = false;
            float leftHullPct = 1f;
            float rightHullPct = 1f;
            foreach (FramePartItem f in SubParts)
                if (f.Type == FramePartType.LEFT_WING) { leftHullPct = f.HullPercentage; leftWingDestroyed = f.Destroyed; }
                else if (f.Type == FramePartType.RIGHT_WING) { rightHullPct = f.HullPercentage; rightWingDestroyed = f.Destroyed; }
            SpriteController.Update(HullPercentage, leftHullPct, rightHullPct, seconds, dSeconds);

            //firing handling
            if (isFiring)
            {
                foreach (WeaponItem w in equippedWeapons) if (w.CanFire && w.ActivePowerConsumption < Power)
                    {
                        Power -= w.ActivePowerConsumption;
                        shotPool.AddEntity(w.FireWeapon());
                        GameDataManager.AddSound(standardFireSound, -0.1f + GameMath.RNGer.NextFloat() * 0.2f, 1f);
                    }
            }

            //movement 
            if (!isBoosting)
            {
                lastBoostCount += seconds; //if not boosting, add time to lastBoostCount to help track the initial bursts 
                boostSustained = 0; //reset sustain sound too, not boosting
                Boost += BoostRegen * seconds;
                if (Boost > 100f) Boost = 100f;
            }
            if (direction.X != 0 || direction.Y != 0)
            {
                float boostSpeedMult = 1f;
                float boostAccelMult = 1f;
                float currentPower = BoostPowerCost * seconds;
                float currentCost = BoostCost * seconds;
                //calculate boost
                if (isBoosting && Power >= currentPower && Boost >= currentCost) //trying to boost and has the power
                {
                    if (lastBoostCount >= 0.25) //been long enough since last boost, fire rings and sound 
                    { 
                        GameDataManager.AddSound(boostSound, 0, 1f); //TODO fire particle rings
                    }
                    lastBoostCount = 0;
                    boostSustained += seconds;
                    if (boostSustained >= 0.25) //play sustain sound
                    {
                        boostSustained = 0;
                        GameDataManager.AddSound(boostSustainSound, -0.2f + (1f - BoostPercentage), 1f);
                    }
                    Power -= currentPower;
                    Boost -= currentCost;
                    boostSpeedMult = BoostSpeedMultiplier;
                    boostAccelMult = BoostAccelMultiplier;
                }
                else if (isBoosting) //trying to boost but failing
                {
                    isBoosting = false;
                    lastBoostCount = 0;
                    boostSustained = 0; 
                }
                //actual movement
                float wingDestructionMult = 1f - ((leftWingDestroyed ? WingDependency : 0f) + (rightWingDestroyed ? WingDependency : 0f));
                velocity += direction * (Acceleration * wingDestructionMult) * boostAccelMult * seconds;
                if (velocity.Length() > (MaxSpeed * wingDestructionMult) * boostSpeedMult)
                {
                    velocity.Normalize();
                    velocity *= (MaxSpeed * wingDestructionMult) * boostSpeedMult;
                }
            }
            else velocity *= 0.9f; //decay 
            newVel = velocity;

            List<ParticleEntity> newParticles = new List<ParticleEntity>();
            //generate rocket thrust effects
            float size = 0.02f + GameMath.RNGer.NextFloat() * 0.18f; //small ones
            List<ParticleAI> types = new List<ParticleAI>();
            types.Add(new FadingAI(0.45f));
            types.Add(new RotatingAI(0.3f));
            types.Add(new GrowingLinearAI(1.2f));
            ParticleEntity o = isBoosting ?
                new ParticleEntity(new Rectangle(256, 128, 64, 64), types, new Vector2(position.X, position.Y + SpriteController.CurrentRect.Height / 2),
                    new Vector2((GameMath.RNGer.NextFloat() - 0.5f) * 180.0f + newVel.X, (GameMath.RNGer.NextFloat() + 2f) * 300.0f + newVel.Y), new Color(0.3f, 0.4f, 0.8f, 1f), GameMath.RNGer.NextFloat() * 6.28319f, new Vector2(size * 1.5f, size * 1.5f)) :
                new ParticleEntity(new Rectangle(256, 128, 64, 64), types, new Vector2(position.X, position.Y + SpriteController.CurrentRect.Height / 2),
                    new Vector2((GameMath.RNGer.NextFloat() - 0.5f) * 210.0f + newVel.X, (GameMath.RNGer.NextFloat() + 2f) * 240.0f + newVel.Y), new Color(0.8f, 0.4f, 0.2f, 1f), GameMath.RNGer.NextFloat() * 6.28319f, new Vector2(size, size));
            newParticles.Add(o);

            //generate rocket smoke effects
            if (GameMath.RNGer.Next(4) == 0)
            {
                size = GameMath.RNGer.NextFloat() * 0.45f + 0.05f;
                types = new List<ParticleAI>();
                types.Add(new FadingAI(0.45f));
                types.Add(new RotatingAI(0.3f));
                types.Add(new GrowingLinearAI(1.2f));
                o = isBoosting ?
                    new ParticleEntity(new Rectangle(0, 0, 64, 64), types, new Vector2(position.X, position.Y + SpriteController.CurrentRect.Height / 2),
                        new Vector2((GameMath.RNGer.NextFloat() - 0.5f) * 300.0f + newVel.X, (GameMath.RNGer.NextFloat() + 2f) * 420.0f + newVel.Y), new Color(0.7f, 0.8f, 1f, 1f), GameMath.RNGer.NextFloat() * 6.28319f, new Vector2(size * 1.5f, size * 1.5f)) :
                    new ParticleEntity(new Rectangle(0, 0, 64, 64), types, new Vector2(position.X, position.Y + SpriteController.CurrentRect.Height / 2),
                        new Vector2((GameMath.RNGer.NextFloat() - 0.5f) * 210.0f + newVel.X, (GameMath.RNGer.NextFloat() + 2f) * 240.0f + newVel.Y), new Color(1f, 1f, 1f, 1f), GameMath.RNGer.NextFloat() * 6.28319f, new Vector2(size, size));
                newParticles.Add(o);
            }
            
            //return new stuff
            return newParticles;
        }

        public void Draw(SpriteBatch b, Vector2 position)
        {
            SpriteController.Draw(b, position);

            if (Shields > 0)
            {
                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.Additive);

                b.Draw(particles, position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(effect1, effect1), SpriteEffects.None, 0f);
                b.Draw(particles, position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(effect2, 0.3f + effect2), SpriteEffects.None, 0f);
                b.Draw(particles, position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(0.3f + effect3, effect3), SpriteEffects.None, 0f);
                b.Draw(particles, position, shieldBubble, effectColor, 0, new Vector2(shieldBubble.Width / 2, shieldBubble.Height / 2), new Vector2(0.3f + effect4, 0.3f + effect4), SpriteEffects.None, 0f);
                
                b.End();
                b.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            }
        }

        public float HitShields(float damage) //for when a part is hit and needs to apply damage to shields first, returns remaining damage after shield is depleted
        {
            if (Shields < damage)
            {
                damage -= Shields;
                Shields = 0;
            }
            else
            {
                Shields -= damage;
                damage = 0;
            }
            return damage;
        }

        public override bool Hit(float damage)
        {
            lastHit = 0;
            if (Shields < damage)
            {
                damage -= Shields;
                Shields = 0;
                if (Hull < damage)
                {
                    Destroyed = true;
                    Hull = 0;
                }
                else
                {
                    Hull -= damage;
                }
            }
            else
            {
                Shields -= damage;
            }
            SpriteController.Hit();
            return Destroyed;
        }

        public override string GetName()
        {
            switch (ItemID)
            {
                case 1: return "Sparrow I";
            }
            return "";
        }
        
        public override void Reset() //called at initialization (and when respawning, for now)
        {
            base.Reset();
            Boost = 100;
            lastHit = 2;
            SubParts = new List<FramePartItem>();
            WeaponPoints = new List<FrameHardpoint>();
            ShieldPoints = new List<FrameHardpoint>();
            CorePoints = new List<FrameHardpoint>();
            EnginePoints = new List<FrameHardpoint>();
            EnhancementPoints = new List<FrameHardpoint>();
            switch (ItemID)
            {
                case 1:
                    //TODO make this depend on quality/damage, also move this stuff to an UpdateStats() method so it can be called on construct and whenever I need to repair the item
                    MaxWeight = 100f;
                    Hull = 200f;
                    MaxHull = 200f;
                    WingDependency = 0.35f;
                    SpriteController = new SparrowSpriteController();
                    Collider = new RectCollider(new Vector2(22f, 53f), new Vector2(0f, -3f), Owner.Entity, ColliderType.Player);
                    SubParts.Add(new FramePartItem(1, 100f, 0, this, Owner));
                    SubParts.Add(new FramePartItem(2, 100f, 0, this, Owner));
                    ShieldPoints.Add(new FrameHardpoint(1, new ShieldGeneratorItem(1, 100f, 0f, Owner)));
                    EnginePoints.Add(new FrameHardpoint(2, new EngineItem(1, 100f, 0f, Owner)));
                    WeaponPoints.Add(new FrameHardpoint(3, new WeaponItem(1, 100f, 0f, Owner)));
                    CorePoints.Add(new FrameHardpoint(4, new PowerCoreItem(1, 100f, 0f, Owner)));
                    EnhancementPoints.Add(new FrameHardpoint(5));
                    break;
            }
            UpdateFrameStats();
            Power = ActualPowerCapacity;
        }

        public override List<ParticleEntity> GetDeathParticles()
        {
            //TODO: replace with something better, this is direct copy from enemies
            List<ParticleEntity> spawnedParticles = new List<ParticleEntity>();
            float explodemod = 2.5f;

            List<ParticleAI> types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.18f));
            types.Add(new RotatingAI(0.25f));
            types.Add(new FadingAI(0.75f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(320, 0, 64, 64), types, Owner.Entity.Position, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(0.15f + explodemod * 0.2f, 0.15f + explodemod * 0.2f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.12f));
            types.Add(new RotatingAI(0.25f));
            types.Add(new FadingAI(1.5f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(704, 0, 64, 64), types, Owner.Entity.Position, new Vector2(), new Color(255, 255, 128), 0f, new Vector2(0.10f + explodemod * 0.2f, 0.10f + explodemod * 0.2f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.08f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(1.8f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(384, 0, 64, 64), types, Owner.Entity.Position, new Vector2(), new Color(255, 128, 128), 0f, new Vector2(0.09f + explodemod * 0.18f, 0.09f + explodemod * 0.18f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.04f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(2f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(576, 0, 64, 64), types, Owner.Entity.Position, new Vector2(), new Color(255, 128, 0), 0f, new Vector2(0.3f + explodemod * 0.2f, 0.3f + explodemod * 0.2f)));

            types = new List<ParticleAI>();
            types.Add(new GrowingAI(1.09f));
            types.Add(new RotatingAI(0.20f));
            types.Add(new FadingAI(1.8f));
            spawnedParticles.Add(new ParticleEntity(new Rectangle(576, 0, 64, 64), types, Owner.Entity.Position, new Vector2(), new Color(255, 192, 64), 0f, new Vector2(0.09f + explodemod * 0.2f, 0.09f + explodemod * 0.2f)));

            Rectangle flameRect = new Rectangle(256, 128, 64, 64);
            for (int count = 0; count < 20; count++)
            {
                types = new List<ParticleAI>();
                types.Add(new RotatingAI(0.20f));
                types.Add(new FadingAI(1.5f));
                types.Add(new PulsingAI(1.01f, 14));
                float sizeMod = GameMath.RNGer.NextFloat();
                spawnedParticles.Add(new ParticleEntity(flameRect, types, Owner.Entity.Position, new Vector2(-300f + GameMath.RNGer.NextFloat() * 600f, -300f + GameMath.RNGer.NextFloat() * 600f), new Color(255, 128 + GameMath.RNGer.Next(128), 0 + GameMath.RNGer.Next(64)), 0f, new Vector2(0.1f + sizeMod * 0.3f, 0.1f + sizeMod * 0.3f)));
            }

            Rectangle sparkRect = new Rectangle(0, 64, 64, 64);
            for (int i = 0; i < 30; i++) //screen sparks
            {
                types = new List<ParticleAI>();
                types.Add(new RotatingAI(0.3f));
                types.Add(new FadingAI(3f));
                types.Add(new StandardGravitationAI());
                types.Add(new PulsingAI(1.03f, 8));
                float size = GameMath.RNGer.NextFloat() * 0.8f + 0.2f;
                spawnedParticles.Add(new ParticleEntity(sparkRect, types, new Vector2(GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Width, GameMath.RNGer.NextFloat() * GameDataManager.Gfx.GraphicsDevice.Viewport.Height), new Vector2((-0.5f + GameMath.RNGer.NextFloat()) * 200f, -GameMath.RNGer.NextFloat() * -200f), new Color(255, 220, 192), GameMath.RNGer.NextFloat() * (float)Math.PI * 2f, new Vector2(size, size)));
            }
            return spawnedParticles;
        }
    }
}
