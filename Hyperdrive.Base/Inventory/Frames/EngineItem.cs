﻿using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory.Frames
{
    public class EngineItem : EquipmentItem
    {
        public float Acceleration { get; protected set; }
        public float MaxSpeed { get; protected set; }
        public float BoostTime { get; protected set; }
        public float BoostCooldown { get; protected set; }
        public float BoostSpeedMultiplier { get; protected set; }
        public float BoostAccelMultiplier { get; protected set; }

        public EngineItem(int id, float quality, float damage, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            Reset();
        }
        
        public override string GetName()
        {
            switch (ItemID)
            {
                case 1: return "Nudger X-33";
            }
            return "";
        }

        public override void Reset()
        {
            base.Reset();
            switch (ItemID)
            {
                case 1:
                    Weight = 12f;
                    ActivePowerConsumption = 8f;
                    PassivePowerConsumption = 12f;
                    Acceleration = 1500f;
                    MaxSpeed = 400f;
                    BoostTime = 3f;
                    BoostCooldown = 10f;
                    BoostAccelMultiplier = 2f;
                    BoostSpeedMultiplier = 1.5f;
                    break;
            }
        }
    }
}
