﻿namespace Hyperdrive.Base.Inventory.Frames
{
    public class FrameHardpoint
    {
        //used to denote a hardpoint on the frame
        public int HardpointID { get; protected set; }
        public EquipmentItem EquippedItem { get; protected set; }

        public FrameHardpoint(int id)
        {
            HardpointID = id;
            EquippedItem = null;
        }

        public FrameHardpoint(int id, EquipmentItem item)
        {
            HardpointID = id;
            EquippedItem = item;
        }

        public bool TryEquipItem(EquipmentItem item)
        {
            if (EquippedItem == null)
            {
                EquippedItem = item;
                return true;
            }
            else return false;
        }

        public bool TryRemoveItem()
        {
            if (EquippedItem != null)
            {
                EquippedItem = null;
                return true;
            }
            else return false;
        }
    }
}