﻿using Microsoft.Xna.Framework;
using Hyperdrive.Base.Player.Shots;
using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory.Frames
{
    public enum WeaponType
    {
        STANDARD
    }

    public class WeaponItem : EquipmentItem
    {
        public float ShotsPerSecond { get; protected set; }
        public float ShotVelocity { get; protected set; }
        public Vector2 ShotScale { get; protected set; }
        public float MinDamage { get; protected set; }
        public float MaxDamage { get; protected set; }
        public float Recoil { get; protected set; }
        public float Knockback { get; protected set; }
        public Color ShotColor { get; protected set; }
        public PlayerShotSpriteController ShotController { get; protected set; }
        public float ShotDelay { get { return 1f / ShotsPerSecond; } }
        public bool CanFire { get { return lastFired >= ShotDelay; } }
        private double lastFired;

        public WeaponItem(int id, float quality, float damage, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            Reset();
        }

        public override string GetName()
        {
            switch (ItemID)
            {
                case 1: return "Needler I";
            }
            return "";
        }

        public override void Reset()
        {
            base.Reset();
            switch (ItemID)
            {
                case 1:
                    ActivePowerConsumption = 4f;
                    PassivePowerConsumption = 8f;
                    ShotController = new StandardShotSpriteController();
                    ShotColor = Color.Lerp(Color.White, Color.Yellow, 0.5f);
                    ShotsPerSecond = 4f;
                    ShotVelocity = 350f;
                    ShotScale = new Vector2(0.25f, 0.5f); //percent of actual shot size from animation (generally 64x64)
                    MinDamage = 2f;
                    MaxDamage = 4f;
                    Recoil = 60f;
                    Knockback = 90f;
                    break;
            }
            lastFired = ShotDelay;
        }
        
        public void Update(double dSeconds)
        {
            lastFired += dSeconds;
        }

        public PlayerShotEntity FireWeapon()
        {
            lastFired = 0;
            //TODO: switch based on type
            return new PlayerShotEntity(Owner, Owner.Entity.Position + new Vector2(0, -32f), new Vector2(0, -ShotVelocity), ShotController.GetNewBulletAnimation(), MinDamage, MaxDamage, ShotScale, Knockback, ShotColor);
        }
    }
}
