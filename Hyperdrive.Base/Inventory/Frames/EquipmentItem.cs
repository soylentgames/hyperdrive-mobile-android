﻿using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory.Frames
{
    public abstract class EquipmentItem : GameItem 
    {
        //abstract superclass for all ship items besides frames and frameparts
        public float PassivePowerConsumption { get; protected set; } //how much the total power bar is reduced by when this item is equipped
        public float ActivePowerConsumption { get; protected set; } //how much power the item consumes per second while in use (for engines this is when boosting, weapons when firing, etc)
        public float Weight { get; protected set; }
        public bool Destroyed { get; protected set; }

        public EquipmentItem(int id, float quality, float damage, PlayerCharacter owner) : base(id, quality, damage, owner)
        {
            Reset();
        }

        public override float GetMaxAmount()
        {
            return 1f;
        }

        public virtual void Reset()
        {
            Destroyed = false;
        }

        public virtual void Destroy()
        {
            Destroyed = true;
        }
    }
}