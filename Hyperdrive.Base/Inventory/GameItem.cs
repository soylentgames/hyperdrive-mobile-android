﻿using Hyperdrive.Base.Player;

namespace Hyperdrive.Base.Inventory
{
    public abstract class GameItem
    {
        public PlayerCharacter Owner { get; set; }
        public int ItemID { get; protected set; }
        public float Amount { get; protected set; }
        public float Quality { get; protected set; } //the quality of the item will be used in spawning code to determine stats
        public float Damage { get; protected set; } //this is the overall damage the item has taken, 100% damaged items will be unusable and require double components to repair
        
        public GameItem(int id, float quality, float damage, PlayerCharacter owner)
        {
            ItemID = id;
            Quality = quality;
            Damage = damage;
            Owner = owner;
        }

        public abstract string GetName();

        public abstract float GetMaxAmount();

        public virtual bool TryChangeAmount(int amt)
        {
            if ((amt > 0 && Amount + amt <= GetMaxAmount()) ||
                (amt < 0 && Amount + amt >= 0))
            {
                Amount += amt;
                return true;
            }
            else return false;
        }
    }
}
