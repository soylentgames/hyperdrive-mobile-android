﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Container;

namespace Hyperdrive.Base.Player
{
    public class PlayerEntity : CollidableEntity
    {
        public PlayerCharacter Owner { get; set; }
        
        private Vector2 originalPosition; //used to respawn

        public PlayerEntity(Vector2 pos) : base(pos, new Vector2(0,0))
        {
            originalPosition = new Vector2(pos.X, pos.Y);
            
            Owner = new PlayerCharacter(this); //TODO replace this later with character loading

            Collider = new FrameCollider(new Vector2(), this, Owner.EquippedFrame.Colliders);
        }
        
        public override void Update(float seconds, double dSeconds) //don't use this version for player
        {
        }

        public void Update(float seconds, double dSeconds, bool isFiring, Vector2 direction, EntityContainer shotPool, bool isBoosting)
        {
            base.Update(seconds, dSeconds); //just updates position based on vel and resets ReturnList
            
            Vector2 newVel;
            ReturnList.AddRange(Owner.EquippedFrame.Update(seconds, dSeconds, isFiring, Position, direction, Velocity, shotPool, isBoosting, out newVel));
            Velocity = newVel;
            
            //keep player on screen
            if (Position.X < 0) Position = new Vector2(0, Position.Y);
            else if (Position.X > GameDataManager.Gfx.GraphicsDevice.Viewport.Width) Position = new Vector2(GameDataManager.Gfx.GraphicsDevice.Viewport.Width, Position.Y);
            if (Position.Y < 0) Position = new Vector2(Position.X, 0);
            else if (Position.Y > GameDataManager.Gfx.GraphicsDevice.Viewport.Height) Position = new Vector2(Position.X, GameDataManager.Gfx.GraphicsDevice.Viewport.Height);

            GameDataManager.Grid.AddToGrid(this); //TODO: move this to player container later
        }
        
        public override void Draw(SpriteBatch b)
        {
            Owner.EquippedFrame.Draw(b, Position);
        }

        public void Respawn()
        {
            Position = new Vector2(originalPosition.X, originalPosition.Y);
            Owner.Reset(); //replace this later to load the last saved state
        }
    }
}
