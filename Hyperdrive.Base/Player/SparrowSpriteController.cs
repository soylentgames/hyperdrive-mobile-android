﻿using Hyperdrive.Base.Gfx;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Hyperdrive.Base.Player
{
    public class SparrowSpriteController : PlayerSpriteController
    {
        public static Texture2D texture;

        public SparrowSpriteController() 
        {
            Animation newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(0, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_Normal, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(64, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(128, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(192, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_Damage1, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(256, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(320, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(384, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_Damage2, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(448, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(512, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(576, 64, 64, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_Damage3, newAnim);

            //left wing
            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(0, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_LeftWing_Normal, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(64, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(128, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(192, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_LeftWing_Damage1, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(256, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(320, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(384, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_LeftWing_Damage2, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(448, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(512, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(576, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_LeftWing_Damage3, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(64, 192, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(128, 192, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(192, 192, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_LeftWing_Destroyed, newAnim);

            //right wing
            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(32, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_RightWing_Normal, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(96, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(160, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(224, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_RightWing_Damage1, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(288, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(352, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(416, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_RightWing_Damage2, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(480, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(544, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(608, 128, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_RightWing_Damage3, newAnim);

            newAnim = new Animation();
            newAnim.AddFrame(new Rectangle(96, 192, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(160, 192, 32, 64), TimeSpan.FromSeconds(0.15));
            newAnim.AddFrame(new Rectangle(224, 192, 32, 64), TimeSpan.FromSeconds(0.15));
            animations.Add(AnimationType.Player_RightWing_Destroyed, newAnim);

            //setup current anims
            animations.TryGetValue(AnimationType.Player_Normal, out currentAnimation);
            animations.TryGetValue(AnimationType.Player_LeftWing_Normal, out currentLeftWingAnim);
            animations.TryGetValue(AnimationType.Player_RightWing_Normal, out currentRightWingAnim);

            flashRectBody = new Rectangle(0, 192, 64, 64);
            flashRectLeftWing = new Rectangle(0, 256, 32, 64);
            flashRectRightWing = new Rectangle(32, 256, 32, 64);

            MainHitbox = new Vector4(0, 0, 20, 64); //guessed values, fix later
            LeftWingHitbox = new Vector4(-32, -10, 32, 32);
            RightWingHitbox = new Vector4(0, -10, 32, 32);

            drawColor = Color.White;
            LastHit = 0;
        }

        public override void Draw(SpriteBatch b, Vector2 position)
        {
            Draw(b, texture, position);
        }
    }
}
