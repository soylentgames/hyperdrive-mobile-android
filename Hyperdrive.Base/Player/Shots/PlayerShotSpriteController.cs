﻿using Hyperdrive.Base.Gfx;

namespace Hyperdrive.Base.Player.Shots
{
    public abstract class PlayerShotSpriteController : SpriteController
    {
        public abstract Animation GetNewBulletAnimation();
    }
}
