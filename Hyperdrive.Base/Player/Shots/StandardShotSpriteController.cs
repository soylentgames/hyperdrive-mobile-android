﻿using Hyperdrive.Base.Gfx;
using Microsoft.Xna.Framework;
using System;

namespace Hyperdrive.Base.Player.Shots
{
    class StandardShotSpriteController : PlayerShotSpriteController
    {
        public StandardShotSpriteController() : base()
        {
            Animation shot1 = new Animation();
            shot1.AddFrame(new Rectangle(448, 128, 64, 64), TimeSpan.FromSeconds(0.1));
            shot1.AddFrame(new Rectangle(512, 128, 64, 64), TimeSpan.FromSeconds(0.1));
            shot1.AddFrame(new Rectangle(576, 128, 64, 64), TimeSpan.FromSeconds(0.1));
            shot1.AddFrame(new Rectangle(640, 128, 64, 64), TimeSpan.FromSeconds(0.1));
            animations.Add(AnimationType.Player_Shot1, shot1);

            currentAnimation = animations[AnimationType.Player_Shot1];
        } 

        public override Animation GetNewBulletAnimation()
        {
            Animation value;
            animations.TryGetValue(AnimationType.Player_Shot1, out value);
            return new Animation(value.GetFrames());
        }
    }
}
