﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Hyperdrive.Base.Particle;
using Hyperdrive.Base.Particle.AI;
using Hyperdrive.Base.Collision;
using Hyperdrive.Base.Gfx;

namespace Hyperdrive.Base.Player.Shots
{
    public class PlayerShotEntity : CollidableEntity
    {
        public static Texture2D particles;
        private static Rectangle glowRect = new Rectangle(574, 0, 64, 64);

        private Animation animation;
        private double glowCount;
        private Color glowColor;

        private float MinDamage { get; set; }
        private float MaxDamage { get; set; }
        private float DamageRange { get { return MaxDamage - MinDamage; } }
        private Vector2 Scale { get; set; }
        private Color ShotColor { get; set; }
        public float Knockback { get; protected set; }
        public PlayerCharacter Owner { get; set; }

        public PlayerShotEntity(PlayerCharacter owner, Vector2 position, Vector2 velocity, Animation animation, float minDamage, float maxDamage, Vector2 scale, float knockback, Color color) : base(position, velocity)
        {
            Owner = owner;
            ShotColor = color;
            this.animation = animation;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            Scale = scale;
            Knockback = knockback;
            glowCount = 0;
            glowColor = new Color();
            Collider = new RectCollider(new Vector2(animation.CurrentRectangle.Size.X * Scale.X, animation.CurrentRectangle.Size.Y * Scale.Y), new Vector2(), this, ColliderType.PlayerShot);
        }

        public override void Update(float seconds, double dSeconds)
        {
            base.Update(seconds, dSeconds);
            glowCount += dSeconds;
            if (glowCount >= 1) glowCount -= 1;
            float amt = (float)((Math.Sin(glowCount * Math.PI * 8) + 1) / 2);
            Vector3 colorVals = ShotColor.ToVector3();
            glowColor = new Color(colorVals.X * 0.5f + colorVals.X * amt * 0.7f, colorVals.Y * 0.5f + colorVals.Y * amt * 0.7f, colorVals.Z * 0.5f + colorVals.Z * amt * 0.7f);
            animation.Update(dSeconds);
            if (GameMath.RNGer.Next(4) == 0)
            {
                List<ParticleAI> types = new List<ParticleAI>();
                types.Add(new FadingAI(0.25f));
                Rectangle currRect = animation.CurrentRectangle;
                ReturnList.Add(new ParticleEntity(currRect, types, Position + new Vector2((-currRect.Width * Scale.X / 2f) + GameMath.RNGer.NextFloat() * (currRect.Width * Scale.X), -(currRect.Height * Scale.Y / 2f) + GameMath.RNGer.NextFloat() * (currRect.Height * Scale.Y)), Velocity / 2f, ShotColor, Rotation, Scale / 2f));
            }
            
        }

        public override void Draw(SpriteBatch b)
        {
            Rectangle currentRect = animation.CurrentRectangle;
            b.Draw(particles, Position, currentRect, ShotColor, 0, new Vector2(currentRect.Width / 2f, currentRect.Height / 2f), Scale, SpriteEffects.None, 0f);
            b.Draw(particles, Position, glowRect, glowColor, 0, new Vector2(glowRect.Width / 2f, glowRect.Height / 2f), Scale + new Vector2(1f,1f), SpriteEffects.None, 0f);
        }

        public float GetDamage()
        {
            return MinDamage + DamageRange * GameMath.RNGer.NextFloat();
        }
    }
}
