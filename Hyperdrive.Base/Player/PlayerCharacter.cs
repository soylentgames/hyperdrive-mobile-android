﻿using Hyperdrive.Base.Inventory;
using Hyperdrive.Base.Inventory.Frames;
using System;

namespace Hyperdrive.Base.Player
{
    public class PlayerCharacter
    {
        //placeholder for player info, make this class later and save all inventory, stats, skills, items, etc in it
        public float Score { get; protected set; }
        public int Level { get; protected set; }
        public int UnusedPoints { get; protected set; }
        public GameInventory Inventory { get; protected set; }
        public FrameItem EquippedFrame { get; protected set; }
        public PlayerEntity Entity { get; protected set; }
        public float ScoreForThisLevel
        {
            get
            {
                if (Level == 1) return 0;
                else return (int)((50 * (Level - 1)) + 100f * Math.Pow(1.12, (Level - 2)));
            }
        }
        public float ScoreForNextLevel
        {
            get { return (int)((50 * Level) + 100f * Math.Pow(1.12, (Level - 1))); }
        }
        public float ExperiencePercentage { get { return Score / ScoreForNextLevel; } }

        public PlayerCharacter(PlayerEntity entity)
        {
            EquippedFrame = new FrameItem(1, 1, 100f, 0f, this); //replace this later with loading
            Entity = entity;
            Reset(); //TODO add methods for loading existing data
        }

        public void Reset()
        {
            //TODO save character state at beginning of level instead of always resetting to new, at that point this method will be kept but only for when players want to start over, and then we'll have to add inventory reset, etc
            Score = 0;
            Level = 1;
            UnusedPoints = 0;
            EquippedFrame.Reset();
            EquippedFrame.SpriteController.Respawn();
        }

        public bool AddScore(float points)
        {
            Score += points;
            return LevelCheck();
        }

        private bool LevelCheck()
        {
            bool leveled = false;
            while (Score >= ScoreForNextLevel)
            {
                Score -= ScoreForNextLevel;
                Level += 1;
                
                if (Level < 10) UnusedPoints += 1;
                else if (Level >= 10 && Level < 25) UnusedPoints += 2;
                else if (Level >= 25 && Level < 50) UnusedPoints += 3;
                else if (Level >= 50 && Level < 100) UnusedPoints += 4;
                else UnusedPoints += 5;
                leveled = true;
            }
            return leveled;
        }
    }
}
