﻿using Hyperdrive.Base.Gfx;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Hyperdrive.Base.Player
{
    public abstract class PlayerSpriteController : SpriteController
    {
        public Vector4 LeftWingHitbox { get; protected set; } //x & y are for origin offset, z & w are for width and height
        public Vector4 RightWingHitbox { get; protected set; } 
        public Vector4 MainHitbox { get; protected set; }
        public float LastHit { get; protected set; }

        protected Rectangle flashRectBody; //these will be used for the segmented body mechanic and flashRect will be nuked
        protected Rectangle flashRectLeftWing;
        protected Rectangle flashRectRightWing;

        protected Color drawColor;
        protected float lastLeftHit = 1f;
        protected float lastRightHit = 1f;
        protected float lastBodyHit = 1f;
        protected Animation currentLeftWingAnim;
        protected Animation currentRightWingAnim;

        public void Update(float hullPercent, float leftWingPercent, float rightWingPercent, float seconds, double dSeconds)
        {
            if (hullPercent < 0.3333f) animations.TryGetValue(AnimationType.Player_Damage3, out currentAnimation);
            else if (hullPercent < 0.6667f) animations.TryGetValue(AnimationType.Player_Damage2, out currentAnimation);
            else if (hullPercent < 1f) animations.TryGetValue(AnimationType.Player_Damage1, out currentAnimation);

            if (leftWingPercent <= 0) animations.TryGetValue(AnimationType.Player_LeftWing_Destroyed, out currentLeftWingAnim);
            else if (leftWingPercent < 0.3333f) animations.TryGetValue(AnimationType.Player_LeftWing_Damage3, out currentLeftWingAnim);
            else if (leftWingPercent < 0.6667f) animations.TryGetValue(AnimationType.Player_LeftWing_Damage2, out currentLeftWingAnim);
            else if (leftWingPercent < 1f) animations.TryGetValue(AnimationType.Player_LeftWing_Damage1, out currentLeftWingAnim);

            if (rightWingPercent <= 0) animations.TryGetValue(AnimationType.Player_RightWing_Destroyed, out currentRightWingAnim);
            else if (rightWingPercent < 0.3333f) animations.TryGetValue(AnimationType.Player_RightWing_Damage3, out currentRightWingAnim);
            else if (rightWingPercent < 0.6667f) animations.TryGetValue(AnimationType.Player_RightWing_Damage2, out currentRightWingAnim);
            else if (rightWingPercent < 1f) animations.TryGetValue(AnimationType.Player_RightWing_Damage1, out currentRightWingAnim);

            else animations.TryGetValue(AnimationType.Player_Normal, out currentAnimation);
            
            LastHit += seconds;
            lastLeftHit += seconds;
            lastRightHit += seconds;
            lastBodyHit += seconds;

            if (LastHit < 0.5f)
            {
                float redAmt = 1.0f - LastHit / 0.5f;
                drawColor = new Color(1f, 1f - redAmt, 1f - redAmt);
            }
            else drawColor = Color.White;

            currentAnimation.Update(dSeconds);
        }

        public void Respawn()
        {
            LastHit = 0.5f;
        }

        public void Hit()
        {
            LastHit = 0;
            lastBodyHit = 0;
        }

        public void LeftWingHit()
        {
            LastHit = 0;
            lastLeftHit = 0;
        }

        public void RightWingHit()
        {
            LastHit = 0;
            lastRightHit = 0;
        }

        protected void Draw(SpriteBatch b, Texture2D texture, Vector2 position)
        {
            b.Draw(texture, position, currentAnimation.CurrentRectangle, drawColor, 0, new Vector2(currentAnimation.CurrentRectangle.Width/2,currentAnimation.CurrentRectangle.Height/2), 1.0f ,SpriteEffects.None, 0f);
            b.Draw(texture, position, currentLeftWingAnim.CurrentRectangle, drawColor, 0, new Vector2(currentLeftWingAnim.CurrentRectangle.Width, currentLeftWingAnim.CurrentRectangle.Height / 2), 1.0f, SpriteEffects.None, 0f);
            b.Draw(texture, position, currentRightWingAnim.CurrentRectangle, drawColor, 0, new Vector2(0, currentRightWingAnim.CurrentRectangle.Height / 2), 1.0f, SpriteEffects.None, 0f);
            if (lastBodyHit == 0) b.Draw(texture, position, flashRectBody, Color.White, 0, new Vector2(flashRectBody.Width/2, flashRectBody.Height/2), 1.0f, SpriteEffects.None, 0f);
            if (lastLeftHit == 0) b.Draw(texture, position, flashRectLeftWing, Color.White, 0, new Vector2(flashRectLeftWing.Width, flashRectLeftWing.Height / 2), 1.0f, SpriteEffects.None, 0f);
            if (lastRightHit == 0) b.Draw(texture, position, flashRectRightWing, Color.White, 0, new Vector2(0, flashRectRightWing.Height / 2), 1.0f, SpriteEffects.None, 0f);
        }

        public abstract void Draw(SpriteBatch b, Vector2 position);
    }
}
